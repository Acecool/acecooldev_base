Things to expect:

Networking Meta-Table / Object with a lot of useful networking functions.
	// Currently adding a Queue to the system, and UMSG support will be added.
	// As of right now 20kb/sec is the speed cap, and sending more data will clog the system.
	// The goal with creating a queue and integrating umsg support is that small messages can
	// be redirected from net to umsg if net is sending large amounts of data; this ensures
	// all messages arrive in good time and will minimize lag caused by sending mass amounts of data
	// with the term "lag" being used loosely as you don't have lag, just a delay on messages going through.

Data Meta-Table / Object that handles how data is stored, manipulated, managed and distributed.
	// This holds data which is used throughout the game-mode. Not only that, but private data is always
	// private, unless you add a link. Example, no one but the driver of a vehicle needs to know the odometer,
	// speed ( when CurTime breaks after server being up more than 24 hours causing the speedo needle to act odd ) so
	// we network it; other people can grab the speed as usual if needed, and other settings for the vehicle; so to
	// ensure the driver gets the data, when the user enters the vehicle a link is established so the user gets
	// all relevant data ( speed, fuel, odo, battery, etc ). The link is removed when users get out of a vehicle.
	// This may be used with weapons too because no one but server and shooter needs to know firemode etc.

Database Meta-Table / Object which controls database connectivity, fallbacks, and more!
	// This will manage a database connection, while allowing more than one if need-be.

SQL Query Builder Meta-Table / Object which builds simple to complex queries ( including joins ) using simple function calls.
	// Example.. This builds a query to determine whether or not a client exists in the accounts table
	local _q = query:New( );
	_q:AddTable( DATABASE_USER_ACCOUNTS_TABLE, { "user_connects", "firstlogin", "steamid64", "steamuid" } );
	_q:AddWhere( DATABASE_USER_ACCOUNTS_TABLE, "steamid", _steamid );
	local _query = _q:Build( );
	
	// And this one updates the information when a client connects ( such as times connected )..
	// The nice thing about this system, is the update_rows and update_data tables can be manipulated to add additional
	// data to update... If you ported data from sv.db, users use uniqueid not SteamID.. You can auto-populate
	// missing fields ( but only if they don't exist ) like this:
	local _update_rows = { "user_connects" };
	local _update_data = { _data.connects + 1 };

	// One of the beauties of the query builder is that I can set up dynamic updates to populate data until it is populated.
	if ( !_data.steamid64 || _data.steamid64 == "0" ) then
		table.insert( _update_rows, "steamid64" );
		table.insert( _update_data, _steamid64 );
	end

	// One of the beauties of the query builder is that I can set up dynamic updates to populate data until it is populated.
	if ( !_data.steamuid || _data.steamuid == "" ) then
		table.insert( _update_rows, "steamuid" );
		table.insert( _update_data, _steamuid );
	end

	local _q = query:New( );
	_q:SetType( QUERY_UPDATE );
	_q:AddTable( DATABASE_USER_ACCOUNTS_TABLE, _update_rows, _update_data );
	_q:AddWhere( DATABASE_USER_ACCOUNTS_TABLE, "steamid", _steamid );
	_q:AddLimit( 1 );
	local _query = _q:Build( );