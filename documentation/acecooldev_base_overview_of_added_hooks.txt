//
// This document briefly covers all NEW GM functions added as hooks.
// The extended information folder will go into greater detail as to where these hooks are fired, and other specifics.
//

[ SHARED ] GM:PlayerJumped( _p )
	Called when a player jumps; only called once until player hits the ground.

[ CLIENT ] GM:PlayerRecordingDemo( _p, _recording )
	Called when a player starts or stops recording a demo with _recording being true/false based
	on whether the user has started or stopped the recording; useful to sync network data as it
	is cleared on start of demo and only data received AFTER the demo has started gets used in the demo.
	-- Networks data to server to become "shared"

[ SERVER ] GM:PlayerFullyConnected( _p )
	Called when a player has FULLY connected to the server ( meaning they are completely in the game ).
	This is useful to sync network data, open VGUI, etc...
	-- Networks data to client to become "shared"

[ SERVER ] GM:SyncData( _p )
	Called in PlayerFullyConnected for an appropriate time to network all data to the client.

[ SERVER ] GM:CanPlayerDropWeapon( _p, _w )
	This hook exists to add a new feature to the game; Essentially it is a check to verify whether or not
	a player may drop their weapon

[ SERVER ] GM:PlayerDroppedWeapon( _p, _w )
	PlayerDroppedWeapon is called when a player successfully dropped their weapon. The META_PLAYER:DropWeapon
	function also calls META_WEAPON:OnDrop by default.
	-- Networks data to client to become "shared"

[ CLIENT ] GM:PlayerChangedResolution( _p )
	Called when a player changed their game-resolution; useful for HUDs and VGUI with complex structures,
	or objects so they can be created ONCE on script load, and re-calculated only when necessary.
	-- Networks data to server to become "shared"

[ SERVER ] GM:RestartLevel( _map, _gamemode )
	Runs changelevel; if no map specified, it uses the current map. If game-mode specified, it runs the
	gamemode command before running changelevel.

[ SERVER ] GM:RestartMap( _map, _gamemode )
	Runs map; if no map specified, it uses the current map. If game-mode specified, it runs the
	gamemode command before running map ( which kicks all clients ).