*******************************************************************************************

************************************ GM RESOURCESLOADER ***********************************

*******************************************************************************************

//
// From our Folder Tree, <acecooldev_base>/content/workshop/
// This file describes what this folder does, and why.
//
[ acecooldev_base ]
	content/ 		-- CONTENT
		workshop/	-- CONTENT_WORKSHOP


//
// Filename Parsing
//
	Filenames are parsed in the <acecooldev_base>/content/workshop/ directory in such a
	way that the file can have any name you want, provided the only digits in the name
	are those of the workshop-id. When the server downloads workshop files, they're named
	ds_123456789.gma; It is best to keep the same name and modify it to add a comment as
	to what the file actually is.

	Here are some examples I use to tell me what addon it is, why I did what I did, etc...
		ds_128089118_m_nine_k_rifles_extracted_to_remove_lua.gma
		ds_128091208_m_nine_k_heavy_extracted_to_remove_lua.gma
		ds_128093075_m_nine_k_small_extracted_to_remove_lua.gma
		ds_144982052_m_nine_k_special_extracted_to_remove_lua.gma
		ds_155298969_jackarunda_explosives_extracted_to_remove_lua.gma

	Remember, ALL files in the directory should be EMPTY unless you want to add other data
	such as a link to the file on the workshop, etc as the content. Treat the files as TEXT.

	I keep the files empty to avoid any issues of the file being loaded automatically as
	some resource; it shouldn't happen as with my resource-loader it marks the folder and
	processes it specially, but nothing is stopping the engine from doing anything with it.

//
// <acecooldev_base>/content/workshop/
//
	The CONTENT_WORKSHOP folder is specifically designed for cases where you want to use
	an addon, but you don't want to load the Lua files into the server, or you wish to 
	prevent other, specific, files from loading but where the addon should be downloaded
	to the client such that files the server wants to use will be made available to the
	client.

	Under normal circumstances, extracting all addons is NOT something that should be done.
	When the server has to load extracted addons, it loads each file individually and it is
	SLOW; however, when loading one .gma file, it is VERY QUICK!

	I did a test to see the speed difference during normal server operation, and during auto-
	-refresh. With all addons on my server extracted ( 40 or so, vehicles, models, etc... )
	the server took 30-60 seconds for the server to auto-refresh.

	When all of my addons are in .gma format ( except the 5 or so I extracted to remove Lua
	files ) the server refreshes completely within ~2-5 seconds.

	When using my custom auto-load system ( "loadgm" concommand ), refreshing takes ~2 seconds.

	When using a completely custom "detect-which-specific-file-changed" variant, refreshes
	takes sub 2 seconds.




//
// Descriptions ( Read them all; they're located in the <acecooldev_base>/documentation/ folder )
//
CONTENT
	This is where content is stored.

CONTENT_WORKSHOP
	This folder is strange. Create new, blank, files and name them as a workshop
	gma file would be named ( such as ds_#_comments.gma: ds_128089118_m9k_ars_lua.gma )
	This folder is so that clients will be told to download these addons. The reason they
	should be told via this folder is in case you have the addon on your server and you
	extracted the contents to remove the Lua and can't use the built-in automatic workshop
	download system because it would load those Lua files. It allows you to put an addon
	in the addons folder which has been extracted and still function properly on the server
	without the bloat / lua files. Example: If you use M9K models on your own custom base,
	download the addon, extract it, remove all .Lua files, place an empty, 0kb, file in
	this directory, name it ds_#ofaddon_comments_to_remember_what_it_is.gma and clients
	will download it on joining.


//
// Old information ( from old readme on the topic )
//

Create an empty file and name it:
	ds_xxxxxxxxxx_name_of_addon_plus_any_comments.gma
	
The contents should be ANYTHING EXCLUDING NUMBERS... The xxxxxxxx should be the number id of the 
gma file as it is typically downloaded to the server, such as ds_128089118.gma for M9K Assault Rifles. 
Give the file a name and description to remember it by. For the M9K Assault Rifles addon, I extracted 
the original GMA file and deleted all Lua content, then I made an empty file called 
ds_128089118_m9k_ars_lua.gma to remind me that addon 128089118 is an M9K addon for Assault Rifles and 
that I removed the lua from it. 

Using my resources system, that workshop file will automatically download to the clients that have
downloads enabled.

So, any GMA file you want to extract ( in case you do NOT want the Lua to load, or you want to
edit the Lua, etc... ), make sure the GMA is NOT in your collection for the server to re-download,
and for the client to download. By placing a BLANK file