//
// Some examples of using the Query Builder - Josh 'Acecool' Moser
//
// NOTE: The other scripts in this folder show certain examples of an old method of extracting data
// from sv.db, and an example of a console command to build a query to update all accounts with missing
// information while NOT updating information that already exists with step-by-step comments.
//
// This file will cover what the calls do, and how they work together...
//


//
// EXAMPLE 1 - Basic Select Query ( Default query type is SELECT ). We don't need to set the type; but it could
// be good practice in case I decide to change it ( probably won't ).
//

// We must create a NEW query ( I may add a Reset option so old query objects can be reset and reused instead of
// needing to create a new for each... )
// We CAN NOT build this query as is because it contains no information other than the type of query.
// We must add tables to the query...
local _q = query:New( );

// Select is the default type; we really only need to call this if we want to update or insert.
// query:SetQueryType is an alias of SetType.
-- _q:SetType( QUERY_SELECT || QUERY_UPDATE || QUERY_INSERT );

// Adding a table is relatively easy, there are some arguments that must be known.
// If you use a table for columns and want to update the data, be sure to have just as many values to match
// the number of columns. You can use "*" or ignore the column argument to select ALL columns.
// query:AddTable( String table_name [, String OR Table columns[ , String OR Table values { for insert / update } ] ] );
_q:AddTable( "example" ); -- Will select ALL because second argument is omitted. could also use "*"

// Create the query...
// This will return "SELECT * FROM example"; The reason I use the table as a prefix is to allow unlimited
// joins without needing to run logic just to add a, b, c... etc... Simplifies logic.
local _query = _q:Build( );


//
// EXAMPLE 2 - Basic Select Query with a WHERE clause, and a LIMIT.
// I'll only add comments to NEW or CHANGED calls.
//
local _q = query:New( );
_q:AddTable( "example" );

// Adds a WHERE clause to the query so that only rows that'll be returned will have specific data
// in a column. ALL INPUT IS ESCAPED. No need to add the SQLStr, it's done for you.
// The _logic argument is there if you add more than 1 WHERE clause so that it can be inclusive or
// exclusive in terms of identifying rows to be returned.
// query:AddWhere( String _table, String _column, <T> _value [, String[ ANG / OR ] _logic ] )
_q:AddWhere( "example", "steamid", Entity( 1 ):SteamID( ) );

// Adds a LIMIT clause to the query so that a maximum of 1 row can be selected. LIMIT can also be used
// for PAGINATION ( Displaying data on a page by using LIMIT start, end ); I may bring my pagination
// logic over from my CMS if there is enough request for it.
// The way it works is: One digit = limit x results.
// Two numbers: The first number is the starting point, the second is how many results to limit from that first numbers point.
// query:AddLimit( Number _limit [, Number _limit_end ] )
_q:AddLimit( 1 );

// This will return: "SELECT * FROM example WHERE example.steamid=<SQLStr( Entity( 1 ):SteamID( ) )> LIMIT 1"
// or, as an example after processing: "SELECT example.* FROM example WHERE example.steamid=`STEAM_0:1:4173055` LIMIT 1"
// the < > is just to depict that the code is executed. ALL variables / values are AUTOMATICALLY escaped for you!
// No input data is ever trusted, this helps you stay secure.
local _query = _q:Build( );


//
// EXAMPLE 3 - Basic Select Query with a WHERE clause, and a LIMIT.
// Just to show how short it is without comments. Same as above.
//
local _q = query:New( );
_q:AddTable( "example" );
_q:AddWhere( "example", "steamid", Entity( 1 ):SteamID( ) );
_q:AddLimit( 1 );
local _query = _q:Build( );


//
// EXAMPLE 4 - Basic JOIN Query ( Essentially connecting two tables together to return a result
// as if both tables were only 1; meaning all data from both tables in one returned table joined
// by the column we match between the two ).
// Each extra table that is added will need a join, if there isn't a join for each table then it
// won't build the query with all of the data in it, or you may end up with unknown data.
//
local _q = query:New( );
_q:AddTable( "example" );
_q:AddTable( "example2" );
_q:AddWhere( "example", "steamid", Entity( 1 ):SteamID( ) );
_q:AddLimit( 1 );

// Type is required, typical is INNER join. Both table names are required, and at least one field.
// The second field is only used if the join column is named differently in the tables. Typically
// though, when connecting data between tables, it is a good idea to keep the joining column
// named the same.
// query:AddJoin( ENUM _type, String _table1, String _table2, String _field1 [, String _field2 ] )
// There are quire a few different join types. Inner Join is probably the most common.
// QUERY_JOIN_OUTER, QUERY_JOIN_FULL_OUTER, QUERY_JOIN_FULL, QUERY_JOIN_RIGHT, QUERY_JOIN_LEFT,
// QUERY_JOIN_LEFT_EXCLUDING_INNER, QUERY_JOIN_RIGHT_EXCLUDING_INNER, QUERY_JOIN_OUTER_EXCLUDING_INNER
_q:AddJoin( QUERY_JOIN_INNER, "example", "example2", "steamid" );

// Generates: "SELECT * FROM example AS example INNER JOIN example2 AS example2 ON example.steamid=example2.steamid WHERE example.steamid='STEAM_0:1:4173055' LIMIT 1"
local _query = _q:Build( );


//
// Example 5 - Basic UPDATE Query ( ALWAYS LIMIT on an update. If you don't LIMIT, and you make a mistake
// such as forgetting the WHERE steamid="UNIQUE_STEAM_ID_OR_ROW_ID", then it'll update ALL ROWS in the entirwe
// table, royally fudging the data. I could force it, but I believe a warning should be enough; I'm already
// providing a simle solution to select, join, update, insert data so I won't put restrictions because there
// are certain occasions when updating all rows is necessary )
//
// This will add steamid64 and steamid3 data to a table which has steamid... In the other files, it shows how to
// only update if it needs it, otherwise it updates connects...
//
local _q = query:New( );
_q:SetType( QUERY_UPDATE );

// One of the helper-functions added.. Input any form of SteamID ( 32, 64, 3 / U ) and it outputs in EXACTLY
// that order, all of the time. Super useful; eliminates logic checks to see what type an id is..
local _steamid, _steamid64, _steamid3 = util.TranslateSteamID( Entity( 1 ):SteamID( ) );
_q:AddTable( "example", { "steamid64", "steamid3" }, { _steamid64, _steamid3 } );
_q:AddWhere( "example", "steamid", _steamid );
_q:AddLimit( 1 );

// Generates: "UPDATE example AS example SET example.steamid64='76561197968611839', example.steamid3='[U:1:8346111]' WHERE example.steamid='STEAM_0:1:4173055' LIMIT 1"
local _query = _q:Build( );


//
// That's it for now. Multi-table updates ARE supported. Play around with it. Many more features are going to bew
// added such as Pivot Table / Cross Tab which will ensure key/value tables are easily accessible. The whole point
// of this is to simplify queries / allow users to prepare complex queries with a few simple calls. I recommend at
// least a basic understanding of queries, how they operate, the dangers of using them incorrectly ( wiping data... ),
// etc before putting new queries in a live environment... Always try the queries out first, you can generate the queries
// and then try them out in phpMyAdmin if you're unsure... See what is returned, and if it is correct then do it!
//
// Many more tools like this are on the way. Many more features are on the way. Stay tuned!
//