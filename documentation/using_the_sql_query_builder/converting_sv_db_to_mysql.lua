//
// Covers the basics to convert SQLite data in sv.db data to MySQL DB - Josh 'Acecool' Moser
//
--[[
	First, I'd suggest using a console command, or something to trigger the building of the
	data table in preparation for the conversion to MySQL insert queries...

	Auto-Refresh is an excellent tool when converting data.

	There are several different ways the data can be extracted, but we're going to use a shotgun
	method of selecting all of it, grabbing it, using minimalistic string parsing ( explode [ vs
	stripping numbers out then the alphanumerics left for _field due to the chance of a fields
	ability to contain numbers ), then building the auto-refresh compatible data-table to output
	the insert queries to transfer the data to MySQL.

	Unique ID is a one-way hash of util.CRC( "gm_<STEAMID>_gm" ); I'd recommend having a steamid
	column in the MySQL DB to be populated as players join instead of relying only on UniqueID.
	This makes it easier to identify players using STEAM API, Grabbing essential information,
	converting the id to SteamID64, For ban checks, etc...

	The whole process is to isolate relational data to the parent to make query building quick and easy.

	For the output of SQL for the MySQL DB, it will be assumed that uniqueid will be a column,
	along with all of the other output data in ONE TABLE. ADDITIONALLY, steamid will be inferred as well.
	
	Storing all information in one table isn't always the best solution, but for the sake of simplicity 
	in this write-up, it'll be done
	this way.

]]

// Output table
local SQL_OUTPUT_TABLE = "accounts";

// Auto-refresh compatible table
SQL_CONVERT_RESULTS = SQL_CONVERT_RESULTS || { };

//
// Our Lua table builder, restructures pdata into different format
//
function BuildSQLResultsForConversion( )
	// Our search query
	local _q = sql.Query( "SELECT * FROM playerpdata" );

	// Error checking...
	if ( !istable( _q ) ) then ErrorNoHalt( "_q didn't return a table..." ); return; end

	// Process results
	for i = 1, #_q do
		// The current row we're looking at..
		local _row = _q[ i ];

		// Data is stored as .value for playerpdata, no additional parsing is necessary...
		local _data = _row.value;

		// Players UniqueID and column-name match this pattern: [0-9]+\[[a-zA-Z_0-9]+\] or 123456[entities_inventory]
		// Thereforce, instead of using string.gsub to strip non-numbers out for the uniqueid and strip numbers and special
		// characters out for the field-name, we'll use an explode on [ so that [ 1 ] will be the unique id, and [ 2 ] will
		// be the field-name plus one character on the end ] which we just sub out from 1 to len - 1. This seemed to be the
		// most obvious / simplest way with lowest chance of failure..
		local _toexplode = _row.infoid;
		local _exploded = string.Explode( "[", _toexplode ); -- explode at [, leaving ] as byproduct to _field.

		// UniqueID is easy pickings
		local _uniqueid = _exploded[ 1 ]; -- The numbers

		// Returns the field -1 which gets rid of ] left over from explode
		local _field = string.sub( _exploded[ 2 ], 1, string.len( _exploded[ 2 ] ) - 1 );

		// Now that we have our data, our field, and our unique id all separated, build a table...
		// Inititialize if not initialized
		if ( !SQL_CONVERT_RESULTS[ _uniqueid ] ) then SQL_CONVERT_RESULTS[ _uniqueid ] = { }; end

		// Update the table.
		SQL_CONVERT_RESULTS[ _uniqueid ][ _field ] = _data;
	end

	// The original table:
	PrintTable( _q );

	print( "-VS-" );

	// The new table:
	PrintTable( SQL_CONVERT_RESULTS );

	// Create the queries, newline delimited.
	local _queries = "";
	for _uniqueid, _columns in pairs( SQL_CONVERT_RESULTS ) do
		// Query header...
		_queries = _queries .. "INSERT INTO " .. SQL_OUTPUT_TABLE .. " SET steamid='', uniqueid=" .. SQLStr( _uniqueid ) .. "";

		// Columns to add...
		for _columnname, _data in pairs( _columns ) do
			_queries = _queries .. ", " .. _columnname .. "=" .. SQLStr( _data );
		end

		// End it...
		_queries = _queries .. ";\n"
	end

	// Print for verification
	print( _queries );

	// Make sure the folder exists
	local _folder = "database/";
	if ( !file.Exists( _folder, "DATA" ) ) then
		file.CreateDir( _folder );
	end

	// Filename, excluding extension
	local _filename = "queries";

	// Write to file located in : garrysmod/data/database/queries.txt
	file.Write( _folder .. _filename .. ".txt", _queries );
end

//
// Put in a concommand to control when it is executed...
// Maybe have the prints change to _output = _output .. _val; to save the SQL queries to a table...
//
BuildSQLResultsForConversion( );


//
// When converting sv.db to MySQL DB or so, you will need to build off UniqueID most likely and insert
// relevant user-data into the database as users join because you can't unhash UniqueID.
//
// Read example_updating_database_table_with_extra_information.lua to see how we use the SQL Query Builder
// to easily add / update data to a table as users join.
//