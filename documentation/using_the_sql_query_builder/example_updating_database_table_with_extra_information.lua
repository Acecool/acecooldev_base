//
// Simple command which updates an entire database of players information by ensuring
// SteamID64, SteamUID, UniqueID are all in the database by converting the SteamID32
// to the other information, then comparing to row-data. Excellent example of the power
// and simplicity of the SQL Query Builder.
//

//
// Instead of a concommand, this would likely be a player_connect game-event hook, or PasswordCheck hook, etc.
// Additionally, instead of looping through all accounts, it'd likely have a WHERE in the first select query to only
// select the person joining. Instead of the for k, v in pairs, we'd remove that. k in database:GetValue would become
// a 1, and everything else would essentially be the same. We'd still compare data to see if we even need to update
// the account, or not. This is a good place to add 1 to user_connects counter if you keep track of that.
// I do, and I put that in the _update_rows / _update_data variable initialization.
//
concommand.Add( "dev_updateaccounts", function( _p, _cmd, _args )
	// Create a new query
	local _q = query:New( );

	// Add a table to the query ( CONST defined as accc_user_accounts, add 4 columns to select / work with
	_q:AddTable( DATABASE_USER_ACCOUNTS_TABLE, { "steamid", "steamid64", "steamuid", "uniqueid" } ); // , "user_connects"

	// Build the query as is ( by default, it is a SELECT query )
	local _query = _q:Build( );

	// Run it through your database wrapper; mine supports TMySQL and MySQLOO as a fallback
	DATABASE:Query( _query, function( _row )
		print( "DATA: ", #_row )

		if ( #_row > 0 ) then
			for k, v in pairs( _row ) do
				// Grab the data per row; this supports TMySQL and MySQLOO; and any other because it supports numerical and string keys
				local _data = {
					steamid 	= database:GetValue( _row, k, 1, "steamid" ); -- _row is our data-source, k is the row number, 1 is the first select column, and "steamid" is the name of the column
					steamid64 	= database:GetValue( _row, k, 2, "steamid64" );
					steamuid 	= database:GetValue( _row, k, 3, "steamuid" );
					uniqueid 	= database:GetValue( _row, k, 4, "uniqueid" );
					-- connects 	= database:GetValue( _row, k, 5, "user_connects" );
				};

				// Don't update the bot row
				if ( _data.steamid == "BOT" ) then continue; end

				// Grab 32, 64, U steamids
				local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _data.steamid );

				// Generate uniqueid from 32
				local _uniqueid = util.CRC( "gm_" .. _steamid .. "_gm" );

				// The beauty of the query builder; easily add data that we need to add / update
				local _update_rows = { }; // local _update_rows = { "user_connects" };
				local _update_data = { }; // local _update_data = { _data.connects + 1 };


				// So, if the row for 64 is nil, or blank, lets add it so we can update the user account row
				if ( !_data.steamid64 || _data.steamid64 == "0" ) then
					table.insert( _update_rows, "steamid64" );
					table.insert( _update_data, _steamid64 );
				end

				// if U is nil or empty, add it to the list of data to update the user account row with
				if ( !_data.steamuid || _data.steamuid == "" ) then
					table.insert( _update_rows, "steamuid" );
					table.insert( _update_data, _steamuid );
				end

				// If UniqueID doesn't exist, add it to the list
				if ( !_data.uniqueid || _data.uniqueid == "0" ) then
					table.insert( _update_rows, "uniqueid" );
					table.insert( _update_data, _uniqueid );
				end

				// I left out 32 in this, because my account table used 32 by default. In some cases, such as converting
				// playerpdata from sv.db to MySQL DB, you'll end up using uniqueid as a starting block. You'd need to
				// insert other data as it became available as the player connected such as SteamID, etc... This system
				// makes it EASY to do...

				// If we're to update, do so; otherwise skip to the next account...
				if ( #_update_rows < 1 ) then continue; end // We'd use return; if we're only updating 1 if we remove the loop.

				// Create a new query ( We could do a check to see if the update rows / data have data before doing this )
				local _q = query:New( );

				// This query will be to UPDATE the row
				_q:SetType( QUERY_UPDATE );

				// The primary table, accc_user_accounts, _update_rows is actually the column list, and _update_data is the
				// data we're matching with the columns
				_q:AddTable( DATABASE_USER_ACCOUNTS_TABLE, _update_rows, _update_data );

				// We need to target only the user-account in question, so we add a WHERE column and data. The data is auto-
				// -matically escaped.
				_q:AddWhere( DATABASE_USER_ACCOUNTS_TABLE, "steamid", _data.steamid );

				// Always add a LIMIT when updating tables; if you don't add a LIMIT, and didn't have a WHERE, then ALL user
				// accounts would get this information...
				_q:AddLimit( 1 );

				// Generate the query string
				local _query = _q:Build( );

				// Output the query string, and execute it.
				print( "EXISTING ACCOUNT UPDATE: ", _query );
				DATABASE:Query( _query );
			end
		end
	end );
end );