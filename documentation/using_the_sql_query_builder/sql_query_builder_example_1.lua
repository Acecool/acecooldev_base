//
//	- Josh 'Acecool' Moser
//


//
// Database DATA ( structure is at bottom; just put data up here to show data is normalized.. Only mandatory
// data is in acecool_accounts, the BOT and RCON account don't have much use for some of the data; rubies,
// first/last login may even be moved too because not everyone will have them or it'll be used once, ie archived... )
//
// It is possible to add a query within a query which could automatically grab the enum_key names, but typing
// it out once when setting up the schema is worth the time saved from having a query within a query....
//
-- Dumping data for table `acecool_accounts`
INSERT INTO `acecool_accounts` (`account_id`, `account_username`, `account_level`, `account_rubies`, `account_connects`, `account_playtime`, `account_firstlogin`, `account_lastlogin`, `account_steamid`, `account_steamid64`, `account_steamid3`) VALUES(-1, 'RCON', 9000, 0, 0, 0, 0, 0, 'RCON', 'RCON', 'RCON');
INSERT INTO `acecool_accounts` (`account_id`, `account_username`, `account_level`, `account_rubies`, `account_connects`, `account_playtime`, `account_firstlogin`, `account_lastlogin`, `account_steamid`, `account_steamid64`, `account_steamid3`) VALUES(0, 'BOT', 1, 0, 0, 0, 0, 0, 'BOT', 'BOT', 'BOT');
INSERT INTO `acecool_accounts` (`account_id`, `account_username`, `account_level`, `account_rubies`, `account_connects`, `account_playtime`, `account_firstlogin`, `account_lastlogin`, `account_steamid`, `account_steamid64`, `account_steamid3`) VALUES(1, 'Acecool', 9000, 0, 1852, 191809, 1368849186, 1409665308, 'STEAM_0:1:4173055', '76561197968611839', '[U:1:8346111]');

-- Dumping data for table `acecool_account_data`
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(1, 1, 1, 'acecoolcompany@gmail.com');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(2, 0, 2, 'Bot');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(3, 0, 3, 'Robotius');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(4, 0, 4, 'Birthday');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(5, 0, 5, 'Country');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(6, 0, 6, 'State');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(7, 0, 7, 'City');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(8, 0, 8, 'Coppa User');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(9, 0, 9, 'Coppa E-Mail');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(10, 0, 10, 'Coppa Statement');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(11, 0, 11, 'Zip');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(12, 0, 12, 'Street');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(13, 0, 13, 'Coppa Signature');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(14, 0, 14, 'Coppa Guardian');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(15, 0, 15, 'Whitelisted Until ( os.time( ) ); 0 for permanent admission, -1 for banned from whitelist events...');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(16, 1, 16, '3175329094');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(17, 1, 1, 'acecool@acecoolco.com');
INSERT INTO `acecool_account_data` (`account_data_id`, `account_id`, `account_data_key`, `account_data_value`) VALUES(18, 0, 1, 'webmaster@acecoolco.com');

-- Dumping data for table `acecool_account_enums`
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(1, 'email');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(2, 'first_name');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(3, 'last_name');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(4, 'birthday');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(5, 'address_country');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(6, 'address_state');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(7, 'address_city');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(8, 'coppa_user');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(9, 'coppa_email');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(10, 'coppa_statement');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(11, 'address_zip');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(12, 'address_street');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(13, 'coppa_signature');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(14, 'coppa_guardian');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(15, 'whitelist_expiry');
INSERT INTO `acecool_account_enums` (`account_enum_id`, `account_enum_key`) VALUES(16, 'garrysmod_uniqueid');



//
// Query Builder using Pivot...
//
local _q = query:New( );

// SELECT * FROM acecool_accounts ( used for base table, the pivot uses some of this data through the joins to use info )
_q:AddTable( "acecool_accounts", "*" );

// AddPivot( enum_table, data_table, enum_friendly_name, data_value, faux_columns_from_keys
_q:AddPivot( "acecool_account_enums", "acecool_account_data", "account_enum_key", "account_data_value", {
	"email"; "first_name"; "last_name"; "birthday"; "whitelist_expiry"; "garrysmod_uniqueid";
	"address_country"; "address_state"; "address_city"; "address_zip"; "address_street";
	"coppa_user"; "coppa_email"; "coppa_statement"; "coppa_signature"; "coppa_guardian";
} );

// We add a join from acecool_accounts to acecool_account_data via account_id on both tables
_q:AddJoin( QUERY_JOIN_INNER, "acecool_accounts", "acecool_account_data", "account_id" );

// We add another join from acecool_account_data to acecool_account_enums via
//	account_data_key from acecool_account_data -- account_data_key == account_enum_id; -- going to change that in the example to make it "simpler"
//	to account_enum_id on acecool_account_enums
_q:AddJoin( QUERY_JOIN_INNER, "acecool_account_data", "acecool_account_enums", "account_data_key", "account_enum_id" );

// This WHERE ensures we're only pulling information from the proper account... If a client joined, we could just as easily use
// any version of steamid here ( and account_id will still be available for the joins above )
_q:AddWhere( "acecool_accounts", "account_id", 0 );

// A limit could be used, but isn't necessary because the pivot merges everything into one row. and only one account row
// can ever be created for a user...

// Put all of the created strings together
local _query = _q:Build( );


//
// And without comments:
//
local _q = query:New( );
_q:AddTable( "acecool_accounts", "*" );
_q:AddPivot( "acecool_account_enums", "acecool_account_data", "account_enum_key", "account_data_value", {
	"email"; "first_name"; "last_name"; "birthday"; "whitelist_expiry"; "garrysmod_uniqueid";
	"address_country"; "address_state"; "address_city"; "address_zip"; "address_street";
	"coppa_user"; "coppa_email"; "coppa_statement"; "coppa_signature"; "coppa_guardian";
} );
_q:AddJoin( QUERY_JOIN_INNER, "acecool_accounts", "acecool_account_data", "account_id" );
_q:AddJoin( QUERY_JOIN_INNER, "acecool_account_data", "acecool_account_enums", "account_data_key", "account_enum_id" );
_q:AddWhere( "acecool_accounts", "account_id", 0 );
local _query = _q:Build( );



//
// and behold...... The pivot created most of the code because the inline function needs to ensure if there is more than 1
// email, name, etc associated, that all of them will be pulled and displayed.
//
// 2698 characters over 29 lines... Tell me you'd like to type that out each time you want to write a new query???
// You could even have the system generate it, then use it instead of recreating it each time... Either way, it is
// still better than typing it out or creating new complex queries from scratch.
//



//
// Using account 0, the bot...
//
[1]
	[account_connects] 		= 0
	[account_data_id] 		= 2
	[account_data_key] 		= 2
	[account_data_value] 	= Bot
	[account_enum_id] 		= 2
	[account_enum_key] 		= first_name
	[account_firstlogin] 	= 0
	[account_id] 			= 0
	[account_lastlogin] 	= 0
	[account_level] 		= 1
	[account_playtime] 		= 0
	[account_rubies] 		= 0
	[account_steamid] 		= BOT
	[account_steamid3] 		= BOT
	[account_steamid64] 	= BOT
	[account_username] 		= BOT
	[address_city] 			= City
	[address_country] 		= Country
	[address_state] 		= State
	[address_street] 		= Street
	[address_zip] 			= Zip
	[birthday] 				= Birthday
	[coppa_email] 			= Coppa E-Mail
	[coppa_guardian] 		= Coppa Guardian
	[coppa_signature] 		= Coppa Signature
	[coppa_statement] 		= Coppa Statement
	[coppa_user] 			= Coppa User
	[email] 				= webmaster@acecoolco.com
	[first_name] 			= Bot
	[garrysmod_uniqueid] 	=
	[last_name] 			= Robotius
	[whitelist_expiry] 		= Whitelisted Until ( os.time( ) ); 0 for permanent admission, -1 for banned from whitelist events...

//
// Using account 1, Acecool; 2 e-mails, not much other data...
//
[1]
	[account_connects] 		= 1852
	[account_data_id] 		= 1
	[account_data_key] 		= 1
	[account_data_value] 	= acecoolcompany@gmail.com
	[account_enum_id] 		= 1
	[account_enum_key] 		= email
	[account_firstlogin] 	= 1368849186
	[account_id] 			= 1
	[account_lastlogin] 	= 1409665308
	[account_level] 		= 9000
	[account_playtime] 		= 191809
	[account_rubies] 		= 0
	[account_steamid] 		= STEAM_0:1:4173055
	[account_steamid3] 		= [U:1:8346111]
	[account_steamid64] 	= 76561197968611839
	[account_username] 		= Acecool
	[address_city] 			=
	[address_country] 		=
	[address_state] 		=
	[address_street] 		=
	[address_zip] 			=
	[birthday] 				=
	[coppa_email] 			=
	[coppa_guardian] 		=
	[coppa_signature] 		=
	[coppa_statement] 		=
	[coppa_user] 			=
	[email] 				= acecoolcompany@gmail.com,acecool@acecoolco.com -- notice the , separating the values... GROUP_CONCAT did that...
	[first_name] 			=
	[garrysmod_uniqueid] 	= 3175329094
	[last_name] 			=
	[whitelist_expiry] 		=








	//
// Database Structure
//
-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2014 at 08:41 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ac_crime_city`
--
CREATE DATABASE IF NOT EXISTS `ac_crime_city` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ac_crime_city`;

-- --------------------------------------------------------

--
-- Table structure for table `acecool_accounts`
--

CREATE TABLE IF NOT EXISTS `acecool_accounts` (
	`account_id` bigint(20) NOT NULL AUTO_INCREMENT,
	`account_username` varchar(32) NOT NULL,
	`account_level` bigint(20) NOT NULL DEFAULT '0',
	`account_rubies` bigint(20) NOT NULL DEFAULT '0',
	`account_connects` bigint(20) NOT NULL DEFAULT '0',
	`account_playtime` bigint(20) NOT NULL DEFAULT '0',
	`account_firstlogin` bigint(20) NOT NULL DEFAULT '0',
	`account_lastlogin` bigint(20) NOT NULL DEFAULT '0',
	`account_steamid` varchar(20) NOT NULL,
	`account_steamid64` varchar(32) NOT NULL,
	`account_steamid3` varchar(20) NOT NULL,
	PRIMARY KEY (`account_id`),
	UNIQUE KEY `account_steamid` (`account_steamid`),
	UNIQUE KEY `account_steamid64` (`account_steamid64`),
	UNIQUE KEY `account_steamid3` (`account_steamid3`),
	UNIQUE KEY `account_username` (`account_username`)
) ENGINE=InnoDB	DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `acecool_account_data`
--

CREATE TABLE IF NOT EXISTS `acecool_account_data` (
	`account_data_id` bigint(20) NOT NULL AUTO_INCREMENT,
	`account_id` bigint(20) NOT NULL,
	`account_data_key` bigint(20) NOT NULL,
	`account_data_value` text NOT NULL,
	PRIMARY KEY (`account_data_id`)
) ENGINE=InnoDB	DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;


-- --------------------------------------------------------

--
-- Table structure for table `acecool_account_enums`
--

CREATE TABLE IF NOT EXISTS `acecool_account_enums` (
	`account_enum_id` bigint(20) NOT NULL AUTO_INCREMENT,
	`account_enum_key` varchar(255) NOT NULL DEFAULT '0',
	PRIMARY KEY (`account_enum_id`)
) ENGINE=InnoDB	DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;