//
// Read-Me - What and why is acecooldev_base?
//


What is this repository for?
	This repo is of a base / skeleton game-mode with many helper functions, metatable objects / classes,
	algorithms, and systems to help speed up the game-mode creation process. Much of the code is written
	in a way that it can be used in an easy way. Examples: If you use Material( ) inside of HUDPaint,
	you'll overflow / run out of memory; you can, however, use draw.GetMaterial in HUDPaint - it'll manage
	the materials for you. Also if you use ClientsideModel in a render-hook, you'll overflow / run out of
	memory; you can, however, use render.ClientsideModel without the issue. Same goes for ParticleEmitter
	vs LocalPlayer( ):GetEmitter( ); etc...


How do I get set up?
	To create an SRCDS: https://dl.dropboxusercontent.com/u/26074909/tutoring/server_srcds_steamcmd/setting_up_a_server_with_steamcmd.lua.html
	Add this repo to the server/garrysmod/gamemodes/ folder When you launch the game-mode, make sure the
	+gamemode is acecooldev_base If you use Windows, Autorefresh will be enabled; if you use Linux, you
	must type loadgm in the realm you want to update such as server-console for server/shared files, and
	GarrysMod game-window console for client/shared


Are contributions recognized?
	Credit is given where credit is due. There is a credit registration system in the game-mode meaning
	any and all features built into the game-mode has the author/editor information recorded. Using this
	database, a credits menu / scroller can be built; by default a credits.txt is generated in the server
	and also in the clients data folder as: garrysmod/data/<acecooldev_base>/credits.txt


Contribution guidelines
	This Skeleton-Game-Mode uses a strict coding-standard. If you contribute to the primary source-code,
	please follow the code-standard. If you are unable to, or want to submit something in a different
	standard, please feel free to contact me and I'll convert it over.
	
	If you are creating your own game-mode, using acecooldev_base as scaffolding, you may use any standard
	you see fit. If you uncover any issues, bugs, etc.. with the default/skeleton-base code, please report
	it, or submit a pull request to have the issue resolved. Like-wise, if a hot-fix is no-longer necessary,
	and we haven't yet changed or noticed it, please let us know!


Where can I review the code-standard?
	https://dl.dropboxusercontent.com/u/26074909/tutoring/_tutorial_quizzes/_coding_standards.lua.html


What is your story Acecool ( Eh, School? A School! )?
	I come from a well-cultured family and have had the priviledge to move around quite a bit when I was
	younger. By age 6 I spoke two languages fluently ( I read at an above-college level ) and understood
	human mortality.
	
	By age 8 I started learning a few programming languages and markup languages such as Iptscrae, C++, 
	HTML, SQL, and more. As I aged, I started toying with the idea of pursuing a game-developer role.

	I worked on many projects and mods during the Half-Life age, and made my own content-management-system
	using PHP, HTML, SQL, MySQL, etc. I asked around to see if it was possible to create a system where
	
	I could manage multiple websites using my CMS by making a system entirely in PHP to deliver
	updates-on-demand to the websites I was managing. Everyone said no, so I said yes and created the "updation"-
	-module.

	It allowed me to apply patches/updates from the site administration panel by "calling-home" to
	request the update data. It allowed for easy installation and uninstallation of plugins supplied by the parent-
	-site, and allowed updating the CMS with one click ( retaining custom changes to files the end-user applied ).
	It also allowed the end-user to restore the default file. It had a completely custom CHMOD Style permission
	system where EVERY element / plugin / page in the administration section, and on the site, used a single-digit
	to allow the user to 0, 1, 2, 3, 4, 5, 6, 7, 9 have different access rights. It allowed every possible right
	with those digits; I later added 8 as a step above 9 ( 8 gave full rights plus immunity/owner rights ).

	I lived in Canada for 3 years, Germany for 6 + a lot of time as a child traveling to and from. I lived in
	England for a brief period, Maine and a bunch of other areas. I got to see the Berlin wall fall at age 4, and
	visited Czechoslovakia before the country split into Czech Republic and Slovakia.

	I went through Hurricane Hugo at age 4 too; the motel we stayed at outside of Charleston ( we were hoping it
	would skip where we were, but it didn't ) and we had to take refuge in a door-jam as the roof was ripped off
	the building.

	I have dual citizenship, Germany and the United States of America. I served in the military while overseas.

	I then started college in the States, hoping it would be a good idea. Classes progressed too slowly, most of
	the professors though it a good idea to talk to the oldest people ( 60+ agers get to sit in classes for free )
	in the class after class and ignore the student trying to ask a question even when the student went up to the
	professor first. I was threatened to be bombed, stabbed, dissolved in a VAT of Chlorine and more by a professor,
	and after my car accident was publically humiliated by my math professor. 

	I was involved in a car accident May 7, 2011 which left me completely messed up. Any downward force applied to
	the top of my head caused my neck, chest and left arm to go numb and eventually found out that it is caused by 
	my Phrenic nerves as they are pinched; they control the heart and diaphragm... ie pulse and breathing... I wake
	up some nights not breathing, have issues walking, sitting, working on the computer and I can't do any other
	activities I used to. All of this still happens...

	I've only really left my house/room to go to the doctor and can count on one hand how frequently I've visited or 
	hung-out with a friend since the crash. I spend 20+ hours in bed, and some days wake up unable to move. I also 
	have Cornea Dystrophy, a hereditary genetic eye disorder which causes the corner to rip under stressful 
	circumstances. It typically shows up in adults 40+ years old if they have it; it has surfaced for me really early 
	due to the stress of the accident. When the corner rips it feels as though there is a hair in the eye and it won't 
	come out, you have to keep your eye closed to prevent any crap from getting inside as an infection  can cause it 
	to be lost. Your head feels as though you were hit by a freight train a few hours after the "hair" disappears, if 
	it does by the time the pain comes. 

Can I donate?
	Yes, I accept donations and they are greatly appreciated. They go towards filling my prescription for pain medication
	81 times more potent than morphine ( which only takes part of the edge of the pain away; I wish it would help more ).

	I have PayPal, Google Wallet, Square, and other options available.