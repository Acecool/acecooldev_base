//
// query Object - Prepared SQL Statements Builder
//
// NOTE: It may not be necessary to use AddTable except for the FIRST table you add. If you use * for
// SELECT * ... then it will select all columns from other tables. I did work on changing this but I
// don't recall 100% if I left it as table.* or *... That is why the example with pivot only uses 1 table
// and all fields are included.
//

	I decided to write an SQL Builder / Prepared Statement Generator because I didn't like the way
	some of the queries looked. At first, they were just blocks of text, then I abstracted some of the
	table and column names to CONSTs but it wasn't enough. They still didn't fit my idea of a "clean"
	look.

	I wrote this to "simplify" some of the advanced queries I was writing because I normalized most of
	my database; normalization is basically reducing the amount of repeated data in a database down to
	the absolute minimum. Instead of storing all account data in one table in one row when most of the
	players are never going to bother adding the information, or the information needed won't apply, such
	as COPPA, the data is broken up into parts. The accounts table stores the essential / mandatory data
	which will always be filled, then there is another table for optional data containing 4 fields / columns.
	The columns in this state wouldn't be "normalized" just yet, because the key field would still be
	repeated for each user that inserted data meaning a lot of wasted space just for words. I reduced those
	to ids too.

	In the examples I provided, it shows the 3 tables that I use for accounts.

	acecool_accounts with the following columns: account_id, account_username, account_rubies,
		account_connects, account_playtime, account_firstlogin, account_lastlogin, account_steamid,
		account_steamid64, account_steamid3

	acecool_account_data with the following columns: account_data_id account_id, account_data_key,
		account_data_value

	acecool_account_enums with the following columns: account_enum_id, account_enum_key

	So, one of the main benefits of using this SQL Builder is that with a few lines of statements, a 30+ line
	of SQL can be generated which ties all 3 tables together and returns it as though it was only one row,
	despite having n rows of account_data ( each data point is one row, accounts can have multiple data points,
	and keys are allowed to be repeated ). So, using standard Joins would only get you so far...

	Join the account_id ( primary, auto_increment ) in accounts to account_id in data; now we can return
	14, 15 rows maybe where each row has identical information, except a new account_data_key and account_data_id
	is returned per row. We could precache the account_enum table and run through n rows each time we query
	a user, or we could take it a step further and automate it. SQL is powerful when used in a normalized
	environment. So, how can we turn 15 rows into 1 while generating faux columns and merging the data into
	them? Pivots...

	AddPivot. It works similarly to a join but not quite, and while a join is still necessary to the enum
	table in addition to the data table, I found a way to "spoof"/replicate Pivots in SQL. It has been tested
	with TMySQL, so it should work with any library which uses libmysql.dll... I didn't add the resulting query
	to the example page, but with this update I'll add a few photos of why it is so important and powerful...

	PHOTOS AND QUERIES USED:
			https://dl.dropboxusercontent.com/u/26074909/tutoring/database/query_builder_example_1.png

			SELECT *
			FROM acecool_accounts
			WHERE account_id = 1;

			// This generates something similar to the above; identifiers are used in each query ( x.*, etc.. ) which simplifies some of the logic
			local _q = query:New( );
			_q:AddTable( "acecool_accounts", "*" );
			_q:AddWhere( "acecool_accounts", "account_id", 0 );
			local _query = _q:Build( );

		// Using BOT account because I filled in all the fields with dummy-data ( although the uniqueid is generated gm_BOT_gm )
		// But, if you look at it, you'll see that it returned 16 rows where only the last 2 columns change data per row...
		// Having to run through a look each time you perform a query isn't ideal...
			https://dl.dropboxusercontent.com/u/26074909/tutoring/database/query_builder_example_2.png

			SELECT *
			FROM acecool_accounts
				AS acecool_accounts
			INNER JOIN acecool_account_data
				ON acecool_accounts.account_id=acecool_account_data.account_id
			WHERE acecool_accounts.account_id = 0;

			// This generates something similar to the above; identifiers are used in each query ( x.*, etc.. ) which simplifies some of the logic
			local _q = query:New( );
			_q:AddTable( "acecool_accounts", "*" );
			_q:AddTable( "acecool_account_data", "*" );
			_q:AddJoin( QUERY_JOIN_INNER, "acecool_accounts", "acecool_account_data", "account_id" );
			_q:AddWhere( "acecool_accounts", "account_id", 0 );
			local _query = _q:Build( );


		// Adding to it, the next join which links the enumeration data to the data. This also just adds on some extra columns,
		// with the same amount of returned rows; but it is just more data to sift through...
			https://dl.dropboxusercontent.com/u/26074909/tutoring/database/query_builder_example_3.png

			SELECT *
			FROM acecool_accounts
				AS acecool_accounts
			INNER JOIN acecool_account_data
				ON acecool_accounts.account_id=acecool_account_data.account_id
			INNER JOIN acecool_account_enums
				ON acecool_account_data.account_data_key=acecool_account_enums.account_enum_id
			WHERE acecool_accounts.account_id = 0;

			// This generates something similar to the above; identifiers are used in each query ( x.*, etc.. ) which simplifies some of the logic
			local _q = query:New( );
			_q:AddTable( "acecool_accounts", "*" );
			_q:AddTable( "acecool_account_data", "*" );
			_q:AddTable( "acecool_account_enums", "*" );
			_q:AddJoin( QUERY_JOIN_INNER, "acecool_accounts", "acecool_account_data", "account_id" );
			_q:AddJoin( QUERY_JOIN_INNER, "acecool_account_data", "acecool_account_enums", "account_data_key", "account_enum_id" );
			_q:AddWhere( "acecool_accounts", "account_id", 0 );
			local _query = _q:Build( );

		// I'll show the photo first, then the query...
			https://dl.dropboxusercontent.com/u/26074909/tutoring/database/query_builder_example_4.png

			// Only the first table needed to be added because * is used; select ALL from all joined tables..
			// I may have changed it in the release to only select the table being looked at meaning AddTable
			// would need to be added for all...
			//
			// CONFIRMED. Only need 1 table if pivoting / joining and *'ed. If you select specific columns, you'll need all
			//
			local _q = query:New( );
			_q:AddTable( "acecool_accounts", "*" );
			_q:AddPivot( "acecool_account_enums", "acecool_account_data", "account_enum_key", "account_data_value", {
				"email"; "first_name"; "last_name"; "birthday"; "whitelist_expiry"; "garrysmod_uniqueid";
				"address_country"; "address_state"; "address_city"; "address_zip"; "address_street";
				"coppa_user"; "coppa_email"; "coppa_statement"; "coppa_signature"; "coppa_guardian";
			} );
			_q:AddJoin( QUERY_JOIN_INNER, "acecool_accounts", "acecool_account_data", "account_id" );
			_q:AddJoin( QUERY_JOIN_INNER, "acecool_account_data", "acecool_account_enums", "account_data_key", "account_enum_id" );
			_q:AddWhere( "acecool_accounts", "account_id", 1 );
			local _query = _q:Build( );

			// The massive query generated which creates faux columns and merges the data into them. Unfortunately a second
			// but "nested" query would need to be added if the pivot should be automated ( IE in the select, a sub-select-query
			// could be added to generate the fields and possibly automate that but I wanted to see about doing it with ONE query!
			SELECT	*,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'email', acecool_account_data.account_data_value, NULL ) ) AS email,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'first_name', acecool_account_data.account_data_value, NULL ) ) AS first_name,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'last_name', acecool_account_data.account_data_value, NULL ) ) AS last_name,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'birthday', acecool_account_data.account_data_value, NULL ) ) AS birthday,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'whitelist_expiry', acecool_account_data.account_data_value, NULL ) ) AS whitelist_expiry,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'garrysmod_uniqueid', acecool_account_data.account_data_value, NULL ) ) AS garrysmod_uniqueid,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'address_country', acecool_account_data.account_data_value, NULL ) ) AS address_country,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'address_state', acecool_account_data.account_data_value, NULL ) ) AS address_state,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'address_city', acecool_account_data.account_data_value, NULL ) ) AS address_city,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'address_zip', acecool_account_data.account_data_value, NULL ) ) AS address_zip,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'address_street', acecool_account_data.account_data_value, NULL ) ) AS address_street,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'coppa_user', acecool_account_data.account_data_value, NULL ) ) AS coppa_user,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'coppa_email', acecool_account_data.account_data_value, NULL ) ) AS coppa_email,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'coppa_statement', acecool_account_data.account_data_value, NULL ) ) AS coppa_statement,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'coppa_signature', acecool_account_data.account_data_value, NULL ) ) AS coppa_signature,
				GROUP_CONCAT( IF ( acecool_account_enums.account_enum_key = 'coppa_guardian', acecool_account_data.account_data_value, NULL ) ) AS coppa_guardian

			FROM acecool_accounts AS acecool_accounts

			INNER JOIN acecool_account_data
				AS acecool_account_data
				ON acecool_accounts.account_id = acecool_account_data.account_id

			INNER JOIN acecool_account_enums
				AS acecool_account_enums
				ON acecool_account_data.account_data_key = acecool_account_enums.account_enum_id

			WHERE acecool_accounts.account_id = '1'


*********************************
* Client:
*********************************

	// ALL query data is SERVER...

*********************************
* Server:
*********************************

	// NOT YET USED
	function query:LikeString( _like )
	function query:AddLike( _table, _column, _value, _logic )

	// Internal
	function query:WhereString( _where )

	// Internal:
	function query:AddWhere( _table, _column, _value, _logic )

	// Internal
	function query:ColumnValueString( _comma, _key, _value, _noquotes )

	// Internal:
	function query:LimitString( _limit )

	// Internal
	function query:FieldsString( _fields, _prefix )

	// Internal: SQLStr is handled for you, automatically, when using the _fields in AddTable
	function query:Escape( _string, _noquotes )

	// ENUMERATION for SetType / SetQueryType
	QUERY_SELECT
	QUERY_UPDATE
	QUERY_INSERT
	QUERY_REPLACE_INTO

	// ENUMERATION for AddJoin _type
	QUERY_JOIN_INNER
	QUERY_JOIN_OUTER
	QUERY_JOIN_FULL_OUTER
	QUERY_JOIN_FULL
	QUERY_JOIN_RIGHT
	QUERY_JOIN_LEFT
	QUERY_JOIN_LEFT_EXCLUDING_INNER
	QUERY_JOIN_RIGHT_EXCLUDING_INNER
	QUERY_JOIN_OUTER_EXCLUDING_INNER

	// Until Reset is added, you'll need to create a new instance per query you generate
	function query:New( )

	// Alias of SetType ( Default is QUERY_SELECT, so for SELECT queries, nothing needs to be done )
	function query:SetQueryType( ENUM _type )

	// Alias of SetQueryType ( Default is QUERY_SELECT, so for SELECT queries, nothing needs to be done )
	function query:SetType( ENUM _type )

	// Adds a table to the query; _values is only needed in insert / update queries.
	// Also, _values must match # of columns.. 1 to many...
	function query:AddTable( String / Table _table, String / Table _columns, String / Table _values )

	// Pivot is complicated. It takes the enumeration table ( list of keys and value-id/names ) and links it to
	// the data table ( list of keys, actual-values, account_id and primary/auto-increment row-id ) through the
	// data_value_column ( which is what matches to the enum_id ) to the enum_id column in enumeration table and
	// Merges them into the columns ( columns should match the enum_value/name of the field )
	//
	// This generates faux columns and allows you to use a normalized database to link multiple rows into a single
	// returned row with all of the data.
	//
	function query:AddPivot( String _enum_table, String _data_table, String _enum_key_column, String _data_value_column, String / Table _columns )

	// Joins two tables together. _field2 is only needed if _table2 uses a different column_name which links together with _table1 / _field1
	function query:AddJoin( ENUM _type, String _table1, String _table2, String _field1, String _field2 )

	// Sets a limit on a query. Typically 1 is used. _limit_end is used if you want to create pagination
	function query:AddLimit( _limit, _limit_end )

	// Used to generate the query; local _query = _q:Build( );
	function query:Build( )

*********************************
* Shared ( ie Client & Server ):
*********************************

	// ALL QUERY data is SERVER...