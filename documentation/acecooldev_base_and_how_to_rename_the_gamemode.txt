*******************************************************************************************

********************************* Renaming AcecoolDev_Base ********************************

*******************************************************************************************

	This covers the how-to re-name "acecooldev_base" to your game-mode name. As acecooldev_base
	is a dev-skeleton, it provides you with some basic tools to help get you started in creating
	your own game-mode such as the auto-loader, meta-table objects / classes, helper-functions,
	and much more.

	To re-name the game-mode, 3 things must be changed; to change the display name, author and
	contact information, a few more things must be done.

		1 ) Re-name the folder "acecooldev_base" to your game-mode folder-name. Please keep all
			characters lower-case and use underscores for spaces.

		2 ) Re-name acecooldev_base.txt to your game-mode folder-name.txt .. Please keep all
			characters lower-case and use underscores for spaces.

		3 ) Inside of the txt file at the very top in between ""s; re-name "acecooldev_base" to what
			you just named the txt file and the folder-name.

		4 ) Inside of the txt file, on the title line: "title"  "AcecoolDevBase"; change AcecoolDevBase
			to a simplistic version of what you'd like to identify the game-mode as.

		5 ) Inside of sh_init.lua, change GM.Name at the very top to the game-mode name that will
			be shown in the server-browser.

		6 ) Inside of sh_init.lua, change segment located in GM.Author identifiable as "___________" to
			your name so that it reads: Base by Acecool; edits/additions by <Your Name Here>. I've set
			GM.BaseAuthor to my name ( unofficial variable )

		7 ) Inside of sh_init.lua, change GM.Email to your e-mail address. I've set mine to GM.BaseEmail
			in case contact is required.

		8 ) Inside of sh_init.lua, change GM.Website to your web-address. I've set mine to GM.BaseWebsite
			in case contact is required.