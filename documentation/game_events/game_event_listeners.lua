//
// GameEvents Tutorial - Josh 'Acecool' Moser
//

//
// Game-Events for a game is located in <game-folder>/resource/gameevents.res
// Garry's Mod is based on TF2 Source Engine, so it should use TF2 gameevents, plus Garry's Mod gameevents.
//

//
// Data obtained from the following resources, in the order listed.
// https://developer.valvesoftware.com/wiki/L4D2_EMS/Appendix:_Game_Events -- Showed location
// server_directory/garrysmod/resource/modevents.res - tested events, used any that could be verified.
// server_directory/tf/resource/modevents.res - tested events, used any that could be verified.
// server_directory/tf/tf2_misc_dir.vpk/resource/modevents.res - tested events, used any that could be verified.
// server_directory/hl2/resource/gameevents.res - tested events, used any that could be verified.
// server_directory/hl2/resource/replayevents.res - tested events, used any that could be verified.
// server_directory/hl2/resource/serverevents.res - tested events, used any that could be verified.
// server_directory/garrysmod/garrysmod_dir.vpk/resource/modevents.res - tested events, used any that could be verified.
//


//
// Data Updated on the Wiki at the following locations
// http://wiki.garrysmod.com/page/gameevent/Listen with confirmed information. -- Examples given
// http://wiki.garrysmod.com/page/Game_Events -- Will add new information here as information is tested
//


//
// Review the res files here ( Garry's Mod uses events from these files, possibly others too ):
// GMod: https://dl.dropboxusercontent.com/u/26074909/tutoring/_redistributable/acecooldev_base/documentation/game_events/garrysmod/modevents.res
// HL2: https://dl.dropboxusercontent.com/u/26074909/tutoring/_redistributable/acecooldev_base/documentation/game_events/hl2/gameevents.res
// HL2: https://dl.dropboxusercontent.com/u/26074909/tutoring/_redistributable/acecooldev_base/documentation/game_events/hl2/replayevents.res
// HL2: https://dl.dropboxusercontent.com/u/26074909/tutoring/_redistributable/acecooldev_base/documentation/game_events/hl2/serverevents.res
// TF2: https://dl.dropboxusercontent.com/u/26074909/tutoring/_redistributable/acecooldev_base/documentation/game_events/tf2/modevents.res
//


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_connect. player_connect is triggered at the exact moment they join the server.
//
// SHARED-REALM -- Only the SERVER can receive this hook.
//
gameevent.Listen( "player_connect" );
hook.Add( "player_connect", "player_connect:TrackingPlayers", function( _data )
	local _name = _data.name;			// Same as Player:Nick( );
	local _steamid = _data.networkid;	// Same as Player:SteamID( );
	local _ip = _data.address;			// Same as Player:IPAddress( );
	local _id = _data.userid;			// Same as Player:UniqueID( );
	local _bot = _data.bot;				// Same as Player:IsBot( );
	local _index = _data.index;			// Same as Player:EntIndex( );
	local _p = Player( _id ); 			// The Player Entity, if valid

	// Player has connected; this happens instantly after they join -- do something..
	// Add to spawning list or players list with isspawning flag - I recommend using one list with status flags.

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_disconnect. player_disconnect is triggered at the exact moment they disconnect from
// 	the server and will ALWAYS trigger regardless of whether they time-out, are kick/banned, client crashes,
// 	they click the X, they type quit in console, etc...
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "player_disconnect" );
hook.Add( "player_disconnect", "player_disconnect:TrackingPlayers", function( _data )
	local _name = _data.name;			// Same as Player:Nick( );
	local _steamid = _data.networkid;	// Same as Player:SteamID( );
	local _id = _data.userid;			// Same as Player:UniqueID( );
	local _bot = _data.bot;				// Same as Player:IsBot( );
	local _reason = _data.reason;		// Text reason for disconnected such as "Kicked by console!", "Timed out!", etc...
	local _p = Player( _id ); 			// The Player Entity, if valid

	// Player has disconnected - this is more reliable than PlayerDisconnect
	// Remove from players list or set the flag to disconnected, and/or the spawning list in case they disconnect while spawning. - I recommend using one list with status flags.

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_spawn. player_spawn is triggered when the player initially spawns, or respawns.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "player_spawn" );
hook.Add( "player_spawn", "player_spawn:Example", function( _data )
	local _id = _data.userid;			// Same as Player:UniqueID( );

	// Called when the player spawns initially or respawns.

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_hurt. player_hurt is triggered when the player is injured or dies.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
// 	The SERVER receives 1 extra value :: Priority.
//
gameevent.Listen( "player_hurt" );
hook.Add( "player_hurt", "player_hurt:Example", function( _data )
	local _health = _data.health;						// Remaining health after injury
	local _priority = ( SERVER ) && _data.Priority || 5; // Priority ??
	local _id = _data.userid;							// Same as Player:UniqueID( );
	local _attackerid = _data.attacker;					// Same as Player:UniqueID( ); but it's the attacker id.

	// Called when the player is injured or dies.

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_hurt. player_hurt is triggered when the player is injured or dies.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
// 	The SERVER receives 1 extra value :: Priority.
//
gameevent.Listen( "player_say" );
hook.Add( "player_say", "player_say:Example", function( _data )
	local _priority = ( SERVER ) && _data.Priority || 1; // Priority ??
	local _id = _data.userid;							// Same as Player:UniqueID( ); for the speaker
	local _text = _data.text;							// The written text.

	// Called when a player writes text ( Called by the SERVER on the client AFTER the PlayerSay hook )

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: player_info. player_info is triggered when the player changes name / info.
//
// CLIENT-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "player_info" );
hook.Add( "player_info", "player_info:Example", function( _data )
	local _name = _data.name;			// Same as Player:Nick( );
	local _steamid = _data.networkid;	// Same as Player:SteamID( );
	local _id = _data.userid;			// Same as Player:UniqueID( );
	local _bot = _data.bot;				// Same as Player:IsBot( );
	local _friendsid = _data.friendsid;	// Unknown...
	local _index = _data.index;			// Same as Player:EntIndex( );
	local _p = Player( _id ); 			// The Player Entity ( Via UniqueID ), if valid
	local _p = Entity( _index ); 		// The Player Entity ( via Index ), if valid

	// Called when the player changes username / info

end


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: entity_killed. entity_killed is triggered when the player or entity dies.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "entity_killed" );
hook.Add( "entity_killed", "entity_killed:Example", function( _data )
	// Inflictor is the Object / Weapon / etc which "inflicted" / caused the damage / death.
	local _inflictor_index 	= _data.entindex_inflictor;		// Same as Weapon:EntIndex( ); / weapon used to kill victim
	local _inflictor 		= Entity( _inflictor_index );	// This is the Inflictor ENTITY

	// Attacker is the person or entity which used the inflictor against the victim
	local _attacker_index 	= _data.entindex_attacker;		// Same as Player/Entity:EntIndex( ); / person or entity who did the damage
	local _attacker 		= Entity( _attacker_index );	// This is the attacker ENTITY

	// Victim is the person / entity which died / was killed
	local _victim_index 	= _data.entindex_killed;		// Same as Victim:EntIndex( ); / the entity / player victim
	local _victim 			= Entity( _victim_index );		// This is the victim ENTITY ( the player, or entity which was killed )

	// Use ( bit.band( _damagebits, DMG_BURN ) == DMG_BURN ) as an example to test for certain types of damage.
	local _damagebits 		= _data.damagebits;				// DAMAGE_TYPE...

	// Turns PlayerDeath hook into a SHARED hook
	if ( CLIENT ) then
		hook.Call( "PlayerDeath", GAMEMODE, _victim, _inflictor, _attacker );
	end

	// Called when a Player or Entity is killed -- Add additional code below

end );


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: server_cvar. server_cvar is triggered when the cvar changes.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "server_cvar" );
hook.Add( "server_cvar", "server_cvar:Example", function( _data )
	local _name = _data.cvarname;	// ConVar Name
	local _value = _data.cvarvalue;	// ConVar Value

	// Called when the cvar changes.

end


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: break_prop. break_prop is triggered when a prop breaks.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "break_prop" );
hook.Add( "break_prop", "break_prop:Example", function( _data )
	local _id = _data.userid;			// Same as Player:UniqueID( ); - The player that broke the prop
	local _index = _data.index;			// Same as Entity:EntIndex( ); - Entity that was broken
	local _p = Player( _id ); 			// The Player Entity ( Via UniqueID ), if valid
	local _ent = Entity( _index ); 		// The Entity ( via Index ), if valid

	// Called when a prop breaks

end


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: break_breakable. break_breakable is triggered when something breakable breaks.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "break_breakable" );
hook.Add( "break_breakable", "break_breakable:Example", function( _data )
	local _id = _data.userid;			// Same as Player:UniqueID( ); - The player that broke the breakable
	local _index = _data.index;			// Same as Entity:EntIndex( ); - Entity that was broken
	local _material = _data.material;	// Same as Entity:GetMaterial( ); - Material of broken breakable
	local _p = Player( _id ); 			// The Player Entity ( Via UniqueID ), if valid
	local _ent = Entity( _index ); 		// The Entity ( via Index ), if valid

	// Called when something breakable breaks.

end


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: flare_ignite_npc. flare_ignite_npc is triggered when an NPC is ignited.
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "flare_ignite_npc" );
hook.Add( "flare_ignite_npc", "flare_ignite_npc:Example", function( _data )
	local _index = _data.index;			// Same as Entity:EntIndex( ); - the ignited NPC
	local _ent = Entity( _index ); 		// The Entity ( via Index ), if valid

	// Called when the

end


//
// This is a basic template for creating game_event listeners/hooks with the purpose of including all
// 	arguments / table variables for each game-event to make it easily known which values can be accessed
// 	and when: entity_killed. entity_killed is triggered when the .
//
// SHARED-REALM -- Both the CLIENT and SERVER can receive this hook.
//
gameevent.Listen( "entity_killed" );
hook.Add( "entity_killed", "entity_killed:Example", function( _data )
	local _victim = _data.entindex_killed;			// Same as Player:EntIndex( ); - the victim entity index
	local _inflictor = _data.entindex_inflictor;	// Same as Entity/Player:EntIndex( ); - the inflictor entity index
	local _attacker = _data.entindex_attacker;		// Same as Player/Entity:EntIndex( ); - the attacker entity index
	local _damagebits = _data.damagebits;			// Unknown
	local _victim = Entity( _index ); 				// The Player Entity ( via Index ), if valid
	local _inflictor = Entity( _index ); 			// The Weapon / Player Entity ( via Index ), if valid
	local _attacker = Entity( _index ); 			// The Player / World Entity ( via Index ), if valid

	// Called when the

end