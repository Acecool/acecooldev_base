/*******************************************************************************************
 *
 * 						Acecool Company: AcecoolDev Base for Garry's Mod
 * 				Josh 'Acecool' Moser :: CEO & Sole Proprietor of Acecool Company
 * 			  Skeleton Game-Mode with many useful helper-functions and algorithms
 *
 * 			 Copyright � 2001-2014 Acecool Company in Name & Josh 'Acecool' Moser
 *     as Intellectual Property Rights Owner & Sole Proprietor of Acecool Company in name.
 *
 * 					 RELEASED Under the ACL ( Acecool Company License )
 *
 *******************************************************************************************/

//
// First have the SERVER send both the SHARED and CLIENT file to the CLIENT, then include the shared file here on the SERVER
//
AddCSLuaFile( "sh_init.lua" )
AddCSLuaFile( "cl_init.lua" )
include( "sh_init.lua" );