//
// This is a good place to setup spawn points for the map - Josh 'Acecool' Moser
//
// When I add fileio, there'll be support to have spawns automatically read from a txt file in data with
// a tool that makes it really easy to create and remove spawn points from the map. This will still be
// a supported method to add spawn-points ( useful for addons, etc )...
//
hook.Add( "SetupMap", "SetupMap:MapSpawnPoints", function( )
	-- Details to come
end );