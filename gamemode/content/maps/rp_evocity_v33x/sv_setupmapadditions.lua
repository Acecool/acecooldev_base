//
//	- Josh 'Acecool' Moser
//
hook.Add( "SetupMapAdditions", game.GetMap( ) .. ":SetupMapAdditions", function( )
	//
	// Bathtub glitch fix ( adds an invisible prop at the bottom to prevent user from getting stuck ).
	//
	map:AddEntity( "prop_physics_multiplayer", {
		{ pos = Vector( 4600, 13618, 100 ); ang = Angle( 90, 90, 0 ); };
	}, "models/hall_suburbs/porch_railing01b.mdl", _skin, _color, { invisible = true; weld = true; }, _callback );

	//
	// Bus Stops
	//
	map:AddEntity( "prop_physics_multiplayer", {
		// Tow Yard
		{ pos = Vector( 1590, 4635, 68 ); ang = Angle( 0, 90, 0 ) };

		// Hospital
		{ pos = Vector( -6070, 9336, 70 ); ang = Angle( 0, -90, 0 ) };
		{ pos = Vector( -6330, 8647, 64 ); ang = Angle( 0, 90, 0 ) };

		//
		{ pos = Vector( 3617, 9539, 58 ); ang = Angle( 0, 90, 0 ) };

		// Subs BP
		{ pos = Vector( 9795, 12164, 58 ); ang = Angle( 0, 90, 0 ) };
		{ pos = Vector( 8661, 12838, 58 ); ang = Angle( 0, -73, 0 ) };

		// Country
		{ pos = Vector( 9715, 809, 64 ); ang = Angle( 0, 90, 0 ) };
		{ pos = Vector( 8101, 978, 64 ); ang = Angle( 0, -50, 0 ) };

		// Car Dealer
		{ pos = Vector( 4924, -4969, 64 ); ang = Angle( 0, -90, 0 ) };

		// Between small shop and clothing store
		{ pos = Vector( -5399, -6555, 72 ); ang = Angle( 0, -180, 0 ) };
	}, "models/sickness/busstop_01.mdl", _skin, _color, { weld = true; }, _callback );


	//
	// Fire Hydrants
	//
	map:AddEntity( "prop_physics_multiplayer", {
		{ pos = Vector( -5690, -7920, 72 ); ang = Angle( 0, 180, 0 ); }; // In front of Large Shop
		{ pos = Vector( -7065, -7540, 72 ); ang = ANGLE_FRONT; }; // Bank
		{ pos = Vector( -7320, -5960, 72 ); ang = ANGLE_FRONT; }; // City AM/PM
		{ pos = Vector( -7256, -4483, 72 ); ang = ANGLE_FRONT; }; // BK
		{ pos = Vector( -5690, -4332, 72 ); ang = Angle( 0, -180, 0 ); }; // Tides
		{ pos = Vector( -4789, -6930, 198 ); ang = ANGLE_FRONT; }; // City - across from medic/firestation
		{ pos = Vector( -5620, -9575, 72 ); ang = Angle( 0, 180, 0 ); }; // Skyscraper
		-- { pos = Vector(-8356, -11045, 72), ang = ANGLE_FRONT; }; // Inside of the evocity2 tides building...
		{ pos = Vector( -6428, -10638, 72 ); ang = ANGLE_FRONT; }; // Hardware store
		{ pos = Vector( -10268, -9455, 72 ); ang = ANGLE_FRONT; }; // Apartments
		{ pos = Vector( 3449, 3567, 68 ); ang = Angle( 0, -90, 0 ); }; // Powerplant

	}, "models/props/cs_assault/firehydrant.mdl", _skin, _color, { weld = true; }, _callback );


	//
	// Park in front of Apartments / Behind Nexus center-piece ( Sun Dial, horse, or whatever )
	//
	map:AddEntity( "prop_physics_multiplayer", {
		{ pos = Vector( -8968, -10148, 72 ); ang = Angle( 0, 0, 0 ) };
	}, "models/props/de_piranesi/pi_sundial.mdl", _skin, _color, { weld = true; }, _callback );
	-- "models/props_c17/statue_horse.mdl"; "models/props_c17/fountain_01.mdl";
	-- "models/props/de_piranesi/pi_sundial.mdl"; "models/props/de_piranesi/pi_orrery.mdl";
end );