//
//	- Josh 'Acecool' Moser
//
hook.Add( "SetupMapTextures", game.GetMap( ) .. ":SetupMapTextures", function( )
	//
	// This can be used as a simple 2 argument function, and it can be repeated.
	// For straight texture replacements, simply use the string. If you want
	// advanced options such as which texture ( base-texture, etc ) to modify,
	// and/or you want to generate some other textures on the fly based on the
	// currently available textures; all of it is possible with the MapTexture( )
	// data-structure. To use the advanced setting, just use
	// AddTextureReplacementEx( "string original", MapTexture( ) replacement )
	//
	// If you want to play around with these textures to see how they look:
	// Medium: https://dl.dropboxusercontent.com/u/26074909/_server/materials/AcecoolAsphaltMD.rar
	// High: https://dl.dropboxusercontent.com/u/26074909/_server/materials/AcecoolAsphaltHD.rar
	//
	// I don't see much difference between the two qualities, however when I add advanced functionality
	// into this system so that very specific things can be altered such as scale from the old texture
	// for the new one, it'll help with making it look better, then it may make a difference.
	// I plan on reskinning the entire evocity_v33 [ the only catch is the user needs to have all original
	// textures, or at least a placeholder needs to exist because they switch to error and then they can't
	// be changed without changing ALL error'd textures.
	//
	--[[ Comment out this line using forward slashes to enable these replacements...
	map:AddTextureReplacement(
		"sgtsicktextures/sicknessroad_01",		"acecool/asphalt/roadquad_v2",
		"sgtsicktextures/sicknessroad_02",		"acecool/asphalt/slabquad_v2",
		"sgtsicktextures/gunroad_01",			"acecool/asphalt/roadquad_v2",
		"sgtsicktextures/gunroad_02",			"acecool/asphalt/roadquad_v2",
		"sgtsicktextures/gunroad_03",			"acecool/asphalt/slabquad_v2",
		"sgtsicktextures/gunroad_04",			"acecool/asphalt/slabquad_v2",
		"ajacks/ajacks_road2",					"acecool/asphalt/roadquad_v2",
		"ajacks/ajacks_road4",					"acecool/asphalt/roadquad_v2",
		"ajacks/ajacks_road5",					"acecool/asphalt/roadquad_v2",
		"ajacks/ajacks_road7",					"acecool/asphalt/slabquad_v2",
		"ajacks/ajacks_road8",					"acecool/asphalt/roadquad_v2",
		"ajacks/ajacks_10",						"acecool/asphalt/slabquad_v2",
		"hall_concrete/hall_concrete11_wet",	"acecool/asphalt/slab_v2"
	); --]]

	// Example, replaces the error texture with something else...
	-- "error", "acecool/asphalt/slab_v2"
end );