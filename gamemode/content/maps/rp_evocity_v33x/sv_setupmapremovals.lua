//
// Example Configuration File for a map - entities to remove from a map - Josh 'Acecool' Moser
//
hook.Add( "SetupMapRemovals", game.GetMap( ) .. ":SetupMapRemovals", function( )
	//
	// This will remove all ENTITY:IsNPC( ) entities from the map on setup
	//
	map:RemoveEntityNPCs( true );


	//
	// This will remove all ENTITY:GetClass( ) == v, where v is the individual class names below, from the map on setup.
	// [ MUST MATCH ONLY CLASS NAME ]
	//
	// This function can be used like below, just keep adding classes delimited by comma where the last one doesn't use
	// a comma. OR you can call this function as many times as you want with only 1 class per call [ Like the Model
	// Example below ].
	//
	map:RemoveEntityByClass(
		"prop_physics", -- Prop_physics such as bouncy balls, props, etc...
		"prop_physics_multiplayer",		-- Prop_physics optimized for Multi-Player
		"prop_vehicle_prisoner_pod",	-- All chairs
		"phys_constraint",				-- Constraints are their own entities
		"phys_constraintsystem",		-- Constraints are their own entities
		"item_healthcharger",			-- Healing stations on wall
		"func_tracktrain",				-- Elevators
		"env_soundscape",				-- Map background / ambient sound ( broken and plays spooky audio anyway )
		"npc_shop",						-- My example shop entity that will be included in the near future
		"env_sporeexplosion"			-- These are slightly bugged and don't remove all the time if you use them which is why they're here.
	);

	//
	// This shows calling it many times by setting the first argument to the class-name that you want to remove
	// from the map instead of simply adding more using commas between the entries.
	//
	-- map:RemoveEntityByClass( "prop_physics" );
	-- map:RemoveEntityByClass( "prop_physics_multiplayer" );
	-- map:RemoveEntityByClass( "prop_vehicle_prisoner_pod" );
	-- map:RemoveEntityByClass( "phys_constraint" );
	-- map:RemoveEntityByClass( "phys_constraintsystem" );
	-- map:RemoveEntityByClass( "item_healthcharger" );
	-- map:RemoveEntityByClass( "func_tracktrain" );
	-- map:RemoveEntityByClass( "env_soundscape" );
	-- map:RemoveEntityByClass( "npc_shop" );
	-- map:RemoveEntityByClass( "env_sporeexplosion" );


	//
	// This will remove all ENTITY:GetModel( ) == v, where v is the individual model names below, from the map on setup.
	// [ MUST MATCH ONLY MODEL NAME ]
	// This function can be used like below, just keep adding models delimited by comma where the last one doesn't use
	// a comma. OR you can call this function as many times as you want with only 1 class per call.
	//
	map:RemoveEntityByModel( "models/props_unique/atm01.mdl" );


	//
	// This will remove all ENTITY:GetClass( ) == v1 && ENTITY:GetModel( ) == v2, where v1 is the individual class names
	// below and v2 is the individual model names below, from the map on setup. [ Must match CLASS AND MODEL ]
	//
	// This function can be used like below, just keep alternating between adding classes and models delimited by comma
	// where the last one doesn't use a comma. OR you can call this function as many times as you want with only 1
	// class and 1 model per call [ Example below ].
	//
	map:RemoveEntityByClassAndModel(
		"prop_dynamic", "models/props_equipment/gas_pump.mdl",
		"prop_dynamic", "models/props_interiors/table_picnic.mdl",
		"prop_dynamic", "models/sickness/genericsign_001.mdl",
		"prop_dynamic", "models/props_interiors/bottles_shelf.mdl",
		"prop_dynamic", "models/props/de_inferno/fountain.mdl",
		"prop_dynamic", "models/player.mdl"
	);


	//
	// This shows using it many times by setting argument 1 to the class and argument 2 to the model.
	// The above example shows alternating so every ODD argument is the class and every EVEN argument is the model.
	//
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/props_equipment/gas_pump.mdl" );
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/props_interiors/table_picnic.mdl" );
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/sickness/genericsign_001.mdl" );
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/props_interiors/bottles_shelf.mdl" );
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/props/de_inferno/fountain.mdl" );
	-- map:RemoveEntityByClassAndModel( "prop_dynamic", "models/player.mdl" );
end );