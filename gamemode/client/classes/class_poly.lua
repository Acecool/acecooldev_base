//
// Poly Class - Josh 'Acecool' Moser
//

// Poly Class containing multiple poly functions - Josh 'Acecool' Moser
//
if ( !poly ) then
	poly = { };
	poly.__index = poly;
	poly.__PolyDB = { };
end


//
// Shape enumerations
//

// This is the traditional triangle shape. All sides are equal in length, and have 3 equal 60 degree angles.
POLY_TRIANGLE_EQUILATERAL	= 0;

// This is slightly scrunched. 2 sides are equal, and there are 2 equal angles.
POLY_TRIANGLE_ISOSCELES		= 1;

// No equal sides, no equal angles.
POLY_TRIANGLE_SCALENE		= 2;

// All angles are less than 90 degrees
POLY_TRIANGLE_ACUTE			= 3;

// Has a 90 degree angle
POLY_TRIANGLE_RIGHT			= 4;

// Has one angle more than 90 degrees
POLY_TRIANGLE_OBTUSE		= 5;


//
// Removes all polys from cache ( great for when player changes resolution...
//
function poly:__PurgeCache( )
	if ( !poly.__PolyDB ) then return; end
	for k, _name in pairs( poly.__PolyDB ) do
		if ( !istable( v ) ) then continue; end
		for k, _id in pairs( v ) do
			poly.__PolyDB[ _name ][ _id ] = nil;
		end
		poly.__PolyDB[ _name ] = nil;
	end
	poly.__PolyDB = nil;
end


//
// Either retrieves cached poly or generates poly. If _update is set, then it will regenerate the poly.
//
function poly:GetPoly( _id, _name, _update, ... )
	// Ensure DB is initialized...
	if ( !poly.__PolyDB ) then poly.__PolyDB = { }; end

	// Ensure the DB for the particular poly is initialized
	if ( !poly.__PolyDB[ _name ] ) then poly.__PolyDB[ _name ] = { }; end

	//
	if ( !poly.__PolyDB[ _name ][ _id ] || _update ) then
		poly.__PolyDB[ _name ][ _id ] = poly[ _name ]( self, ... );
	end

	-- print( type( poly.MakeTriangle ), type( poly.__PolyDB ), type( poly.__PolyDB[ _name ] ), type( poly[ _name ] ), type( poly.__PolyDB[ _name ][ _id ] ) );
	print( poly.__PolyDB[ _name ][ _id ] );

	//
	return poly.__PolyDB[ _name ][ _id ];
end


//
// Simply Creates a poly, keeps it in cache and draws it. Safe to call in HUDPaint, etc...
//
function poly:Draw( _id, _name, _color, _mat, _update, ... )
	// Update the poly when it changes... Need an easy method to grab width, etc...
	-- local _update = ( math.Round( CurTime( ) ) % 1 == 0 );

	// Grab or generate the poly we want
	local _poly = poly:GetPoly( _id, _name, _update, ... );

	// Ensure alpha works correctly
	draw.NoTexture( );

	// Set the draw color
	surface.SetDrawColor( _color || COLOR_WHITE );

	// If we are going to use a material
	if ( _mat ) then
		surface.SetMaterial( _mat );
	end

	// Finally draw the poly...
	surface.DrawPoly( _poly );

	//
	return _poly;
end


//
// Mirrors a poly ( left to right )
//
function poly:Mirror( _poly )

end


//
// Flips a poly ( top to bottom )
//
function poly:Flip( _poly )

end


//
// Rotates a poly around the ORIGIN ( 0, 0 ) by default. If you want it to rotate around the center, SetOrigin needs to be called.
//
function poly:Rotate( _poly, _degrees )

end


//
// Changes Origin from TOP-LEFT ( 0, 0 )
//
function poly:SetOrigin( _poly, _x, _y )

end


//
// Makes a rectangle Poly. Changed format from start x, end x, start y, end y into easier x, y, w, h.
//
function poly:MakeRect( _x, _y, _w, _h, _textureWidth, _textureHeight )

end


//
// Makes a shiftable rectangle poly ( Shifts top-left/top-right along X-Axis AND/OR bottom row the same way )
//
function poly:MakeShiftedRect( _x, _y, _w, _h, _shiftTop, _shiftBottom )

end


//
//	2
//	/\	Creates a Isosceles Triangle - Top-left is ( 0, 0 ) [ imagine a square box with point 2 being at y = 0, x = ( width / 2 ) ]
// /__\
//1		3
//
// NOTE: May combine; pattern between ISO and Right is
// local _mIso = ( _type == ISO ) && ( _w / 2 ) || 0; -- replace + ( w / 2 ) with + _mIso
//
function poly:MakeTriangle( _x, _y, _w, _h, _rotation )

end


//
// 2
//	|\	Creates a Right Triangle - Top-left is ( 0, 0 )
//	|_\
// 1	3
function poly:MakeRightTriangle( _x, _y, _w, _h, _rotation )

end


//
//
//
function poly:MakeArrowEx( _x, _y, _w, _h )

end


//
// Creates an arrow
//
function poly:MakeArrow( _x, _y, _size, _rotation )

end


//
// Creates a circle
//
function poly:MakeCircle( _x, _y, _r, _points, _textureScale )

end


//
// Hooks
//
hook.Add( "PlayerChangedResolution", "PlayerChangedResolution:PurgePolyCache", function( _p, _w, _h, _oldw, _oldh )
	poly:__PurgeCache( );
end );



function poly:DebugPoly( )

end