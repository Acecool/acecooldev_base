//
// Creates a temp hook which draws / renders something for a specific time. - Josh 'Acecool' Moser
//
function draw.This( _time, _callback, _hook )
	return hook.AddTemp( _hook || "HUDPaint", _time, _callback );
end