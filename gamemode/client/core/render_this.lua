//
// Creates a temp hook which draws / renders something for a specific time. - Josh 'Acecool' Moser
//
function render.This( _time, _callback, _hook )
	return hook.AddTemp( _hook || "PostDrawTransulentRenderables", _time, _callback );
end