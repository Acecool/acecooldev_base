//
//  - Josh 'Acecool' Moser
//


//
// Copies _data to clipboard
//
function string.CopyToClipboard( _data )
	local _len = string.len( _data );
	if ( _len > 0 ) then
		chat.AddText( COLOR_GREEN, "Data copied to clipboard! " .. ( _len < 100 && _data || "" ) );
		print( "Data copied to clipboard:", _data );
		SetClipboardText( _data );
	else
		chat.AddText( COLOR_RED, "No data copied to clipboard!" );
	end
end