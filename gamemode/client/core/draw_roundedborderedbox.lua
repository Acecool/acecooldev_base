//
// Helper-function which gives a "border" effect by drawing two objects over each other - Josh 'Acecool' Moser
//
function draw.RoundedBorderedBoxEx( _cornerDepth, _x, _y, _w, _h, _color, _borderSize, _borderColor, _topleft, _topright, _bottomleft, _bottomright )
	// Make sure the corner-depth is set
	_cornerDepth = ( isnumber( _cornerDepth ) && _cornerDepth >= 0 ) && _cornerDepth || 0;

	// Make sure width and height are large enough to accommodate corners
	_w = ( isnumber( _w ) && _w >= _cornerDepth * 2 ) && _w || _cornerDepth * 2;
	_h = ( isnumber( _h ) && _h >= _cornerDepth * 2 ) && _h || _cornerDepth * 2;

	// Call the functions which handle drawing...
	draw.RoundedBoxEx( _cornerDepth, _x - _borderSize, _y - _borderSize, _w + _borderSize * 2, _h + _borderSize * 2, _borderColor, _topleft, _topright, _bottomleft, _bottomright );
	draw.RoundedBoxEx( _cornerDepth, _x, _y, _w, _h, _color, _topleft, _topright, _bottomleft, _bottomright );
end


//
// Helper-function which gives a "border" effect by drawing two objects over each other - Josh 'Acecool' Moser
//
function draw.RoundedBorderedBox( _cornerDepth, _x, _y, _w, _h, _color, _borderSize, _borderColor )
	// Make sure the corner-depth is set
	_cornerDepth = ( isnumber( _cornerDepth ) && _cornerDepth >= 0 ) && _cornerDepth || 0;

	// Make sure width and height are large enough to accommodate corners
	_w = ( isnumber( _w ) && _w >= _cornerDepth * 2 ) && _w || _cornerDepth * 2;
	_h = ( isnumber( _h ) && _h >= _cornerDepth * 2 ) && _h || _cornerDepth * 2;

	// Set whether or not the corners are 90-degree angles, or if they'll be curved
	local _corners = ( _cornerDepth > 0 && _cornerDepth % 2 == 0 ) && true || false;

	// Call the function which handles it...
	return draw.RoundedBorderedBoxEx( _cornerDepth, _x, _y, _w, _h, _color, _borderSize, _borderColor, _corners, _corners, _corners, _corners );
end