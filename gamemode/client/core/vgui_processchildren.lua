//
// Executes callback function for all children of input panel - Josh 'Acecool' Moser
//
function vgui.ProcessChildren( _panel, _callback )
	// If the panel isn't set, stop
	if ( !_panel ) then return; end

	// One of the rare cases where the index starts at 0 in GMod, start at 0 and go until n - 1 for children
	for i = 0, _panel:ChildCount( ) - 1 do
		// Reference the current child
		local _child = _panel:GetChild( i );

		// If the child isn't a panel, skip it.
		if ( !ispanel( _child ) ) then continue; end

		// If it is a panel, run the callback with the current child as the argument
		_callback( _child );

		// Recurse children of this child
		vgui.ProcessChildren( _child, _callback );
	end
end