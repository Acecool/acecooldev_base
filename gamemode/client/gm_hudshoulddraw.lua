//
// Optimized HUDShouldDraw - Josh 'Acecool' Moser
// Uses methods described in benchmark: https://dl.dropboxusercontent.com/u/26074909/tutoring/benchmarking_tips/benchmarking_hud_stuff.lua.html
// Create new HUDs like this to allow controlling them here: https://dl.dropboxusercontent.com/u/26074909/tutoring/vgui/proper_hud_creation.lua.html
//
// The reason it is done like this, and not just { "CHUDAmmo", ... }; with table.HasValue is because when running a
// benchmark of both 100 million times, I discovered that using table.HasValue took 15+ seconds, while running this
// way ran at 0.08 seconds. A MAJOR difference. Additionally, the benchmark only showed it running "once" instead of
// how it actually runs n^2 because for each hud / element it runs this, plus the table.HasValue is a loop so that
// becomes O( n^2 ), a huge cost. Using direct access / "hash-table" it becomes O( 1 ) for the function itself, but
// O( n ) from the bigger picture.
//
local HideHudElements = {
	// HL2 HUD
	CAchievementNotificationPanel 	= false; 	-- Achievement notifications
	CHudAmmo 						= true; 	-- Primary Ammo
	CHudBattery 					= true; 	-- Your Armor
	CHudChat 						= false; 	-- The chat area
	CHudCloseCaption 				= false; 	-- The closed-caption messages
	CHudCredits 					= false; 	-- Credits
	CHudCrosshair 					= true; 	-- Your crosshair
	CHudDeathNotice 				= false; 	-- Death notices that appear at the top right of the screen
	CHudHealth 						= true; 	-- Your Health
	CHudHintDisplay 				= true; 	-- Displays hints as seen in Half-Life 2
	CHudHistoryResource 			= true; 	-- List of items/ammunition the player recently picked up
	CHudSecondaryAmmo 				= true; 	-- Secondary Ammo
	CHudSuitPower 					= false; 	-- Most likely Auxiliary Power
	CHudTrain 						= false; 	-- Controls when using a func_train?
	CHudMessage 					= false; 	-- Messages printed to the center of the screen
	CHudMenu 						= false; 	-- Unknown
	CHudWeapon 						= false; 	-- This is the indicators that appear when you have picked up ammo or weapons.
	CHudWeaponSelection 			= false; 	-- Player weapon selection menu
	CHudGMod 						= false; 	-- HUD's produced by Garrysmod
	CHudDamageIndicator 			= false; 	-- The red trapazoids displayed on the side of the screen when damage is taken, also includes the red screen you get when died from combat.
	CHudVehicle 					= false; 	-- Control panel when entering a vehicle/crane?
	CHudVoiceStatus 				= false; 	-- Shows when other players use microphone voice chat.
	CHudVoiceSelfStatus 			= false; 	-- Shows when the local player uses microphone voice chat.
	CHudSquadStatus 				= false; 	-- Squad status panel shown when rebels join your squad
	CHudZoom 						= false; 	-- Dimming and large crosshair
	CHudCommentary 					= false; 	-- Display showing the duration and progress of the currently active commentary node
	CHudGeiger 						= false; 	-- Commentary panel
	CHudAnimationInfo			 	= false; 	-- Displays information about HUD elements, activated by the console command cl_animationinfo
	CHUDAutoAim 					= false; 	-- Unknown
	CHudFilmDemo 			 		= false; 	-- Unknown
 	CHudHDRDemo 					= false; 	-- Lost Coast HDR Demonstration HUD element
	CHudPoisonDamageIndicator 	 	= false; 	-- Panel that appears upon a player receiving poison [Neurotoxin] damage
	CPDumpPanel 		 			= false; 	-- Unknown


	// Hooks - Most likely you would really want to keep this working
	PlayerChangedResolutionLogic 	= false; 	-- Logic for PlayerChangedResolution hook call


	// Custom Game-Mode HUDs ( These are examples of hud names )
	LuaHUDStamina 					= false;
	LuaHUDHealth 					= false;
	LuaHUDArmor 					= false;
	LuaHUDDebug 					= false;
	LuaHUDVehicleSpeedometer 		= false;
};


//
// HUDShouldDraw... Set the elements you wish to hide as true above.
//
function GM:HUDShouldDraw( _name )
	return !HideHudElements[ _name ];
end