--[[---------------------------------------------------------
   Name: GM:StartChat( teamsay )
   Desc: Start Chat.

		 If you want to display your chat shit different here's what you'd do:
			In StartChat show your text box and return true to hide the default
			Update the text in your box with the text passed to ChatTextChanged
			Close and clear your text box when FinishChat is called.
			Return true in ChatText to not show the default chat text

-----------------------------------------------------------]]
//
// Detect when someone starts chatting
//
function GM:StartChat( teamsay )
	local _p = LocalPlayer( );
	networking:SyncFlag( "inchat", _p, _p, true, false, FLAG_PLAYER );
	return !_p:Alive( );
end