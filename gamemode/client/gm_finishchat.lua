//
// Detect when someone stops chatting
//
function GM:FinishChat( )
	local _p = LocalPlayer( );
	if ( !_p.Alive ) then return; end
	networking:SyncFlag( "inchat", _p, _p, false, false, FLAG_PLAYER );
	return !_p:Alive( );
end