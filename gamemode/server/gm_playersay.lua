//
//	- Josh 'Acecool' Moser
//
function GM:PlayerSay ( _p, _text, _private )
	// Do /commands _privately, !commands publicly - Josh 'Acecool' Moser
	local _bSlash = string.StartWith( _text, "/" );
	local _bExclaim = string.StartWith( _text, "!" );
	if ( _bSlash || _bExclaim ) then
		networking:SendToClient( "RunConsoleCommand", _p, string.sub( _text, 2 ) );
		if ( !_bExclaim ) then
			return "";
		end
	end

	// Allow players to chat with admins privately, or admins with themselves
	local _bAdminChat = string.StartWith( _text, "@" );
	if ( _bAdminChat ) then
		// Strip the @
		_text = string.sub( _text, 2 );
		if ( string.len( _text ) > 0 ) then
			// Send to admins and return if admins are online [ for player ]
			local _admins = networking:BroadcastToAdmins( "PlayerSay:AdminChat", _p, _text, time:Now( ) );

			// Only send to client if client isn't admin, prevents 2 messages from showing up and prevents
			// need to use cooldown
			if ( !_p:IsAdmin( ) ) then
				networking:SendToClient( "PlayerSay:AdminChat", _p, _p, _text, time:Now( ), _admins );
			end
		else
			_p:Notify( "#you_must_write_something_to_the_admins" );
		end

		return "";
	end

	return _text;
end