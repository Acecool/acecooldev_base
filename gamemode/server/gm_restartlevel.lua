//
// ChangeLevel with GM support - Josh 'Acecool' Moser
//
function GM:RestartLevel( _map, _gamemode )
	// If no map specified, use the current map
	if ( !_map || string.Trim( _map ) == "" ) then
		_map = string.lower( game.GetMap( ) );
	end

	// If gamemode specified, run the command
	if ( _gamemode ) then
		RunConsoleCommand( "gamemode", string.lower( _gamemode ) );
	end

	RunConsoleCommand( "changelevel", _map );
end