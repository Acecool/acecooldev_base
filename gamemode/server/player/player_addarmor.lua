//
//  - Josh 'Acecool' Moser
//
function META_PLAYER:AddArmor( _amount )
	local _newAmount = math.Clamp( self:Armor( ) + _amount, 0, ( self.GetMaxArmor && self:GetMaxArmor( ) || 100 ) );
	self:SetArmor( _newAmount );
end