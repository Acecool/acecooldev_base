//
// Returns whether or not the player is locked, and who they were locked by ( steamid ) - Josh 'Acecool' Moser
//
function META_PLAYER:IsMovementLocked( )
	local _data = self:GetFlag( "frozen_data", { frozen = false; frozen_by = ""; frozen_pos = vector_origin; frozen_ang = angle_zero; }, true );
	local _frozen = _data.frozen || self:GetFlag( "isfrozen", false, false, FLAG_PLAYER );
	local _frozen_by = _data.frozen_by;
	local _frozen_pos = _data.frozen_pos;
	local _frozen_ang = _data.frozen_ang;

	return _frozen, _frozen_by, _frozen_pos, _frozen_ang;
end