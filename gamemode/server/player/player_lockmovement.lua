//
// Locks the target player in place, unable to move ( even after death ) - Josh 'Acecool' Moser
//
function META_PLAYER:LockMovement( _t, _freeze, _override )
	if ( !self:IsAdmin( ) ) then Log( self, "Calling LockMovement on " .. toprint( _t ) .. " without being admin!" ); return; end
	if ( !self:IsHigherLevel( _t ) ) then Log( self, "Calling LockMovement on " .. toprint( _t ) .. " without being higher level!" ); return; end

	if ( !_freeze ) then
		_t:UnLock( );
		_t:SetMoveType( MOVETYPE_WALK );
		_t:Notify( "#unfrozen_by", self:Nick( ), self:SteamID( ) );
		_t:SetFlag( "isfrozen", nil, false, FLAG_PLAYER );
		if ( !_override ) then
			_t:SetFlag( "frozen_data", nil, true, FLAG_PLAYER );
		end
	else
		_t:Lock( );
		_t:SetMoveType( MOVETYPE_NOCLIP );
		_t:Notify( "#frozen_by", self:Nick( ), self:SteamID( ) );
		_t:SetFlag( "isfrozen", true, false, FLAG_PLAYER );
		if ( !_override ) then
			_t:SetFlag( "frozen_data", { frozen = true; frozen_by = self:EntIndex( ); frozen_pos = _t:GetPos( ); frozen_ang = _t:GetAngles( ); }, true, FLAG_PLAYER );
		end
	end

	return true;
end