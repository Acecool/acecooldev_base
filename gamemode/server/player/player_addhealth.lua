//
// Simple helper to add health to a player ( supports - values too, but doesn't handle death; TakeDamage / TakeDamageInfo should be used for that ) - Josh 'Acecool' Moser
//
function META_PLAYER:AddHealth( _amount )
	local _newAmount = math.Clamp( self:Health( ) + _amount, 0, self:GetMaxHealth( ) );
	self:SetHealth( _newAmount );
end