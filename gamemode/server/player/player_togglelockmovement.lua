//
// Toggles movement lock on target - Josh 'Acecool' Moser
//
function META_PLAYER:ToggleLockMovement( _t )
	local _frozen, _frozen_by, _frozen_pos, _frozen_ang = _t:IsMovementLocked( );
	if ( _frozen ) then
		self:LockMovement( _t, false );
	else
		self:LockMovement( _t, true );
	end

	return true;
end