//
// Modified SetPlayerSpeed to allow different options, and access to all speeds - Josh 'Acecool' Moser
//
function GM:SetPlayerSpeed( _p, _options, _run )
	// Backwards compatibility.
	if ( !istable( _options ) && ( _options || _run ) ) then
		// In place of _walk
		if ( _options ) then
			_p:SetWalkSpeed( _options )
		end

		if ( _run ) then
			_p:SetRunSpeed( _run );
		end
	else
		// How powerful our jump should be
		if ( _options.jump_power ) then
			_p:SetJumpPower( _options.jump_power );
		end

		// How fast to move when not running
		if ( _options.walk_speed ) then
			_p:SetWalkSpeed( _options.walk_speed );
		end

		// How fast to move when running
		if ( _options.run_speed ) then
			_p:SetRunSpeed( _options.run_speed );
		end

		// Multiply move speed by this when crouching
		if ( _options.crouched_walk_speed ) then
			_p:SetCrouchedWalkSpeed( _options.crouched_walk_speed );
		end

		// How fast to go from not ducking, to ducking
		if ( _options.duck_speed ) then
			_p:SetDuckSpeed( _options.duck_speed );
		end

		// How fast to go from not ducking, to ducking
		if ( _options.unduck_speed ) then
			_p:SetUnDuckSpeed( _options.unduck_speed );
		end
	end
end

/* EXAMPLE USAGE
	hook.Call( "SetPlayerSpeed", GAMEMODE, _p, {
		walk_speed = PLAYER_WALK_SPEED;
		run_speed = PLAYER_RUN_SPEED;
		jump_power = PLAYER_JUMP_POWER;
		crouched_walk_speed = PLAYER_CROUCHED_WALK_SPEED;
		duck_speed = PLAYER_DUCK_SPEED;
		unduck_speed = PLAYER_UNDUCK_SPEED;
	} );
*/