//
// Called when the weapon is picked up... Link the private data to the client.
//
function GM:WeaponEquip( _w, _p )
	if ( !IsValid( _p ) || !_p:IsPlayer( ) ) then return; end

	data:LinkData( _p, _w );
end