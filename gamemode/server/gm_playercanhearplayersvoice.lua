//
// - Josh 'Acecool' Moser
// Returns: boolean canHear, boolean use3D
function GM:PlayerCanHearPlayersVoice( _listener, _speaker )
	// Is LoopBack enabled, if not continue otherwise shut it down... print( _speaker:Get voice_loopback )
	if ( _listener == _speaker ) then return false, false; end

	//
	if ( !IsValid( _listener ) || !IsValid( _speaker ) ) then return false, false; end

	// Bots / NPCs won't be talking through these channels, ignore them...
	if ( _listener:IsNPC( ) || _speaker:IsNPC( ) || _listener:IsBot( ) || _speaker:IsBot( )	) then return false, false; end

	// Is the speaker speaking? Server now uses the networked isspeaking flag to determine if player is speaking or not... ( button held down )
	if ( !_speaker:IsSpeaking( ) ) then return false, false; end

	// There will be more systems in place to reduce how many times this gets called in the future along with custom options...

	// CanHear, Use3D
	return true, true;
end