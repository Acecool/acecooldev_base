//
// Simple helper-function to create explosions with proper magnitude dispersion - Josh 'Acecool' Moser
//
--[[
	https://developer.valvesoftware.com/wiki/Explosions
	https://developer.valvesoftware.com/wiki/Env_explosion
	https://developer.valvesoftware.com/wiki/Env_physexplosion
	https://developer.valvesoftware.com/wiki/Env_ar2explosion
	https://developer.valvesoftware.com/wiki/Env_shake
	https://developer.valvesoftware.com/wiki/Env_shooter
	https://developer.valvesoftware.com/wiki/Env_spark
	https://developer.valvesoftware.com/wiki/Env_splash
 ]]

//
// util.CreateExplosion - with proper magnitude dispersement, damage, etc... - Josh 'Acecool' Moser
//
function util.CreateExplosion( _pos, _magnitude, _radius, _damage, _inflictorWeapon, _inflictorPlayer )
	// Generate the damage from the explosion.
	util.BlastDamage( _inflictorWeapon || game.GetWorld( ), _inflictorPlayer || game.GetWorld( ), _pos, _radius || _magnitude, _damage || 0 )

	// Ensure that the entire magnitude is dispensed in 100 increments/intervals. 100 is the max, but they do stack.
	local _times = math.ceil( _magnitude / 100 );
	for i = 0, _times - 1 do
		local _mag = ( _magnitude - ( i * 100 ) );

		// Generate the explosion / force of the explosion.
		local explosion = ents.Create( "env_explosion" );

		// No damage just force, push players, disorient players if pushed
		explosion:SetKeyValue( "spawnflags", 1 + 2 + 16 );

		// Mag is clamped from 1 to 100 anyway
		explosion:SetKeyValue( "iMagnitude", math.Clamp( _mag, 1, 100 ) );

		// Radius or magnitude
		explosion:SetKeyValue( "iRadiusOverride", _radius || _magnitude );

		explosion:SetPos( _pos );
		explosion:Spawn( );

		// Touch-Off
		explosion:Fire( "Explode", "", 0 );

		// Remove it from the world
		explosion:Fire( "Kill", "", 0 );
	end
end