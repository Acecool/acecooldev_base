//
// resource.AddWorkshop Override which will output data when resource.AddFile is called - Josh 'Acecool' Moser
//


//
// Init Vars, etc...
//
__RESOURCE_ADDWORKSHOP				= __RESOURCE_ADDWORKSHOP || resource.AddWorkshop;

// Save the data to a file??
resource.__save_workshop_to_file	= true;

// This holds the data...
resource.__addworkshop				= { };


//
// The override itself...
//
function resource.AddWorkshop( _id )
	// Get some debugging info starting at stack level 2; 1 refers to this function, 2 is right before that.
	local _data = debug.getinfo( 2 );

	// Output Text...
	local _text = "resource.AddWorkshop( \"%s\" ); called from file[ %s ] on line %s\n";
	local _output = string.format( _text, _id, _data.source, _data.lastlinedefined );

	// Add data to table...
	resource.__addworkshop[ _id ] = _output;

	// If we want to save to file...
	if ( resource.__save_workshop_to_file ) then
		// This'll create a timer or overwrite itself each time resource.AddWorkshop is called.
		// It'll only call 3 seconds after all resource.AddWorkshop calls have been completed.
		timer.Create( "SaveResourceAddWorkshopDataToFile", 3, 1, function( )
			local _filedata = "";
			for k, v in pairs( resource.__addworkshop ) do
				_filedata = _filedata .. v;
			end

			file.Write( "resource_addworkshop.txt", _filedata );
		end );
	end

	// Always output to console...
	debug.print( "resource.AddWorkshop", _output );

	// Call the original function...
	__RESOURCE_ADDWORKSHOP( _id );
end