//
// Super Basic structure for CanPickupWeapon... Includes WeaponEquip call for private data!!!
//
function GM:PlayerCanPickupWeapon( _p, _w )
	//
	if ( !IsValid( _w ) && !IsValid( _p ) ) then return false; end

	// This should be called on successful pickup... I will expand this to have many more
	// options such as press e to pickup, hold e to swap, 1 weapon per slot, etc...
	hook.Call( "WeaponEquip", GAMEMODE, _w, _p );

	// Null slots = physgun/physcannon; always allow...
	local _wslot = _w:GetTable( ).Slot;
	if ( !_wslot ) then
		return true;
	end

	// Pick it up!
	return true;
end