//
// Simple Helper to process a table of key-values to set on an entity - Josh 'Acecool' Moser
//
function META_ENTITY:SetKeyValues( _keyvalues )
	if ( istable( _keyvalues ) ) then
		for k, v in pairs( _keyvalues ) do
			self:SetKeyValue( k, v );
		end
	else
		error( "You must use a table where the [ key ] is the key, and the = value; is the value..." );
	end
end