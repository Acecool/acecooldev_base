//
// Simple helper to fire lock on an entity - Josh 'Acecool' Moser
//
function META_ENTITY:Lock( _time )
	self:Fire( "lock", "", _time || 0 );
end