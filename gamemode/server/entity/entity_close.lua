//
// Helper function to fire close on an entity - Josh 'Acecool' Moser
//
function META_ENTITY:Close( _time )
	self:Fire( "close", "", _time || 0 );
end