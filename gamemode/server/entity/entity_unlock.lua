//
// Simple helper to fire unlock on an entity - Josh 'Acecool' Moser
//
function META_ENTITY:UnLock( _time )
	self:Fire( "unlock", "", _time || 0 );
end