//
// Simple helper to fire open on an entity - Josh 'Acecool' Moser
//
function META_ENTITY:Open( _time )
	self:Fire( "open", "", _time || 0 );
end