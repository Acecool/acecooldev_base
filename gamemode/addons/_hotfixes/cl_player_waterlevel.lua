//
// META_PLAYER:WaterLevel( ) networking fix.
// https://github.com/Facepunch/garrysmod-issues/issues/631
//
function META_PLAYER:WaterLevel( )
	local _pos1 = self:GetPos( );
	local _halfSize = self:OBBCenter( );
	local _pos2 = _pos1 + _halfSize;
	local _pos3 = _pos2 + _halfSize;
	if ( bit.band( util.PointContents( _pos3 ), CONTENTS_WATER ) == CONTENTS_WATER ) then
		return 3;
	elseif ( bit.band( util.PointContents( _pos2 ), CONTENTS_WATER ) == CONTENTS_WATER ) then
		return 2;
	elseif ( bit.band( util.PointContents( _pos1 ), CONTENTS_WATER ) == CONTENTS_WATER ) then
		return 1;
	end

	return 0;
end