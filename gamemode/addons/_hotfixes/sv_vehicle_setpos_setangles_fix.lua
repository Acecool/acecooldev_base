//
// HotFix to allow SetPos and SetAngles on Vehicles by injection / redirecting - Josh 'Acecool' Moser
//


//
// HotFix for vehicles being unable to be moved using SetPos after they've spawned and have been moved
//
__META_ENTITY_SETPOS = __META_ENTITY_SETPOS || META_ENTITY.SetPos;
function META_ENTITY:SetPos( _pos )
	__META_ENTITY_SETPOS( self, _pos );

	if ( self:IsVehicle( ) ) then
		self:Spawn( );
	end
end


//
// HotFix for vehicles being unable to be rotated using SetAngles after they've spawned and have been moved
//
__META_ENTITY_SETANGLES = __META_ENTITY_SETANGLES || META_ENTITY.SetAngles;
function META_ENTITY:SetAngles( _ang )
	__META_ENTITY_SETANGLES( self, _ang );

	if ( self:IsVehicle( ) ) then
		self:Spawn( );
	end
end