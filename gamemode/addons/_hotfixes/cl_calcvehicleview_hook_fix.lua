//
// HOTFIX: Allows CalcVehicleView to be hook.Add'ed - Josh 'Acecool' Moser
//


//
// NOTE: If you're creating your own game-mode, you can fix this problem
// at the source ( if, more specifically, you overwrite function GM:CalcView )
//
// Change the highlighted line seen at the following URL ( in your own copy of function GM:CalcView:
-- https://github.com/garrynewman/garrysmod/blob/master/garrysmod/gamemodes/base/gamemode/cl_init.lua#L363
//
// TO ( without -- comments ):
--	if ( IsValid( Vehicle ) ) then return hook.Call( "CalcVehicleView", GAMEMODE, Vehicle, ply, view ) end
//
// Otherwise, if you're creating an addon, this CalcView hook will accomplish the same goal, just copy
// it to your server_path/garrysmod/addons/acecool/lua/autorun/client/cl_calcvehicleview_hook_fix.lua
//


//
// Allows CalcVehicleView to be hook.Add'ed
//
hook.Add( "CalcView", "CalcView:CalcVehicleViewHooker", function( _p, _pos, _ang, _fov, _znear, _zfar )
	if ( !IsValid( _p ) ) then return; end

	// We need to pass a view through...
	local _view = { };
	_view.origin 		= _pos;
	_view.angles 		= _ang;
	_view.fov 			= _fov;
	_view.znear 		= _znear;
	_view.zfar 			= _zfar;
	_view.drawviewer 	= hook.Call( "ShouldDrawLocalPlayer", GAMEMODE, _p );

	// Let the vehicle override the view
	local _v = _p:GetVehicle( );
	if ( IsValid( _v ) ) then
		return hook.Call( "CalcVehicleView", GAMEMODE, _v, _p, _view );
	end
end );