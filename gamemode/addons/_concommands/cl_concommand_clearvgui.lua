//
// Console command which processes the logic to remove all vgui elements from existence - Josh 'Acecool' Moser
//
concommand.Add( "clearvgui", function( ply, cmd, args )
	// First one removes all vgui-elements with a parent of the WorldPanel ( ALL vgui elements, essentially )

	// Reference the world panel; if you do vgui.Create( "DPanel" ); then it is parented to WorldPanel.
	local _panel = vgui.GetWorldPanel( );

	// Print out the panel
	MsgC( Color( 255, 0, 0, 255 ), _panel, "\n" );

	// Call the processor with the callback to remove the child
	vgui.ProcessChildren( _panel, function( _child )
		_child:Remove( );
	end );


	// Second one removes all vgui elements which have the parent as the HUDPanel; this doesn't work as intended...

	// Reference the HUDPanel; if you do local _panel = vgui.Create( "DPanel" ); _panel:ParentToHUD( );
	local _hud = GetHUDPanel( )

	// Print out the panel
	MsgC( Color( 255, 255, 0, 255 ), _hud, "\n" );

	// Call the processor with the callback to remove the child
	vgui.ProcessChildren( _hud, function( _child )
		_child:Remove( );
	end );

	// Disable the mouse on screen; useful if MakePopup( ) was called on a panel, or if the mouse is unlocked. Prevents mouse staying active.
	gui.EnableScreenClicker( false );
end );