//
// Helper Console Commands which go with the networking / data system - Josh 'Acecool' Moser
//
// Helps understand who knows what data... Private data is only known to the entity it belongs to
//	and any entity linked ( driver gets in vehicle, driver knows vehicle private data.. Driver gets
//	out, driver forgets vehicle private data ). Public data is known by all.
//
// The description above ONLY occur if the server sets the data... If the client sets the data, regardless
// 	if it is private or not, then only the client knows that data...
//


//
// Helper-Function to print out data we want to know about...
//
local function dev_getdata( _p, _cmd, _args, _pdata )
	local _id = string.numbers( _args[ 1 ] );

	if ( _id == -1 ) then
		local _data = data:GetFlags( _pdata );
		for k, v in pairs( _data ) do
			local _ent = Entity( k );

			print( "[ " .. k .. "\t:\"" .. _ent .. "\":\t:\"" .. ( IsValid( _ent ) && _ent:GetClass( ) || "N/A CLASS" ) .. "\":\t:\"" .. ( IsValid( _ent ) && _ent:GetModel( ) || "N/A MODEL" ) .. "\"]", "\n" .. toprint( v, nil, 1 ) );
		end
	else
		local v = data:GetFlags( _pdata )[ _id ];
		local _ent = Entity( _id );

		if ( IsValid( _ent ) ) then
			print( "[ " .. _id .. "\t:\"" .. _ent .. "\":\t:\"" .. _ent:GetClass( ) .. "\":\t:\"" .. _ent:GetModel( ) .. "\"]", "\n" .. toprint( v, nil, 1 ) );
		else
			print( "[ " .. _id .. "\t:\"NULL\":\t:\"NULL\":\t:\"NULL\"]", "\n" .. toprint( v, nil, 1 ) );
		end
	end
end


//
// Outputs public world-data - SERVER and CLIENT knows all...
//
concommand.Add( "dev_getworlddata", function( _p, _cmd, _args )
	dev_getdata( _p, _cmd, "0", false );
end );


//
// Outputs public data ( optionally for entity id ) - SERVER and CLIENT knows all...
//
concommand.Add( "dev_getdata", function( _p, _cmd, _args )
	dev_getdata( _p, _cmd, _args, false );
end );


//
// Outputs known private data ( optionally for entity id ) - SERVER knows all, clients know some...
//
concommand.Add( "dev_getpdata", function( _p, _cmd, _args )
	dev_getdata( _p, _cmd, _args, true );
end );