//
// Executes changelevel command
//
concommand.Add( "relevel", function( _p, _cmd, _args )
	if ( _p != NULL && !_p:IsSuperAdmin( ) ) then
		// Log, or something like it, will be available in the update that contains a universal admin system
		-- Log( _p, language.GetPhrase( "relevel_log", language.__default ) );
		return;
	end

	// Detect if map was entered. If so, use it, otherwise nil ( because the RestartMap will use the current map if nil is entered
	local _map = ( _args[ 1 ] && string.Trim( _args[ 1 ] ) != "" ) && string.lower( _args[ 1 ] ) || string.lower( game.GetMap( ) );

	// Detect if gamemode was entered. If so, use it, otherwise nil ( no point in "changing" gm if not entered )
	local _gamemode = ( _args[ 2 ] && string.Trim( _args[ 2 ] ) != "" ) && string.lower( _args[ 2 ] ) || nil; // string.lower( engine.ActiveGamemode( ) );

	// If gamemode specified, run the command
	if ( _gamemode ) then
		RunConsoleCommand( "gamemode", string.lower( _gamemode ) );
	end

	// Always run _map
	RunConsoleCommand( "changelevel", _map );
end );


//
// Executes map command
//
concommand.Add( "remap", function( _p, _cmd, _args )
	if ( _p != NULL && !_p:IsSuperAdmin( ) ) then
		// Log, or something like it, will be available in the update that contains a universal admin system
		-- Log( _p, language.GetPhrase( "remap_log", language.__default ) );
		return;
	end

	// Detect if map was entered. If so, use it, otherwise nil ( because the RestartMap will use the current map if nil is entered
	local _map = ( _args[ 1 ] && string.Trim( _args[ 1 ] ) != "" ) && string.lower( _args[ 1 ] ) || nil; // string.lower( game.GetMap( ) );

	// Detect if gamemode was entered. If so, use it, otherwise nil ( no point in "changing" gm if not entered )
	local _gamemode = ( _args[ 2 ] && string.Trim( _args[ 2 ] ) != "" ) && string.lower( _args[ 2 ] ) || nil; // string.lower( engine.ActiveGamemode( ) );

	// If gamemode specified, run the command
	if ( _gamemode ) then
		RunConsoleCommand( "gamemode", string.lower( _gamemode ) );
	end

	// Always run _map
	RunConsoleCommand( "map", _map );
end );