//
// By typing "install" in console, it will set up the folder-structure for this dev-game-mode.
// The folders will be located in the data/ directory on the server. Merge the structure with the game-mode.
//
local _folder_structure = {
	//
	// AcecoolDev_Base Folder Structure
	//
	"acecooldev_base/";
	"acecooldev_base/content/";
	"acecooldev_base/documentation/";
	"acecooldev_base/entities/";
	"acecooldev_base/gamemode/";

	//
	// Content Folder Structure
	//
	"acecooldev_base/content/materials/";
	"acecooldev_base/content/models/";
	"acecooldev_base/content/scripts/";
	"acecooldev_base/content/scripts/vehicles/";
	"acecooldev_base/content/sound/";

	//
	// Entities Folder Structure
	//
	"acecooldev_base/entities/effects/";
	"acecooldev_base/entities/entities/";
	"acecooldev_base/entities/weapons/";

	//
	// GameMode Folder Structure
	//
	"acecooldev_base/gamemode/addons/";
	"acecooldev_base/gamemode/client/";
	"acecooldev_base/gamemode/content/";
	"acecooldev_base/gamemode/server/";
	"acecooldev_base/gamemode/shared/";

	//
	// Client Folder Structure
	//
	"acecooldev_base/gamemode/client/_definitions/";
	"acecooldev_base/gamemode/client/classes/";
	"acecooldev_base/gamemode/client/core/";
	"acecooldev_base/gamemode/client/entity/";
	"acecooldev_base/gamemode/client/gui/";
	"acecooldev_base/gamemode/client/hooks/";
	"acecooldev_base/gamemode/client/player/";
	"acecooldev_base/gamemode/client/vehicle/";

	//
	// Content Folder Structure
	//
	"acecooldev_base/gamemode/content/events/";
	"acecooldev_base/gamemode/content/items/";
	"acecooldev_base/gamemode/content/jobs/";
	"acecooldev_base/gamemode/content/maps/";
	"acecooldev_base/gamemode/content/npcs/";
	"acecooldev_base/gamemode/content/player/";
	"acecooldev_base/gamemode/content/vehicle_attachments/";
	"acecooldev_base/gamemode/content/vehicles/";

	//
	// Server Folder Structure
	//
	"acecooldev_base/gamemode/server/_definitions/";
	"acecooldev_base/gamemode/server/core/";
	"acecooldev_base/gamemode/server/entity/";
	"acecooldev_base/gamemode/server/hooks/";
	"acecooldev_base/gamemode/server/player/";
	"acecooldev_base/gamemode/server/vehicle/";

	//
	// Shared Folder Structure
	//
	"acecooldev_base/gamemode/shared/_definitions/";
	"acecooldev_base/gamemode/shared/classes/";
	"acecooldev_base/gamemode/shared/core/";
	"acecooldev_base/gamemode/shared/entity/";
	"acecooldev_base/gamemode/shared/hooks/";
	"acecooldev_base/gamemode/shared/languages/";
	"acecooldev_base/gamemode/shared/net/";
	"acecooldev_base/gamemode/shared/player/";
	"acecooldev_base/gamemode/shared/vehicle/";
	"acecooldev_base/gamemode/shared/weapon/";

};

//
//
//
concommand.Add( "install", function( _p, _cmd, _args )
	if ( _p != NULL && !_p:IsSuperAdmin( ) ) then return; end
	for k, v in pairs( _folder_structure ) do
		if ( !file.Exists( v, "DATA" ) ) then
			print( "Creating DIR: ", v );
			file.CreateDir( v );
		end
	end
	file.Write( "acecooldev_base/readme.txt", "Merge this folder-structure into the downloaded Skeleton++ GameMode!", "DATA" )
	MsgC( Color( 0, 255, 255, 255 ), string.rep( "*", 50 ) );
	MsgC( Color( 255, 0, 255, 255 ), "\nFolder Structure set up. Merge garrysmod/data/acecooldev_base/* folder structure with gamemodes/acecooldev_base/\n" );
	MsgC( Color( 0, 255, 255, 255 ), string.rep( "*", 50 ) .. "\n" );
end );