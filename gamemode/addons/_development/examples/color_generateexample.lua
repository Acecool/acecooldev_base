//
// Shows off functionality... - Josh 'Acecool' Moser
//
if ( !META_COLOR ) then
	if ( !GAMEMODE_LOADED ) then
		MsgC( COLOR_RED, "#meta_color_unavailable" );
	end
	return;
end
function META_COLOR:GenerateExample( )
	// Local ( to GenerateExample ) function which provides a quick look at current status while we play with some functions without having to repeat this over and over.
	local function CurrentState( color )
		local output = "Color( " .. self.r .. ", " .. self.g .. ", " .. self.b .. ", " .. self.a .. " )"
		local c, m, y, k = self:ToCMYK( )
		local hex = self:ToHex( )


		MsgC( color_white, "I am this ", self, "..Color..", color_white, " in this format Color( R, G, B, A ): " .. output .. ", my Hex Code: ", Color( 0, 255, 255, 255 ), "#" .. hex, color_white, " and my C, M, Y, K: ", Color( 0, 255, 255, 255 ), c .. ", " .. m .. ", " .. y .. ", " .. k .. "\n\n" )
		print( "Back to hex: ", self:FromHex( hex ) )
	end

	// Keep a copy, because we're going to be altering some values...
	local old = Color( self.r, self.g, self.b, self.a )

	CurrentState( self );
	MsgC( color_white, "RGBA stands for Red, Green, Blue, Alpha where the first 3 are colors, and alpha controls visibility.\n\n" )
	MsgC( color_white, "CMYK stands for Cyan, Magenta, Yellow, Key where the first 3 are colors, and Key controls darkness. It is how printers typically mix colors together.\n\n" )

	self:FromHex( "FF0000" )
	MsgC( self, "I've just changed myself to RED FromHex FF0000 by using self:FromHex( \"FF0000\" )\n" )

	CurrentState( self )

	self:FromCMYK( 1, 0, 0, 0 )
	MsgC( self, "I've just changed myself to Cyan by using self:FromCMYK( 1, 0, 0, 0 )\n" );

	CurrentState( self )

	MsgC( self, "Lets try adjusting K at 0.25 incrementally increasing until 1 with Cyan left at 1:\n" )

	self:FromCMYK( 1, 0, 0, 0.25 )
	CurrentState( self )

	self:FromCMYK( 1, 0, 0, 0.5 )
	CurrentState( self )

	self:FromCMYK( 1, 0, 0, 0.75 )
	CurrentState( self )

	self:FromCMYK( 1, 0, 0, 1 )
	CurrentState( self )

	MsgC( color_white, "Notice that even though Cyan is at 1, because of how CMYK is calculated it is irrelevant because K is at 1, and when K is 1 the collor is black. So self:FromCMYK( 1, 0, 0, 1 ) essentially becomes self:FromCMYK( 0, 0, 0, 1 )\n\n" )

	// Reverting color back..
	self = old;

	MsgC( color_white, "GenerateExample has finished, self has been restored back to the original color!\n" );
	CurrentState( self )

	do return; end
	local _divider = string.rep( "=", 75 );

	//
	// CMY functionality...
	//
	--[[
		Verification KEY

		Name 		( C, M, Y, K ) 	(   R,   G,   B ) #Hex
		----------|----------------|-----------------|-------
		Black 		( 0, 0, 0, 1 ) 	(   0,   0,   0 ) #000000
		White 		( 0, 0, 0, 0 ) 	( 255, 255, 255 ) #FFFFFF
		Red 		( 0, 1, 1, 0 ) 	( 255,   0,   0 ) #FF0000
		Green 		( 1, 0, 1, 0 ) 	(   0, 255,   0 ) #00FF00
		Blue 		( 1, 1, 0, 0 ) 	(   0,   0, 255 ) #0000FF
		Yellow 		( 0, 0, 1, 0 ) 	( 255, 255,   0 ) #FFFF00
		Cyan 		( 1, 0, 0, 0 ) 	(   0, 255, 255 ) #00FFFF
		Magenta 	( 0, 1, 0, 0 ) 	( 255,   0, 255 ) #FF00FF
	]]

	print( _divider .. "\nInfo: Color -> CMYK Examples\n\nUsage: Color( r, g, b, a ):ToCMY( ) will return the CMY value in the form\nof a string: CMYK: c m y k\n\nNotes: Alpha is not transferred to CMY...\n" .. _divider );
	print( "Color\tOutput With Color\t\tCMYK: c m y k\n" .. _divider );
	print( "Black", Color( 0, 0, 0, 255 ), "", Color( 0, 0, 0, 255 ):ToCMY( ) );			-- Black
	print( "White", Color( 255, 255, 255, 255 ), Color( 255, 255, 255, 255 ):ToCMY( ) );	-- White
	print( );
	print( "Red", Color( 255, 0, 0, 255 ), "", Color( 255, 0, 0, 255 ):ToCMY( ) );		-- Red
	print( "Green", Color( 0, 255, 0, 255 ), "", Color( 0, 255, 0, 255 ):ToCMY( ) );		-- Green
	print( "Blue", Color( 0, 0, 255, 255 ), "", Color( 0, 0, 255, 255 ):ToCMY( ) );		-- Blue
	print( );
	print( "Yellow", Color( 255, 255, 0, 255 ), Color( 255, 255, 0, 255 ):ToCMY( ) );		-- Yellow
	print( "Cyan", Color( 0, 255, 255, 255 ), Color( 0, 255, 255, 255 ):ToCMY( ) );		-- Cyan
	print( "Magenta", Color( 255, 0, 255, 255 ), Color( 255, 0, 255, 255 ):ToCMY( ) );		-- Magenta
	print( );

	print( _divider .. "\nInfo: Color <- CMYK Examples\n\nUsage: Color( 0, 0, 0, a ):FromCMY( ) will alter Color to the color the\nCMYK input corresponds to\n\nNotes: CMYK contains no Alpha, therefore Alpha from Color is used or it\ndefaults to 255...\n" .. _divider );
	print( "Color\tOutput With Color\t\tCOLOR_BLACK:FromCMY( c, m, y, k )\n" .. _divider );
	print( "Black", Color( 0, 0, 0, 255 ), "", Color( 0, 0, 0, 255 ):FromCMY( 0, 0, 0, 1 ) );	-- Black
	print( "White", Color( 255, 255, 255, 255 ), Color( 0, 0, 0, 255 ):FromCMY( 0, 0, 0, 0 ) );	-- White
	print( );
	print( "Red", Color( 255, 0, 0, 255 ), "", Color( 0, 0, 0, 255 ):FromCMY( 0, 1, 1, 0 ) );	-- Red
	print( "Green", Color( 0, 255, 0, 255 ), "", Color( 0, 0, 0, 255 ):FromCMY( 1, 0, 1, 0 ) );	-- Green
	print( "Blue", Color( 0, 0, 255, 255 ), "", Color( 0, 0, 0, 255 ):FromCMY( 1, 1, 0, 0 ) );	-- Blue
	print( );
	print( "Yellow", Color( 255, 255, 0, 255 ), Color( 0, 0, 0, 255 ):FromCMY( 0, 0, 1, 0 ) );	-- Yellow
	print( "Cyan", Color( 0, 255, 255, 255 ), Color( 0, 0, 0, 255 ):FromCMY( 1, 0, 0, 0 ) );	-- Cyan
	print( "Magenta", Color( 255, 0, 255, 255 ), Color( 0, 0, 0, 255 ):FromCMY( 0, 1, 0, 0 ) );	-- Magenta
	print( );

	print( _divider .. "\nInfo: Color -> HEX Examples\n\nUsage: Color( 0, 0, 0, a ):ToHex( _bConvertWithAlpha ) will alter Color to the color the\nCMYK input corresponds to\n\nNotes: CMYK contains no Alpha, therefore Alpha from Color is used or it\ndefaults to 255...\n" .. _divider );
	print( "Color\tOutput With Color\t\tHEX\tHex Alpha\tAlpha HEX a\n" .. _divider );
	print( "Black", Color( 0, 0, 0, 255 ), "", Color( 0, 0, 0, 255 ):ToHex( ), Color( 0, 0, 0, 255 ):ToHex( true ), Color( 0, 0, 0, 255 ):ToHex( true, true, true ) );	-- Black
	print( "White", Color( 255, 255, 255, 255 ), Color( 255, 255, 255, 255 ):ToHex( ), Color( 255, 255, 255, 255 ):ToHex( true ), Color( 255, 255, 255, 255 ):ToHex( true, true, true ) );	-- White
	print( );
	print( "Red", Color( 255, 0, 0, 255 ), "", Color( 255, 0, 0, 255 ):ToHex( ), Color( 255, 0, 0, 255 ):ToHex( true ), Color( 255, 0, 0, 255 ):ToHex( true, true, true ) );	-- Red
	print( "Green", Color( 0, 255, 0, 255 ), "", Color( 0, 255, 0, 255 ):ToHex( ), Color( 0, 255, 0, 255 ):ToHex( true ), Color( 0, 255, 0, 255 ):ToHex( true, true, true ) );	-- Green
	print( "Blue", Color( 0, 0, 255, 255 ), "", Color( 0, 0, 255, 255 ):ToHex( ), Color( 0, 0, 255, 255 ):ToHex( true ), Color( 0, 0, 255, 255 ):ToHex( true, true, true ) );	-- Blue
	print( );
	print( "Yellow", Color( 255, 255, 0, 255 ), Color( 255, 255, 0, 255 ):ToHex( ), Color( 255, 255, 0, 255 ):ToHex( true ), Color( 255, 255, 0, 255 ):ToHex( true, true, true ) );	-- Yellow
	print( "Cyan", Color( 0, 255, 255, 255 ), Color( 0, 255, 255, 255 ):ToHex( ), Color( 0, 255, 255, 255 ):ToHex( true ), Color( 0, 255, 255, 255 ):ToHex( true, true, true ) );		-- Cyan
	print( "Magenta", Color( 255, 0, 255, 255 ), Color( 255, 0, 255, 255 ):ToHex( ), Color( 255, 0, 255, 255 ):ToHex( true ), Color( 255, 0, 255, 255 ):ToHex( true, true, true ) );	-- Magenta
	print( );
end


-- META_COLOR.GenerateExample( );
-- Color( 251, 182, 245, 255 ):GenerateExample( );
-- local _pink = Color( 140, 10, 140, 255 );
-- local _pink = Color( 90, 5, 90, 255 );
-- local _pink = Color( 0, 100, 0, 255 );
-- local _from = Color( 0, 0, 0, 125 ):FromHex( "#FF8040" );
-- print( _pink, _pink:ToHex( ), _pink:ToCMY( ), _from );

// 8C0A8C
// FF8040
// FF80C0
// 5A055A
// 006400