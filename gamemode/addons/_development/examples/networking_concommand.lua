//
// Shows an example of a networked concommand - Josh 'Acecool' Moser
//


//
// When the client writes: dev_spawnent in console, it'll show a list of spawnable entities by class.
// When the client hits enter, the networking system networks the console command to the server and
// executes it through the client. This allows autofill help on the client for serverside commands!
//
if ( CLIENT ) then
	networking:AddConCommand( "dev_spawnent", function( _cmd, _input )
		// Set up out list, and process the input; it needs to be trimmed because the space ( which separates args ) isn't excluded...
		_input = ( !_input || _input == "" ) && "" || string.Trim( string.lower( _input ) );
		local _list = concommand.GetAutofillEntityList( _cmd, _input, _list )

		return {
			_cmd .. " <Entity ClassName>";	// This one is to show the player what is expected
			unpack( _list );				// And this shows data based on what the user is typing
		};
	end, "[DEV] Spawns an entity, x, ( 5 feet + object size ) in front of the player." );
end