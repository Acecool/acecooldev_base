//
// Simple MOTD System - Josh 'Acecool' Moser
//


//
// Vars -- No need to make these global..
//
local MOTD_ENABLED 				= true;
local MOTD_ACCEPT_WAIT 			= 5;
local MOTD_DECLINE_CONFIRMATION = true;
-- local MOTD_HIDE_FOR_ADMINS 	= false;

local MOTD_HTML = [[<!DOCTYPE html>
<html>
	<body bgcolor="#DDDDDD">
		<div style="font-family:verdana;padding:20px;border-radius:10px;border:10px solid #80B3DF;">
			<center><b><font color="#000000">Message of the Day!</font></b></center><br />
			<b>Please read and abide by the following rules during your time on our server. We are open-minded and easy-going, work with us and we'll work with you.</b><br />
			<ul>
			<li>Try to find and report as many bugs as possible.</li>
			<li>Don't abuse other players ( e.g. Trolling, Cyber-Bullying, Racist comments, Personal Attacks, etc... )</li>
			<li>Most importantly, please have fun!</li>
			</ul>
		</div>
	</body>
</html>]];

hook.Add( "MOTD", "SimpleMOTDExampleByAcecool", function( _p )
	// No sense doing the work if the MOTD is disabled...
	if ( !MOTD_ENABLED ) then return; end

	// Our screensize
	local _w = ScrW( );
	local _h = ScrH( );

	// Create the parent window
	local _window = vgui.Create( "DPanel" );

	// Create our HTML Element ( can also use DHTML )
	local _html = vgui.Create( "HTML", _window );

	// Accept Button; We use # to use Valve language system... Update text, size and tooltip.
	local _accept = vgui.Create( "DButton", _window );
	_accept:SetText( "#Accept " .. " ( " .. MOTD_ACCEPT_WAIT .. " ) " );
	_accept:SetWide( _accept:GetWide( ) * 1.5 );
	_accept:SetTooltip( "I agree to the terms and conditions." );

	// The DoClick function; this can be written many different ways including:
	_accept.DoClick = function( )
		RunConsoleCommand( "say", "I've read and agreed to both the End User License Agreement, and the Terms of Service." );
		_window:ClosePanel( );
	end

	--[[
		// Defining it like an actual function using :.. : infers self whereas . does not and therefore must be passed as first argument ( if used )
		function _accept:DoClick( )
			RunConsoleCommand( "say", "I've read and agreed to both the End User License Agreement, and the Terms of Service." );
			_window:ClosePanel( );
		end

		// Defining like an actual function instead of defining a variable as a function; self must be passed as first argument ( if you intend on using it )
		function _accept.DoClick( self )
			RunConsoleCommand( "say", "I've read and agreed to both the End User License Agreement, and the Terms of Service." );
			_window:ClosePanel( );
		end
	]]

	// Decline Button; We use # to use Valve language system... Update text, size and tooltip.
	local _decline = vgui.Create( "DButton", _window );
	_decline:SetText( "#Decline" );
	_decline:SetWide( _decline:GetWide( ) * 1.5 );
	_decline:SetTooltip( "I do not agree to the terms and conditions and I agree to remove any and all content and associated files of this game-mode from my PC." );

	// The DoClick function; just like the accept button, this can be written in just as many ways...
	_decline.DoClick = function( )
		RunConsoleCommand( "disconnect" );
	end

	//
	// Hack to fix ClosePanel
	//
	_window.ClosePanel = function( )
		// Remove each element we created, then nullify the reference...
		_decline:Remove( ); 			_decline = nil;
		_accept:Remove( ); 				_accept = nil;
		_html:Remove( ); 				_html = nil;

		// We'll "hide" the window first, then cleanup...
		_window:SetVisible( false );
		_window:Remove( ); 				_window = nil;
	end

	// Update the size of the entire window
	_window:SetSize( _w - _w / 3, _h - _h / 3 );

	// Center the window, which can be done using PANEL:Center( ); or by calculating the position...
	_window:Center( );

	// Update the HTML Panel position to be top/left in the parent frame
	_html:SetPos( 0, 0 );

	// Update the HTML Panel size; give a little breathing room for the buttons on the bottom
	_html:SetSize( _window:GetWide( ), _window:GetTall( ) - 30 );

	// Update the accept and decline button positions
	_accept:SetPos( ( _window:GetWide( ) / 2 ) - _accept:GetWide( ), _window:GetTall( ) - 25 );
	_decline:SetPos( ( _window:GetWide( ) / 2 ) + _decline:GetWide( ), _window:GetTall( ) - 25 );

	// Populate the HTML Panel with the html we set above
	_html:SetHTML( MOTD_HTML );

	// If we enabled a mandatory wait time, and the wait time is 1 second or more
	if ( MOTD_ACCEPT_WAIT && MOTD_ACCEPT_WAIT > 0 ) then
		// then lock the accept button
		_accept:SetDisabled( true );

		// Iterate once for each second to create a count-down using timers ( could be done in Think, Tick, etc.. )
		for i = 1, MOTD_ACCEPT_WAIT do
			// Use a timer for each second of the count-down
			timer.Simple( i, function( )
				// TotalWaitTime - i = the way to count down. If it is 0, remove the counter; unlock happens elsewhere
				if ( MOTD_ACCEPT_WAIT - i == 0 ) then
					_accept:SetText( "#Accept" );
				else
					_accept:SetText( "#Accept" .. " ( " .. MOTD_ACCEPT_WAIT - i .. " ) " );
				end
			end )
		end

		// Use a timer to unlock the button
		timer.Simple( MOTD_ACCEPT_WAIT, function( )
			_accept:SetDisabled( false );
		end )
	end

	// Make the MOTD window known to the player
	_window:SetVisible( true );

	// Unlock the mouse for the user ( can be done other ways; also, when using MakePopup, ensure this is the PARENT window! )
	_window:MakePopup( );
end );

//
// Add a bunch of console commands to reference the same thing......
//
concommand.Add( "motd", function( ) hook.Call( "MOTD", GAMEMODE ); end );
concommand.Add( "rules", function( ) hook.Call( "MOTD", GAMEMODE ); end );
concommand.Add( "tos", function( ) hook.Call( "MOTD", GAMEMODE ); end );
concommand.Add( "eula", function( ) hook.Call( "MOTD", GAMEMODE ); end );
concommand.Add( "terms", function( ) hook.Call( "MOTD", GAMEMODE ); end );