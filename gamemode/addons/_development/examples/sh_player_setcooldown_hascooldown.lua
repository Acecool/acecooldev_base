//
// Tests the cooldown system. - Josh 'Acecool' Moser
//
concommand.Add( "dev_cooldown", function( _p )
	print( "START" );
	print( "Has Cooldown?\t\t", Entity( 1 ):HasCooldown( "testing" ) );
	print( "Set Cooldown 3 seconds: ", Entity( 1 ):SetCooldown( "testing", 3 ) );
	print( "Do we have Cooldown now?", Entity( 1 ):HasCooldown( "testing" ) );
	timer.Create( "testing", 1, 3, function( )
		print( "Timer Output Has Cooldown?", Entity( 1 ):HasCooldown( "testing" ) );
	end );
end );


--
-- EXPECTED OUTPUT:
--
--[[
	] dev_cooldown
		START
		Has Cooldown?				false	0
		Set Cooldown 3 seconds: 	true
		Do we have Cooldown now?	true	3
		Timer Output Has Cooldown?	true	2
		Timer Output Has Cooldown?	true	1
		Timer Output Has Cooldown?	false	0
]]