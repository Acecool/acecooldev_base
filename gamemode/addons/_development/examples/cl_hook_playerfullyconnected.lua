//
// PlayerFullyConnected Example Usage - Josh 'Acecool' Moser
//
hook.Add( "PlayerFullyConnected", "PlayerFullyConnectedExample", function( _p )
	// Instead of calling it in the main logic for people that don't want it...
	// We can hook into PlayerFullyConnected and add features / behaviors, without editing the core
	// files, such as chat output, or a hook.Call to open an MOTD panel!

	//
	// Add some basic chat
	//
	chat.AddText(
		COLOR_RED, "[" .. GAMEMODE.Name .. "] Please read and abide by the rules during your time on our server. We are open-minded and easy-going, work with us and we'll work with you.\n",
		COLOR_ORANGE, "1) Try to find bugs\n2) Don't abuse other players\n3) Have fun!\n",
		COLOR_AMBER, "Press F1 for help with some commands, F2 to view key configuration, Press F3 to toggle third/first person view modes, Press F4 for admin mode.\n"
	);
	chat.PlaySound( );

	//
	// Call a MOTD hook
	hook.Call( "MOTD", GAMEMODE, _p );
end );