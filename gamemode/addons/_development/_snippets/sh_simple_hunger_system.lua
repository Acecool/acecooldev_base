//
// Basic Hunger System based on the new data Build Accessors function - Josh 'Acecool' Moser
//


//
// Hunger Meta-Functions - Josh 'Acecool' Moser
//
//	_p:Hunger( );		_p:SetHunger( x );		_p:AddHunger( x );	_p:HasHunger( [ x ] );	_p:CanSetHunger( x );
//	_p:MinHunger( );	_p:SetMinHunger( x );	_p:MaxHunger( );	_p:SetMaxHunger( x );
//
local _tab = data:BuildAccessors( "Hunger", 0, true, FLAG_CHARACTER, META_PLAYER, TYPE_NUMBER, nil, {
	adder		= true;	-- Adds Add, and allows other functions to be added
	get_prefix	= "";	-- MaxHunger instead of GetMax, etc...
	adder_clamp	= true;	-- Clamps values between the Min / Max functions
	min			= 0;	-- Default Minimum, Adds SetMin / Min functions
	max			= 100;	-- Default Maximum, Adds SetMax, Max functions
} );

// Aliases...
META_PLAYER.GetHunger		= _tab.Get;
META_PLAYER.GetMinHunger	= _tab.GetMin;
META_PLAYER.GetMaxHunger	= _tab.GetMax;


if ( SERVER ) then
	timer.Create( "HungerSystem", 1, 0, function( )
		for k, _p in pairs( player.GetAll( ) ) do
			// If the player isn't valid ( ie not fully connected, etc ) defined, skip to next player
			if ( !IsValid( _p ) ) then continue; end

			-- // If the player is >= 100% hungry, start damaging the player with hunger pains...
			if ( _p:HasHunger( 100 ) ) then
				local _pains = DamageInfo( );
				_pains:SetInflictor( _p );
				_pains:SetAttacker( _p );
				_pains:SetDamageType( DMG_GENERIC );
				_pains:SetDamage( 1 );
				_pains:SetMaxDamage( 1 );
				_p:TakeDamageInfo( _pains );
			else
				-- // If the player has not reached 100% hunger, keep adding 0.1 hunger every second
				_p:AddHunger( 0.1 );
			end
		end
	end );
	timer.Remove( "HungerSystem" );
else
	// CLIENT side code; You'll need to network hunger so the client will see it.
	hook.Add( "HUDPaint", "HungerOutput", function( )
		local _p = LocalPlayer( );
		if ( !IsValid( _p ) ) then return; end

		surface.SetFont( "Default" );
		surface.SetTextColor( 255, 255, 255, 255 );
		surface.SetTextPos( 50, 650 );
		surface.DrawText( "Hunger: " .. _p:Hunger( ) );
	end );
end