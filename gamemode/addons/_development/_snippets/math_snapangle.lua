//
// Snaps Angle to nearest... Josh 'Acecool' Moser
//
function math.SnapAngle( _angle, _snap_degrees )
	if ( _snap_degrees == 0 ) then return _angle; end

	_angle = math.Round( _angle / _snap_degrees ) * _snap_degrees;
	_angle = math.NormalizeAngle( _angle ); // Normalizes angle, so it takes value between -180 and 180. 

	return _angle;
end

// _ang.y = math.NormalizeAngle( math.SnapAngle( _ang.y, 90 ) );