This snippets folder contains code that I wrote which may or may not be useful for learning purposes.

Some of the code such as MsgC, META_ANGLE:SnapTo, META_VECTOR:ToColor and others are now
in Garry's Mod code; they're no longer needed to be added / included to work but I included them
in this folder to serve as an example of how to create certain functionality.