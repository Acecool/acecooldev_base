//
// Converts Vector To Color - Josh 'Acecool' Moser
//
function META_VECTOR:ToColor( )
	return Color( math.Round( self.x * 255, 0 ), math.Round( self.y * 255, 0 ), math.Round( self.z * 255, 0 ), 255 );
end