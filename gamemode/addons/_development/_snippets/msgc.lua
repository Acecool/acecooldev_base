//
// MsgC Helper Function to act more like print / chat.AddText - Josh 'Acecool' Moser
// -- Do as you want with this code -- 
//
if ( !__MSGC ) then __MSGC = MsgC; end
function MsgC( _color, _text, ... )
	__MSGC( _color, _text );

	local _tab = { ... };
	if ( #_tab >= 2 ) then
		return MsgC( ... );
	end

	return true;
end