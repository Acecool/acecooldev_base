//
// Resources lua - Josh 'Acecool Moser'
//
// Install Directory ( Server Garry's Mod Folder ): garrysmod/addons/acecool/lua/autorun/server/sv_add_gamemode_resources.lua
//
// Purpose: resource.AddSingleFile for all custom content on server inside the
// gamemodes/<gamemode_name>/content/ directory which tells clients to download them.
//
// Also set up to add all .gma files in garrysmod/addons/ -- If you edit line 119 and set the
// 4th argument to false, it'll recurse throughout all files/folders in garrysmod/addons/ instead of
// just the base-folder.
//

// For those of you not using AcecoolDev_Base
local COLOR_GREEN	= Color( 0, 255, 0, 255 );
local COLOR_RED		= Color( 255, 0, 0, 255 );

// These extensions are ignored by the standard system
local IgnoreExtensions = {
	txt = true;
	psd = true;
	tga = true;
	zip = true;
	rar = true;
	db 	= true;
	exe = true;

	// workshop/ folder if you want .gma files to be included
	gma = true;
};


//
// The old / outdated recursive resource loader...
//
hook.Add( "Initialize", "AcecoolDev:resource.AddSingleFile", function( )
	// This is for Workshop Addons that are included; The list can be sent to users and compared to their
	// engine.GetAddons( ) to verify they downloaded all server content.
	if ( !COMPARE_ADDONS_LIST ) then COMPARE_ADDONS_LIST = { }; end

	// Initialize a few counters
	local _fileCount 		= 0;
	local _workshopFileCount = 0;

	// Our base directory... This recursively moves through the gamemode content/ folder.
	local base_dir 			= "gamemodes/" .. GAMEMODE.FolderName .. "/content/";


	//
	// Recursive portion of function...
	//
	local function includeAllInDirectory( _base, _addToBase, _addonsfolder, _addonsfolderonly )
		// Don't process the scripts directory
		if ( _addToBase == "scripts/" ) then return; end

		// Grab files and folders in current folder
		local _files, _folders = file.Find( _base .. "*" , "MOD" );

		// For each folder inside the current folder, we open and do the same thing...
		if ( !_addonsfolderonly ) then
			for _, f in pairs( _folders ) do
				// This used to be necessary as files without extension could be considered a directory. Shouldn't be needed any longer...
				if ( file.IsDir( _base .. f, "MOD" ) ) then
					includeAllInDirectory( _base .. f .. "/", tostring( _addToBase .. f .. "/" ), false );
				end
			end
		end

		// This lets you set up a _protected_content.txt file inside of a directory where you don't want files downloaded ( ie HL2 content, or other protected content )
		if ( file.Exists( _base .. "_protected_content.txt", "MOD" ) ) then
			// Server console output acknowledging the data is skipped; sub-folders will still be included...
			MsgC( COLOR_RED, "Protected Content not offered for download: " .. _addToBase .. "\n" );
		else
			// For each file inside of the current folder
			for _, f in pairs( _files ) do
				// Grab extension; can also use string.GetExtensionFromFilename
				local _ext = string.Right( f, 3 );

				// If the current folder is workshop and the extension of the current file is a .gma ( can be empty gma )
				if ( ( _addonsfolder || _addToBase == "workshop/" ) && _ext == "gma" ) then
					// Strip all non-digits from the file, we only need workshop-id
					local _file = string.gsub( f, "%D", "" );

					// Tell the client to download the file, and add it to our table which we will send to clients on connect to compare addons
					resource.AddWorkshop( _file );
					table.insert( COMPARE_ADDONS_LIST, _file );

					// Update the counter...
					_workshopFileCount = _workshopFileCount + 1;
				else
					// If we aren't skipping this extension
					if ( !IgnoreExtensions[ _ext ] ) then
						// Add it to the list of files to be downloaded ( I've found resource.AddSingleFile to be much more reliable than resource.AddFile [ which is supposed to add like files ] )
						resource.AddSingleFile( _addToBase .. f );

						// Update the counter...
						_fileCount = _fileCount + 1;
					else
						// Notify the server / console that a file was skipped...
						MsgC( COLOR_RED, "Skipped Content ( erroneous extension \"" .. _ext .. "\" ):" .. _addToBase .. f .. "\n" );
					end
				end
			end
		end
	end

	// Start the recursive inclusion.. Wrap it with green text...
	MsgC( COLOR_GREEN, "************************************************\n" );
	MsgC( COLOR_GREEN, "**Processing GM custom content directories and resources...\n" );

	includeAllInDirectory( base_dir, "" );

	MsgC( COLOR_GREEN, "**Processing completed - " .. _fileCount .. " items & " .. _workshopFileCount .. " workshop items made available to download.\n" );
	MsgC( COLOR_GREEN, "************************************************\n" );
	MsgC( COLOR_GREEN, "**Processing GarrysMod/addons/ folder...\n" );

	_fileCount 		= 0;
	_workshopFileCount = 0;

	// 3rd arg is true for first iteration, addons folder which loads .gma files
	// 4th argument is to determine if we only look at the BASE addons folder ( for .gma files )
	includeAllInDirectory( "addons/", "", true, true );

	MsgC( COLOR_GREEN, "**Processing completed - " .. _fileCount .. " items & " .. _workshopFileCount .. " workshop items made available to download.\n" );
	MsgC( COLOR_GREEN, "************************************************\n" );
end );