//
// Incorporates Physgun Freezing / UnFreezing using the Physgun - Josh 'Acecool' Moser
//


//
// HOOKS
//


//
// When a Player is Picked up by an Admin using the Physgun, UnLock the Player.
//
hook.Add( "PhysgunPickup", "AcecoolAdmin:FreezePlayer:PhysgunPickup", function( _p, _t )
	if ( IsValid( _t ) && _t:IsPlayer( ) && _p:IsAdmin( ) && _p:IsHigherLevel( _t ) ) then
		_t:Notify( "#physgun_pickup_by", _p:Nick( ), _p:SteamID( ) );
		if ( _t:IsFrozen( ) ) then
			_p:LockMovement( _t, false );
		end

		// Done to prevent glitch-dropping which injures...
		_t:Freeze( true );
		_t:SetMoveType( MOVETYPE_NOCLIP );
		return true;
	end
end );


//
// When a Player is Dropped by an Admin using the Physgun, notify them.
//
hook.Add( "PhysgunDrop", "AcecoolAdmin:FreezePlayer:PhysgunDrop", function( _p, _t )
	// Hack because OnPhysgunFreeze isn't called on players; Right Clicking drops the player anyway so this works well.
	if ( IsValid( _t ) && _t:IsPlayer( ) && IsValid( _p ) ) then
		if ( _p:KeyDown( IN_ATTACK2 ) ) then
			hook.Call( "OnPhysgunFreeze", GAMEMODE, _p:GetWeapon( "weapon_physgun" ), _t:GetPhysicsObject( ), _t, _p );
		else
			_t:SetMoveType( MOVETYPE_WALK );
		end
	end

	if ( IsValid( _t ) && _t:IsPlayer( ) ) then
		_t:Notify( "#physgun_dropped_by", _p:Nick( ), _p:SteamID( ) );
	end
end );


//
// When the Reload Button is pressed with the PhysGun ( UnFreezes only those that were frozen by admin )
//
hook.Add( "OnPhysgunReload", "AcecoolAdmin:FreezePlayer:OnPhysgunReload", function( _w, _p )
	for k, v in pairs( player.GetAll( ) ) do
		if ( !IsValid( v ) ) then continue; end

		local _frozen, _frozen_by, _frozen_pos, _frozen_ang = v:IsMovementLocked( );
		if ( _frozen ) then
			if ( _frozen_by != "" && _p:EntIndex( ) != _frozen_by ) then continue; end
			_p:LockMovement( v, false );
		end
	end
end );


//
// When Secondary Attack is used with the PhysGun
//
hook.Add( "OnPhysgunFreeze", "AcecoolAdmin:FreezePlayer:OnPhysgunFreeze", function( _w, _phys, _t, _p )
	if ( IsValid( _t ) && _t:IsPlayer( ) && !_t:IsFrozen( ) ) then
		_p:LockMovement( _t, true );
	end
end );


//
// Ensures PlayerDeathThink is called and Forces Respawn on Death
//
hook.Add( "PlayerDeath", "AcecoolAdmin:FreezePlayer:PlayerDeath", function( _victim, _inflictor, _attacker )
	local _frozen, _frozen_by, _frozen_pos, _frozen_ang = _victim:IsMovementLocked( );
	if ( _frozen ) then
		if ( IsValid( _attacker ) && _attacker:IsPlayer( ) && !_attacker:HasCooldown( "attacking_frozen_player_warning" ) ) then
			_attacker:SetCooldown( "attacking_frozen_player_warning", 3 );
			_attacker:Notify( "#attacking_frozen_player_warning" );
		end

		// Hack to allow player to respawn / PlayerDeathThink hook to be called...
		Entity( _frozen_by ):LockMovement( _victim, false, true );
		_victim:Spawn( );
	end
end );


//
// Prevents Suicide if Frozen
//
hook.Add( "CanPlayerSuicide", "AcecoolAdmin:FreezePlayer:CanPlayerSuicide", function( _p )
	if ( _p:IsFrozen( ) ) then return false; end
end );


//
// Prevents escape via Suicide ( Just in case )
//
hook.Add( "PlayerSpawn", "AcecoolAdmin:FreezePlayer:PlayerSpawn", function( _p )
	// Hack to ensure this runs after player spawn...
	timer.Simple( 0, function( )
		local _frozen, _frozen_by, _frozen_pos, _frozen_ang = _p:IsMovementLocked( );
		if ( _frozen ) then
			local _admin = Entity( _frozen_by );
			_p:SetPos( _frozen_pos );
			_p:SetEyeAngles( _frozen_ang );
			_admin:LockMovement( _p, true, true );
		end
	end );
end );


//
// Prevent damage while frozen and warn the attacker that attacking is futile!
//
hook.Add( "PlayerShouldTakeDamage", "AcecoolAdmin:FreezePlayer:PlayerShouldTakeDamage", function( _victim, _attacker )
	if ( IsValid( _victim ) && _victim:IsPlayer( ) && _victim:IsFrozen( ) ) then
		if ( IsValid( _attacker ) && _attacker:IsPlayer( ) && !_attacker:HasCooldown( "attacking_frozen_player_warning" ) ) then
			_attacker:SetCooldown( "attacking_frozen_player_warning", 3 );
			_attacker:Notify( "#attacking_frozen_player_warning" );
		end

		return false;
	end
end );