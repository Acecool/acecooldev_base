//
// Steam API - Josh 'Acecool' Moser
// Read the GenerateExample function to understand how to use these functions...
//


//
// Database / Config
//
steam = { //
	// Domain needed to obtain the key..
	KEY_OUTPUT 		= "http://steamcommunity.com/dev/apikey";

	// Go to the KEY_OUTPUT domain to activate / obtain a STEAM API Key and put it in below
	KEY 			= "XXXXXXXXXXXXXXXXXXXXXX";

	// When you obtain the key, be sure to register it to the proper domain otherwise it won't work.
	KEY_DOMAIN 		= "127.0.0.1";

	// Owner SteamID goes here ( useless at the moment, but maybe it'll do something soon ).
	OWNER_STEAM 	= "STEAM_0:1:4173055";
	OWNER_STEAM64 	= "76561197968611839";
};


//
// Enumerations
//
STEAM_API_KEY 		= 1;
STEAM_API_FORMAT 	= 2;
STEAM_API_STEAMID 	= 3;
STEAM_API_STEAMID64 = 4;
STEAM_API_GAMEID 	= 5;
STEAM_API_RESULTS 	= 6;


//
// Banned Enums
//
STEAM_API_VAC_BANNED = 1;
STEAM_API_ECONOMY_BAN = 2;
STEAM_API_COMMUNITY_BAN = 3;


//
// More Enumerations: https://developer.valvesoftware.com/wiki/Steam_Web_API
//
STEAM_API_PERSONSTATE_OFFLINE = 0
STEAM_API_PERSONSTATE_ONLINE = 1
STEAM_API_PERSONSTATE_BUSY = 2
STEAM_API_PERSONSTATE_AWAY = 3
STEAM_API_PERSONSTATE_SNOOZE = 4
STEAM_API_PERSONSTATE_LOOKTOTRADE = 5
STEAM_API_PERSONSTATE_LOOKTOPLAY = 6

STEAM_API_PROFILE_VISIBILITY_PRIVATE = 1
STEAM_API_PROFILE_VISIBILITY_PUBLIC = 3


//
// The helper-function which handles ALL steam requests using JSON format...
//
function steam:FetchData( _urlstring, _varargs, _callback )
	local _query_url = "http://api.steampowered.com/";
	local _url = string.format( _query_url .. _urlstring, unpack( ( _varargs || { } ) ) ) .. "&format=json";
	http.Fetch( _url, function( _data )
		local _raw = util.JSONToTable( _data );
		if ( isfunction( _callback ) ) then
			_callback( _raw );
		else
			print( _data )
			print( _raw );
		end
	end );
end


//
// Creates an enumeration list. ( Requires trimming.. Replace all "[", "_]", and "]" with "" in that order;
// for it to be Lua ( align if you want ) or download unaligned ( 655KB ):
//	https://dl.dropboxusercontent.com/u/26074909/tutoring/__data/game_enumerations.txt )
// or Aligned ( 1,676KB ):
//	https://dl.dropboxusercontent.com/u/26074909/tutoring/__data/game_enumerations_tabbed.txt
//
function steam:BuildENUMList( )
	steam:FetchData( "ISteamApps/GetAppList/v2?steamapi=Acecool", { }, function( _raw )
		-- local _enums = _G[ "steam" ][ "enums" ] = { };
		-- steam.__enums = { };
		steam.__enum_output = "";
		local _applist = _raw.applist;
		local _apps = _applist.apps;
		for k, v in pairs( _apps ) do
			local _appid = v.appid;

			// A lot of work to prepare the name...
			local _name = string.upper( v.name );
			_name = steam.ENUM_PREFIX .. _name;
			_name = string.gsub( _name, "%W", " " );
			_name = string.gsub( _name, "%s", "_" );
			_name = string.gsub( _name, "_+", "_" );

			// And here it gets set
			-- steam.__enums[ _name ] = _appid;
			steam.__enum_output = steam.__enum_output .. _name .. " = " .. _appid .. ";\n";
		end

		// All of that just so we can prepare it and output it to a file...
		if ( fileio ) then
			-- fileio:Write( "steam_api", "game_enumerations", toprint( steam.__enums ) );
			fileio:Write( "steam_api", "game_enumerations", toprint( steam.__enum_output ) );
		end
	end );
end


//
// simple helper function
//
function steam:BuildVersion( _version )
	return "v000" .. tostring( _version || 1 ) .. "/";
end


//
// Lookup SteamID to see whether user is using FamilySharing / Owns the game or not...
//
function steam:PlayerFamilySharing( _steam, _gameid, _callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64, _gameid };
	local _urlstring = "IPlayerService/IsPlayingSharedGame/v0001/?key=%s&steamid=%s&appid_playing=%s";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _lenderid = _raw.response.lender_steamid;
		local _bSharing = ( _lenderid != "0" );
		_lenderid = ( !_bSharing ) && _steamid || _lenderid;
		_callback( _bSharing, _lenderid );
	end );
end


//
// Returns VAC Ban History
//
function steam:PlayerBanHistory( _steam, _callback, _error_callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64 };
	local _urlstring = "ISteamUser/GetPlayerBans/v0001/?key=%s&steamids=%s&format=json";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.players;
		local _data = _response[ 1 ];

		if ( !_data || table.Count( _data ) == 0 ) then
			local _error = "Unknown Profile";
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( "Error: " .. _error );
			end

			return;
		else
			local _bans = {
				[ STEAM_API_VAC_BANNED ] = _data.VACBanned;
				[ STEAM_API_ECONOMY_BAN ] = _data.EconomyBan;
				[ STEAM_API_COMMUNITY_BAN ] = _data.CommunityBanned;
			};
			local _banned = _data.VACBanned || _data.EconomyBan || _data.CommunityBanned;
			_banned = ( _banned == "none" ) && false || true;

			// Community = ??, vac = server, economy = trading, number of bans, last ban x days ago..
			_callback( _banned, _bans, _data.NumberOfVACBans, _data.DaysSinceLastBan, _raw );
		end
	end );
end


//
// Returns player friends list
//
function steam:PlayerFriendsList( _steam, _callback, _error_callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64 };
	local _urlstring = "ISteamUser/GetFriendList/v0001/?key=%s&steamid=%s&format=json";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.friendslist;

		if ( !_response || table.Count( _response ) == 0 ) then
			local _error = "Private Profile, Unknown Profile or No Friends..";
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( "Error: " .. _error );
			end

			return;
		else
			// Processed friends.. SteamID set as KEY and value as rest of data...
			local _friends = { };
			local _raw_friends = _response.friends;
			for k, v in pairs( _raw_friends ) do
				local _steam, _steam64, _steam3 = util.TranslateSteamID( v.steamid );
				v.steamid = _steam;
				v.steamid64 = _steam64;
				v.steamid3 = _steam3;
				_friends[ _steam ] = v;
			end

			_callback( _friends, _raw );
		end
	end );
end


//
// Returns games owned by the player
//
function steam:PlayerGames( _steam, _callback, _error_callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64, 1, 1 };
	local _urlstring = "IPlayerService/GetOwnedGames/v0001/?key=%s&steamid=%s&include_appinfo=%s&include_played_free_games=%s";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _game_count = _response.game_count;
		local _error = ( !_response || table.Count( _response ) == 0 || !_game_count )

		if ( _error ) then
			_error = "Unknown Profile or no games ever owned..";
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( "Error: " .. _error );
			end

			return;
		else
			// Processed games.. game app ID set as KEY and value as rest of data...
			local _games = { };
			local _raw_games = _response.games;
			for k, v in pairs( _raw_games ) do
				_games[ v.appid ] = v;
			end

			_callback( _game_count, _games, _raw );
		end
	end );
end


//
// Returns list of recently played games by the player
//
function steam:PlayerRecentlyPlayedGames( _steam, _callback, _error_callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64, 10 };
	local _urlstring = "IPlayerService/GetRecentlyPlayedGames/v0001/?key=%s&steamid=%s&count=%s";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _error = _response.total_count == 0;

		if ( _error ) then
			_error = "Unknown Profile or no last-games ever played..";
			if ( isfunction( _error_callback ) ) then
				_error_callback( "" );
			else
				print( "Error: " .. _error );
			end

			return;
		else
			// Processed games.. game app ID set as KEY and value as rest of data...
			local _games = { };
			local _raw_games = _response.games;
			for k, v in pairs( _raw_games ) do
				_games[ v.appid ] = v;
			end

			_callback( _games, _raw );
		end
	end );
end




//
// Returns list of groups player is a part of
//
function steam:PlayerGroups( _steam, _callback, _error_callback )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	if ( _steamid == "BOT" ) then return false; end
	local _varargs = { self.KEY, _steamid64, 10 };
	local _urlstring = "ISteamUser/GetUserGroupList/v0001/?key=%s&steamid=%s";
	self:FetchData( _urlstring, _varargs, function( _raw )
		local _response = _raw.response;
		local _error = _response.error;
		local _success = _response.success;

		if ( !_success && _error ) then
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( "Error: " .. _error );
			end

			return;
		else
			// Processed groups.. Groups ID set as KEY and value as true to make checking if part of group easier...
			// Just if ( _groups[ id ] ) then ... end
			local _groups = { };
			local _raw_groups = _response.groups;
			for k, v in pairs( _raw_groups ) do
				_groups[ v.gid ] = true;
			end

			_callback( _groups, _raw );
		end
	end );
end


//
// Returns players currently playing in game
//
function steam:GamePlayerCount( _game, _callback, _error_callback )
	local _urlstring = "ISteamUserStats/GetNumberOfCurrentPlayers/v0001/?appid=" .. tostring( _game || 4000 );
	self:FetchData( _urlstring, _varargs, function( _raw )
		if ( !_raw || !_raw.response ) then
			error( "Error fetching valid response: " .. toprint( _raw ) );
		end

		local _result = _raw.response.result
		if ( _result == 42 ) then
			if ( isfunction( _error_callback ) ) then
				_error_callback( _result );
			else
				print( "Error code: " .. _result .. ". InValid Game ID!" );
			end
		else
			local _player_count = _raw.response.player_count;
			_callback( _player_count, _result, _raw )
		end
	end );
end


//
// Returns information about a player in this table:
// local _data = _raw.response.players.player[ 1 ];
// commentpermission, profileurl, avatar, personaname, avatarfull, personastate,
// avatarmedium, profilestate, communityvisibilitystate, lastlogoff, steamid64
//
function steam:PlayerSummary( _steam, _callback, _error_callback, _version )
	local _steamid, _steamid64, _steamuid = util.TranslateSteamID( _steam );
	local _urlstring = "ISteamUser/GetPlayerSummaries/" .. self:BuildVersion( _version ) .. "?key=%s&steamids=%s";
	local _varargs = { self.KEY, _steamid64 };
	self:FetchData( _urlstring, _varargs, function( _raw )
		// V1 and 2 are identical in terms of data, but V2 only nests 2 tables instead of 3.
		local _data = nil;
		if ( !_version || _version == 1 ) then
			_data = _raw.response.players.player[ 1 ];
		elseif( _version == 2 ) then
			_data = _raw.response.players[ 1 ];
		end

		// _data.steamid which is Steam64 swapped for Steam32.
		// ( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw )
		if ( _data ) then
			_callback( _steamid, _data.profileurl, _data.personaname, _data.lastlogoff, _data.commentpermission, { avatar = _data.avatar, avatar_med = _data.avatarmedium, avatar_full = _data.avatarfull }, { state_persona = _data.personastate, state_profile = _data.profilestate, state_visibility = _data.communityvisibilitystate }, _raw );
		else
			local _error = "steam:PlayerSummary( " .. ( ( _steam && _steam != "" ) && _steam || "STEAM_BLANK" ) .. ( tostring( _callback ) || "CALLBACK_BLANK" ) .. ( _version || 1 ) .. " ); -> FAILED!";
			if ( isfunction( _error_callback ) ) then
				_error_callback( _error );
			else
				print( _error );
			end
		end
	end );
end


//
// Example of how to use each element in this system
//
function steam:GenerateExample( _errors )
	if ( _errors ) then

		// PlayerFamilySharing; NO ERROR CALLBACK. Returns true if sharing, and lenderid becomes SteamID of lender
		// but if not sharing, _lenderid is steamid of local player
		steam:PlayerFamilySharing( "STEAM_0:1:4173055", 4000, function( _bSharing, _lenderid )
			print( _bSharing, _lenderid );
		end );

		// Showing PlayerBanHistory; ERROR No bans for me
		steam:PlayerBanHistory( "STEAM_0:1:4173055", function( _banned, _bans, _ban_count, _sober_for, _raw )
			local _ban_community	= _bans[ STEAM_API_COMMUNITY_BAN ];
			local _ban_vac			= _bans[ STEAM_API_VAC_BANNED ];
			local _ban_economy		= _bans[ STEAM_API_ECONOMY_BAN ];

			print( "Banned?: ", _banned );
			print( "Times Banned: ", _ban_count );
			print( "Sober For: ", _sober_for, " days!" );
			print( "Community Ban: ", _ban_community );
			print( "VAC Ban: ", _ban_vac );
			print( "Economy Ban: ", _ban_economy );

			-- print( _banned, _bans, _ban_count, _sober_for, _raw );
		end, function( _error )
			print( _error );
		end );

		// Showing Player Friends List; ERROR, PRIVATE PROFILE / UNKNOWN PROFILE / NO FRIENDS
		steam:PlayerFriendsList( "STEAM_0:1:4173055", function( _friends_list, _raw )
			print( _data );
		end, function( _error )
			print( _error );
		end );

		// Showing PlayerGames; ERROR, _game_count is number of games owned, _games is table with AppID as KEY, appid, img_icon_url, img_logo_url, name, playtime_forever as value
		steam:PlayerGames( "STEAM_0:1:4173055111111", function( _game_count, _games, _raw )
			print( _game_count );
		end, function( _error )
			print( _error );
		end );

		// Showing PlayerRecentlyPlayedGames; ERROR.
		steam:PlayerRecentlyPlayedGames( "-1", function( _games, _raw )
			print( _games );
		end, function( _error )
			print( _error );
		end	);

		// Showing PlayerGroups; ERROR, private profile
		steam:PlayerGroups( "76561198043337919", function( _groups )
			print( _groups );
		end, function( _error )
			print( _error );
		end );

			// Showing PlayerGroups; ERROR, Profile doesn't exist
			steam:PlayerGroups( "-1", function( _groups )
				print( _groups );
			end, function( _error )
				print( _error );
			end );

		// Showing GamePlayerCount test; ERROR, using OnError Callback function ( would print otherwise )
		steam:GamePlayerCount( -1, function( _player_count, _result, _raw )
			print( _player_count, _result, _raw );
		end, function( _error )
			print( _error );
		end );

		// Showing PlayerSummary test; ERROR, using OnError Callback function ( would print otherwise )
		steam:PlayerSummary( "STEAM_0:1:4173055-11111", function( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw )
			print( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw );
		end, function( _error )
			print( _error );
		end	);
	else
		//
		// MOTE, all SteamID FIELDS can accept any form of SteamID...
		// SteamID-32 ( STEAM_0:1:4173055 ), SteamID64 ( 76561197968611839 ), SteamID3/U ( [U:1:8346111] )
		// All forms of SteamID are converted using util.TranslateSteamID( x ) where x can be ANY form and
		// all 3 are returned in the above mentioned order. local _steam, _steam64, _steam3 = util.TranslateSteamID( x );
		//

		// PlayerFamilySharing; NO ERROR CALLBACK. Returns true if sharing, and lenderid becomes SteamID of lender
		// but if not sharing, _lenderid is steamid of local player
		steam:PlayerFamilySharing( "STEAM_0:1:4173055", 4000, function( _bSharing, _lenderid )
			print( _bSharing, _lenderid );
		end);

		// Showing PlayerBanHistory
		steam:PlayerBanHistory( "STEAM_0:0:34824839", function( _banned, _bans, _ban_count, _sober_for, _raw )
			local _ban_community	= _bans[ STEAM_API_COMMUNITY_BAN ];
			local _ban_vac			= _bans[ STEAM_API_VAC_BANNED ];
			local _ban_economy		= _bans[ STEAM_API_ECONOMY_BAN ];

			print( "Banned?: ", _banned );
			print( "Times Banned: ", _ban_count );
			print( "Sober For: ", _sober_for, " days!" );
			print( "Community Ban: ", _ban_community );
			print( "VAC Ban: ", _ban_vac );
			print( "Economy Ban: ", _ban_economy );

			-- print( _banned, _bans, _ban_count, _sober_for, _raw );
		end, function( _error )
			print( _error );
		end );

		// Showing PlayerFriendsList; SUCCESS, _friendslist is table of friends, SteamID is KEY, friends_since ( os.time( ) ), relationship, steamid, steamid64, steamid3 as values
		steam:PlayerFriendsList( "STEAM_0:0:98423266", function( _friendslist, _raw )
			print( _data );
		end );

		// Showing PlayerGames; SUCCESS, _game_count is number of games owned, _games is table with AppID as KEY, appid, img_icon_url, img_logo_url, name, playtime_forever as value
		steam:PlayerGames( "STEAM_0:1:4173055", function( _game_count, _games, _raw )
			print( _game_count );
		end );

		// Showing PlayerRecentlyPlayedGames; SUCCESS. AppID as KEY, appid, img_icon_url, img_logo_url, name, playtime_2weeks, playtime_forever as value
		steam:PlayerRecentlyPlayedGames( "STEAM_0:1:4173055", function( _games, _raw )
			print( _games );
		end );

		//
		// Showing PlayerGroups; SUCCESS - Non-Private Profile
		// Format is a table where the KEY is the group, value is true ( makes it easy to compare via direct access )
		//
		steam:PlayerGroups( "STEAM_0:0:98423266", function( _groups )
			print( _groups );
		end );

		// Showing GamePlayerCount; SUCCESS - no OnError function used
		steam:GamePlayerCount( 4000, function( _player_count, _result, _raw )
			print( _player_count, _result, _raw );
		end );


		// Showing Player Summary; SUCCESS
		steam:PlayerSummary( "STEAM_0:1:4173055", function( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw )
			print( _steam, _profileurl, _name, _lastlogoff, _commentpermission, _avatardata, _statedata, _raw );
		end );

	end
end


//
// Uncomment to run; all of them may not run at once... But each one works. Copy the ones with the error
// function for full functionality, although if an error function is omitted, it will simply print the error out..
//
-- steam:GenerateExample( );