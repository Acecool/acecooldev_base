//
//	- Josh 'Acecool' Moser
//


//
// STEAM API Configuration
//
// Steam API is in the modules/ folder, and because of the way the autoloader includes files
// ( deepest-first ), we can set up our config in this file to make it easier to get to.
// Eventually this will be replaced by the new config system which is being written but I still
// have a few design choices to make ( such as where to save the data, txt, sv.db, etc... )
//
// Go to the KEY_OUTPUT domain to activate / obtain a STEAM API Key and put it in below
steam.KEY 			= "XXXXXXXXXXXXXXXXXXXXXX";

// When you obtain the key, be sure to register it to the proper domain otherwise it won't work.
steam.KEY_DOMAIN 		= "127.0.0.1";


//
// Bans Configuration
//
-- bans.FlagVacBannedAccounts = true;
-- bans.