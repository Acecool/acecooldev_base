//
// Syncable Flags Registry...
//
if ( SERVER ) then
	// These are variables the client controls. The ShouldSync should be used to prevent other players from
	// syncing flags to other players, etc...

	// eXAMPLE
	-- networking:RegisterSyncableFlag( "<example>", DATA_TYPE_or_TYPES, { true, false, 1, 2, 3, "blah", "heh", "bleh" }, _private, _category,
		// ShouldSync Function; return false to prevent sync from happening...
		-- function( _p, _ent, _flag, _value, _private, _category )
			
		-- end,

		// OnSuccess Function; To be used if something should happen when successfully synced...
		-- function( _p, _ent, _flag, _value, _private, _category )
			
		-- end
	-- );


	//
	// Helper Function; returns false if the ent isn't the player ( to prevent players from syncing a var to non-self )
	//
	local function SyncableToCurrentPlayerOnly( _p, _ent, _flag, _value, _private, _category )
		// Return false if _p isn't the _ent...
		if ( IsValid( _p ) && IsValid( _ent ) && _p != _ent ) then
			return false;
		end
	end


	//
	// Helpder function to allow certain flags to be synced if the player is driving the vehicle
	//
	local function SyncableToCurrentDriverOnly( _p, _ent, _flag, _value, _private, _category )

	end


	//
	// networking:RegisterSyncableFlag( _flag, _datatype, _values, _private, _category, _shouldsync, _onsuccess );
	//
	networking:RegisterSyncableFlag( "inconsole", TYPE_BOOL, nil, nil, FLAG_PLAYER, SyncableToCurrentPlayerOnly );
	networking:RegisterSyncableFlag( "isspeaking", TYPE_BOOL, nil, nil, FLAG_PLAYER, SyncableToCurrentPlayerOnly );
	networking:RegisterSyncableFlag( "isrecording", TYPE_BOOL, nil, true, FLAG_PLAYER, SyncableToCurrentPlayerOnly );
	networking:RegisterSyncableFlag( "inchat", TYPE_BOOL, nil, nil, FLAG_PLAYER, SyncableToCurrentPlayerOnly );

	networking:RegisterSyncableFlag( "active_tool", TYPE_NUMBER, nil, false, FLAG_ADMIN );
end

// CL Receivers
networking:RegisterReceiver( "PlayerSay:AdminChat", { TYPE_ENTITY, TYPE_STRING, TYPE_STRING } );
-- networking:RegisterReceiver( "TestReceiver_CL", TYPE_TRIPLEDOT );
networking:RegisterReceiver( "RunConsoleCommand", TYPE_STRING );
networking:RegisterReceiver( "PlayAnimation", { TYPE_ENTITY, TYPE_NUMBER } );
-- networking:RegisterReceiver( "RequestedFlag", { TYPE_ENTITY, TYPE_STRING, TYPE_UNKNOWN } );
-- networking:RegisterReceiver( "CallHook", { TYPE_STRING, TYPE_STRING, TYPE_TRIPLEDOT } );
networking:RegisterReceiver( "ShowPlayerSteamProfile", TYPE_ENTITY );
-- networking:RegisterReceiver( "RedownloadLightMaps", TYPE_CHAR );
-- networking:RegisterReceiver( "Notification", { TYPE_STRING, TYPE_STRING, TYPE_TRIPLEDOT } );
networking:RegisterReceiver( "CopyToClipboard", TYPE_STRING );
networking:RegisterReceiver( "ClearDecals" );
networking:RegisterReceiver( "ProcessEffect", { TYPE_VECTOR, TYPE_VECTOR, TYPE_STRING, TYPE_STRING, TYPE_ENTITY } );





// SV Receivers
-- networking:RegisterReceiver( "TestReceiver_SV", TYPE_TRIPLEDOT );
-- networking:RegisterReceiver( "__UNHANDLED_MESSAGE__", TYPE_TRIPLEDOT );
-- networking:RegisterReceiver( "RequestFlag", { TYPE_ENTITY, TYPE_STRING, TYPE_UNKNOWN, TYPE_UNKNOWN } );
-- networking:RegisterReceiver( "SyncFlag", { TYPE_ENTITY, TYPE_STRING, TYPE_UNKNOWN, TYPE_BOOL } );
-- networking:RegisterReceiver( "EntitySetFlag", { TYPE_ENTITY, TYPE_STRING, TYPE_ANYTHING, TYPE_BOOL, TYPE_STRING } );
networking:RegisterReceiver( "EntityGetFlag", { TYPE_ENTITY, TYPE_STRING } );
-- networking:RegisterReceiver( "networking:PassThrough:ClientToClient", { TYPE_PLAYER, TYPE_STRING, TYPE_TRIPLEDOT } );