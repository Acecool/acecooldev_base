//
// Example and Default Networking Receivers - Josh 'Acecool' Moser
//


//
// Syntax for adding a receiver: function( _p = LocalPlayer( ) on CLIENT who receives the message followed by any/all args the function receives.
//
networking:AddReceiver( "TestReceiver_CL", function( _p, ... )
	print( "TestReceiver_CL", _p, ... );
end );


//
// Simple RunString system. In current form can be used for any data. Will increase security in future.
//
networking:AddReceiver( "FileRefreshed", function( _p, _gmfolder, _folder, _file, _realm, _data )
	MsgC( COLOR_GREEN, "File Refreshed: " .. _folder .. _file .. "!\n" );
	-- loader:IncludeFiles( _gmfolder, _folder, _file, _realm );
	-- include( _folder .. _file );
	RunString( _data );
end );


//
// Admin Chat Receiver
//
networking:AddReceiver( "PlayerSay:AdminChat", function( _p, _sender, _text, _time, _admins )
	local _phrase = "admin_chat_from_x";
	if ( !_p:IsAdmin( ) && !_admins ) then
		_phrase = "admin_chat_from_x_error";
	end

	chat.AddText( COLOR_GREEN, language.GetPhrase( _phrase, _sender, _time ), COLOR_WHITE, _text, COLOR_GREEN, "!" );
end );


//
// Notification Receiver - Tied into PLAYER_META:Notify( ) from SERVER>CLIENT
// and the ... allows us to pass in string.format text if a language phrase is used ( start _msg with # )
// and if the language-phrase has replacements..
//
networking:AddReceiver( "Notification", function( _p, _type, _msg, ... )
	if ( !_msg ) then _msg = "ERROR_NO_MSG!"; end
	notify:Add( _msg, _type, 5, true, ... );
end );


//
// So the server can call RunConsoleCommand on the client
//
networking:AddReceiver( "RunConsoleCommand", function( _p, _data )
	RunConsoleCommand( unpack( string.Explode( " ", _data ) ) );
end );


//
// Copies data to the cients clipboard
//
networking:AddReceiver( "CopyToClipboard", function( _p, _data )
	string.CopyToClipboard( _data );
end );


//
// Shows the player steam profile
//
networking:AddReceiver( "ShowPlayerSteamProfile", function( _p, _ent )
	_ent:ShowProfile( );
end );


//
// Syntax for adding a receiver: function( _p = LocalPlayer( ) on CLIENT who receives the message followed by any/all args the function receives.
//
networking:AddReceiver( "RequestedFlag", function( _p, _ent, _flag, _category, _value )
	_ent:SetFlag( _flag, _value, true, _category );
end );


//
// Allow the server to network hooks without adding a specific receiver for each one...
//
networking:AddReceiver( "CallHook", function( _p, _name, _table, ... )
	local _upper = string.upper( _table )
	local _hooks = hook.GetTable( );
	local _bHookExists = _hooks[ _name ];
	local _bGMHookExists = ( ( !_table || _upper == "GM" || _upper == "GAMEMODE" ) && _G[ _upper ][ _name ] );
	local _bCustomHookExists = ( _table && _G[ _table ] && _G[ _table ][ _name ] );

	if ( !_bHookExists && !_bGMHookExists && !_bCustomHookExists ) then
		networking:SendToServer( "Error", "Hook Not Found", _name, _table );
	else
		hook.Call( _name, ( ( _table ) && _G[ _table ] || nil ), unpack( { ... } ) );
	end
end );