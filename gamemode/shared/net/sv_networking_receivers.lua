//
// Example and Default Networking Receivers - Josh 'Acecool' Moser
//


//
//
// Syntax for adding a receiver: function( Player who sent message to SERVER on SERVER followed by any/all args the function receives.
networking:AddReceiver( "TestReceiver_SV", function( _p, ... )
	print( "TestReceiver_SV", _p, ... );
end );


//
// Alows networking of console commands ( so that server console commands can have an autofill on the client )
//
networking:AddReceiver( "concommand.Run", function( _p, _cmd, _args )
	debug.print( "Networking:concommand.Run", _p, _cmd, _args );
	concommand.Run( _p, _cmd, _args, _args );
end );


//
//
//
networking:AddReceiver( "Error", function( _p, ... )
	local _error = "";
	for k, v in pairs( { ... } ) do
		_error = _error .. " " .. v;
	end
	MsgC( COLOR_RED, "Error Received from " .. _p .. ": ", COLOR_WHITE, _error, "\n" );
	-- Log( _p, "Error Message Received: " .. _error );
end );


//
// Must have debug.Add( "Networking:UnhandledMessage" ); called somewhere to view these errors...
//
networking:AddReceiver( "__UNHANDLED_MESSAGE__", function( _p, ... )
	debug.print( "Networking:UnhandledMessage", _p, ... );
end );


//
// Pass-Through Receiver; This allows clients to call messages on clients ( Just as dangerous as SyncFlag; must be monitored! )
//
-- networking:AddReceiver( "networking:PassThrough:ClientToClient", function( _p, _targetClient, _msgName, ... )
	-- networking:SendToClient( _msgName, _targetClient, ... );
-- end );


//
// Allow clients to grab private flags upon request ( so vehicle fuel, etc isn't synced across entire server )
//
networking:AddReceiver( "RequestFlag", function( _p, _ent, _flag, _value, _private, _category, _default )
	local _data = _ent:GetFlag( _flag, _default, _private, _category );
	if ( _data != _value ) then
		networking:SendToClient( "RequestedFlag", _p, _ent, _flag, _category, _data );
	end
end );


//
// The most dangerous receiver ( Allows clients to sync ANY flag which is why you MUST use
// RegisterSyncableFlag in conjunction with this receiver so that only authorized flags may
// be synced such as inconsole, inchat, etc... and NOT money/currency, exp, etc... )
//
networking:AddReceiver( "SyncFlag", function( _p, _ent, _flag, _value, _private, _category )
	// No point on continuing if the entity is invalid...
	if ( !IsValid( _ent ) ) then error( "#sync_flag_invalid_entity" ); return false; end

	// Grab a local reference to our syncable flags
	local _flags = networking[ NET_SHARED ].syncable_flags[ _flag ];
	local _error = "";

	// If we have syncable flags to compare
	if ( _flags ) then
		// Grab a local reference to types and see if the value matches type or TypeID. If none, allow.
		local _types = _flags.types;
		if ( _types && !_types[ type( _value ) ] && !_types[ TypeID( _value ) ] ) then
			_error = _error .. "Type\t";
		end

		// Grab a local reference to values and see if values match allowed values. If none, allow.
		local _values = _flags.values;
		if ( _values && !_values[ _value ] ) then
			_error = _error .. "Value\t";
		end

		// Grab a local reference to callback and if defined as a function, call it. If non-nil returned, deny.
		local _shouldsync = _flags.shouldsync;
		if ( _shouldsync && isfunction( _shouldsync ) ) then
			local _returned = _shouldsync( _p, _ent, _flag, _value, _private, _category );
			if ( _returned || _returned == false ) then
				_error = _error .. "Callback\t";
				-- return false;
			end
		end
	else
		// If we don't, deny allowing the value to be set...
		_error = _error .. "Registry\t";
	end

	if ( _error == "" ) then
		// If everything passed, then allow it to change the variable
		_ent:SetFlag( _flag, _value, _private, _category );

		// On Success Callback
		local _callback = _flags.callback;
		if ( _callback && isfunction( _callback ) ) then
			_callback( _p, _ent, _flag, _value, _private, _category );
		end

		// Just in case we use networking:Call and want a return......
		return true;
	else
		-- Log( NULL, "Error with SyncFlag( " .. toprint( _p ) .. ", " .. toprint( _ent ) .. ", " .. toprint( _flag ) .. ", " .. toprint( _value ) .. ", " .. toprint( _private ) .. " ) because: " .. _error .. " is missing or wrong!" );
		return false;
	end
end );