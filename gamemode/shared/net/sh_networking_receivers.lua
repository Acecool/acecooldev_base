//
// Example and Default Networking Receivers - Josh 'Acecool' Moser
//


//
// Syntax for adding a receiver: function( _lp = LocalPlayer( ) on CLIENT and _p = Player on SERVER followed by any/all args the function receives.
//
networking:AddReceiver( "TestReceiver_SH", function( _p, _arg2 )
	print( "TestReceiver_SH", "Player: ", _p, "arg2: ", _arg2 );
end );


//
// Just something which outputs data to the console
//
networking:AddReceiver( "OutputToConsole", function( _p, ... )
	print( "OutputToConsole:", ... );
end );