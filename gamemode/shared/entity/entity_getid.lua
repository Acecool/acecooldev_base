//
// Returns an ID that will always be the same, regardless of max-players set on server - Josh 'Acecool' Moser
//
// NOTE: If you want to use this, and use it well, read this: https://dl.dropboxusercontent.com/u/26074909/tutoring/entities/entity_getid_mapping_system.lua.html
//
function META_ENTITY:GetID( )
	local _index = ( ( self:EntIndex( ) + 128 ) - game.MaxPlayers( ) );

	// Prevents World Data from being removed by "physics_entity_solver" reporting 0.
	if ( IsValid( self ) && _index == 0 && self != Entity( 0 ) ) then
		_index = nil;
	end

	return _index || -1;
end