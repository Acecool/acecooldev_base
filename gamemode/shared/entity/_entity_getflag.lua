//
// Front-End for Flags - Gets value of a flag - Josh 'Acecool' Moser
//
function META_ENTITY:GetFlag( _flag, _default, _private, _delay )
	if ( !data || !istable( data ) ) then error( "Class->data doesn't exist!" ); end

	return data:GetFlag( self, _flag, _default, _private );
end