//
// Determines whether or not an entity is a door - Josh 'Acecool' Moser
//
function META_ENTITY:IsDoor ( )
	if ( !IsValid( self ) ) then return false; end
	if ( !self:GetClass( ) || !self:GetModel( ) ) then return false; end
	local _bSearchClass = string.find( self:GetClass( ), "door" );
	local _bSearchModel = string.find( self:GetModel( ), "door" );

	return ( _bSearchClass && _bSearchClass > 0 ) || ( _bSearchModel && _bSearchModel > 0 );
end

-- print( self:GetClass( ), ( _bSearchClass && _bSearchClass > 0 ), self:GetModel( ), ( _bSearchModel && _bSearchModel > 0 ) )