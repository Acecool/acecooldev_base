//
// Front-End for Flags - Resets flags / Nullifies flags - Josh 'Acecool' Moser
//
function META_ENTITY:ResetFlags( )
	if ( !data || !istable( data ) ) then error( "Class->data doesn't exist!" ); end

	local _flags = data:ResetFlags( self:GetID( ) );
	-- local _pflags = data:ResetFlags( self:GetID( ), true );

	return _flags, _pflags;
end