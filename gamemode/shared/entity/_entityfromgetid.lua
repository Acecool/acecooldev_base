//
// Maps GetID to Entity - Josh 'Acecool' Moser
//
// 	If you want to get an Entity from a GetID. Identical to: Entity( EntIndexFromGetID( ID ) );
// 	Usage: 	local _id = Entity( 500 ):GetID( ); -- This ID is mapped to an Entity...
// 			local _ent = EntityFromGetID( _id ); -- Returns the Entity...
//
// NOTE: If you want to use this, and use it well, read this: https://dl.dropboxusercontent.com/u/26074909/tutoring/entities/entity_getid_mapping_system.lua.html
//
function EntityFromGetID( _id )
	return Entity( EntIndexFromGetID( _id ) ) || NULL;
end