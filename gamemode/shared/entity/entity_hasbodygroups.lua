//
// Determines whether or not an entity has selectable body-groups - Josh 'Acecool' Moser
//

// Database.. Each model only needs to be checked once instead of each entity as some may spawn/despawn, etc..
if ( !BODY_GROUPS_DB ) then BODY_GROUPS_DB = { has = { }; bodygroups = { }; }; end


function META_ENTITY:HasBodyGroups( )
	// Vars
	local _model = self:GetModel( ) || ".default";

	// If the result isn't cached, find out if there are selections to be made
	if ( BODY_GROUPS_DB.has[ _model ] == nil ) then
		// Otherwise set the default as false
		BODY_GROUPS_DB.has[ _model ] = false;

		// And look for selectable body-groups
		for k, v in pairs( self:GetBodyGroups( ) ) do
			// If there are more than 1 sub-model for this category ( selections available )
			if ( table.Count( v.submodels ) > 1 ) then
				// Update from the default value
				BODY_GROUPS_DB.has[ _model ] = true;

				// Kill the loop / break-out of the loop
				break;
			end
		end
	end

	// Return the result
	return BODY_GROUPS_DB.has[ _model ];
end
