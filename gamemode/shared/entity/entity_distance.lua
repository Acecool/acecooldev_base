//
// Shortcut function - Josh 'Acecool' Moser
// instead of needing to use Entity:GetPos( ):Distance( Entity2:GetPos( ) ); it uses Entity:Distance( Entity2 )
//
function META_ENTITY:Distance( _data )
	if ( !IsValid( self ) ) then return false; end

	if ( _data ) then
		if ( isvector( _data ) ) then
			return math.GetDistance( self:GetPos( ), _data );
		elseif ( IsValid( _data ) ) then
			return math.GetDistance( self:GetPos( ), _data:GetPos( ) );
		end
	end

	ErrorNoHalt( "Error using Distance", type( _data ) );
	return false;
end