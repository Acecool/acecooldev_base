//
// Returns the move direction of an entity based on Velocity... - Josh 'Acecool' Moser
//


//
// Enumerations
//
MOVEDIR_NONE			= 0;
MOVEDIR_FORWARD			= 1;
MOVEDIR_BACK			= -1;
MOVEDIR_RIGHT			= 2;
MOVEDIR_LEFT			= -2;


//
// Vars
//
local UNITS_TO_INCHES	= 4 / 3;				-- 4/3 units per 1 inch,
local UNITS_TO_FEET 	= UNITS_TO_INCHES * 12;	-- 16 units per foot


//
// Tolerance Config...
//
// DOT will appear high in direction.
local TOLERANCE_DOT		= 0.25;

// Speed tolerance as idle vehicle no handbreak can rock 3+ units.
local TOLERANCE_SPEED	= 0;
local TOLERANCE_V_SPEED	= UNITS_TO_FEET / 4;


//
// Returns the movement direction of the entity - Josh 'Acecool' Moser
// Simple returns only the 4 basic directions WASD; doesn't include up/down for any mode.
// Vehicles are rotated, so Left is forward instead of Forward.
//
function META_ENTITY:GetMoveDirection( )
	// Define our default vars
	local _bVehicle 		= self:IsVehicle( );
	local _vel 				= self:GetVelocity( );
	local _speed 			= math.Round( _vel:Length( ) );
	local _vnorm 			= _vel:GetNormal( );

	// If isn't moving... no point in running the calculations...
	local _bIsMoving = ( ( !_bVehicle && _speed > TOLERANCE_SPEED ) || ( _bVehicle && _speed > TOLERANCE_V_SPEED ) );
	if ( !_bIsMoving ) then
		return MOVEDIR_NONE;
	end

	// We are only determining from 4 directions, just need yaw...
	local _ang 				= self:GetAngles( );
	_ang.p 					= 0;
	_ang.r 					= 0;

	// Vehicle rotation because vehicles treat left as forward...
	_ang.y 					= ( _bVehicle ) && _ang.y + 90 || _ang.y;
	_ang.y 					= math.NormalizeAngle( _ang.y );

	// Normalize the Forward and Right Angles
	local _f 				= _ang:Forward( );
	local _r 				= _ang:Right( );

	// Logic: Thank you Dot Product... Compare based on a tolerance...
	local _bIsMovingForward = _vnorm:Dot( _f ) > TOLERANCE_DOT;
	if ( _bIsMovingForward ) then
		return MOVEDIR_FORWARD;
	end

	local _bIsMovingBack 	= _vnorm:Dot( _f ) < -TOLERANCE_DOT;
	if ( _bIsMovingBack ) then
		return MOVEDIR_BACK;
	end

	local _bIsMovingLeft 	= _vnorm:Dot( _r ) < -TOLERANCE_DOT;
	if ( _bIsMovingLeft ) then
		return MOVEDIR_LEFT;
	end

	local _bIsMovingRight 	= _vnorm:Dot( _r ) > TOLERANCE_DOT;
	if ( _bIsMovingRight ) then
		return MOVEDIR_RIGHT;
	end
end