//
// Builds a table of selectable bodygroups - Josh 'Acecool' Moser
//

// Database.. Each model only needs to be checked once instead of each entity as some may spawn/despawn, etc..
if ( !BODY_GROUPS_DB ) then BODY_GROUPS_DB = { has = { }; bodygroups = { }; }; end


function META_ENTITY:GetBodyGroupsTable( )
	// Vars
	local _model = self:GetModel( ) || ".default";

	// If there are bodygroups for this model but they haven't been setup yet.. process..
	if ( self:HasBodyGroups( ) && BODY_GROUPS_DB.bodygroups[ _model ] == nil ) then
		local _kits = { };

		for k, v in pairs( self:GetBodyGroups( ) ) do
			if ( table.Count( v.submodels ) > 1 ) then
				table.insert( _kits, { id = v.id; name = v.name; count = v.num; num = v.num; } )
			end
		end

		// Setup the db table...
		BODY_GROUPS_DB.bodygroups[ _model ] = _kits;
	end

	// Return kits, or empty table to handle errors
	return BODY_GROUPS_DB.bodygroups[ _model ] || { };
end