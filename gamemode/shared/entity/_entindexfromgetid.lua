//
// Maps GetID to EntIndex - Josh 'Acecool' Moser
//
// 	If you need to get the EntIndex from a GetID.
// 	Usage: local _ent = Entity( EntIndexFromGetID( ID_TO_MAP ) );
//
// NOTE: If you want to use this, and use it well, read this: https://dl.dropboxusercontent.com/u/26074909/tutoring/entities/entity_getid_mapping_system.lua.html
//
function EntIndexFromGetID( _id )
	local _index = _id - ( 128 - game.MaxPlayers( ) );

	// If accidentally used on player, make sure the right index is returned..
	if ( game.MaxPlayers( ) - _id > 0 ) then
		_index = _id;
	end

	return _index;
end