//
// Front-End for Flags - Sets flag on this entity - Josh 'Acecool' Moser
//
function META_ENTITY:SetFlag( _flag, _value, _private )
	if ( !data || !istable( data ) ) then error( "Class->data doesn't exist!" ); end

	return data:SetFlag( self, _flag, _value, _private );
end