//
// FileIO Class - Josh 'Acecool' Moser
// Also planned to redirect "io" to fileio for those used to using io in Lua.
//

--[[
	NOTICE: This object is meant to aide in reading and writing from and to files. By default
		this object is locked into the data/<game-mode_folder-name>/ folder unless an external
		file is required ( such as ReadFunction or ReadExternal[ implementing to simplify args
		of fileio:Read( )... ]. Because it is locked in that base folder it'll help prevent
		other game-modes files' from being overwritten. However, it is possible for the same
		game-mode folder to be used on many servers where owners may have additional data, so
		to also aide in that a new config option will be added so that one additional step-folder
		will be used...

		config( "base_data_folder" ) / config.get( "base_data_folder" )
		config.set( "base_data_folder", "server_1" ) / config.set( "base_data_folder", "server_1/blah" )

		A trailing slash isn't required but may be added. The config rule set for that config option
		will force a leading or trailing slash to be removed. If you want a folder pre/post-fix to
		be added to any files there are config options for those as well...

		config( "file_prefix" ) / config.get( "file_prefix" )
		config( "file_postfix" ) / config.get( "file_postfix" )
		config.set( "file_prefix", "server_1" ) / config.set( "file_prefix", "server_1/blah" )
		config.set( "file_postfix", "server_1" ) / config.set( "file_postfix", "server_1/blah" )

		The rule-set for these config options will use string.safe which converts non-alphanumeric
		characters into underscores to prevent adding a folder which won't be created.

		fileio is used with the config system, binds-manager and many other default systems and
		is therefore required by many files. A new feature will be coming to AcecoolDev_Base to
		ensure files can be placed where-ever and be loaded using the default load order, or
		if require'd they'll be loaded then. They will NOT be re-loaded unless the file has been
		altered within the short time it takes for all files to be loaded. This new feature has
		been on the drawing board for some-time and I was thinking of different ways of how I could
		accomplish it successfully without causing files to be re-loaded, etc... With the advances
		made towards the smart auto-refresh system I feel it'll be a straight-forward addition
		at this time.

		It was either require a file now and prevent from loading later, or generate a list of
		files to load with some logic determining load-order and then processing the list or
		simply processing once... Processing once is much better especially as the autoloader
		is called during auto-refresh.

		Expect it in the near future...
--]]


//
// Definitions
//
FILEIO_FILETYPES = {
	archive = {
		rar = true;
		zip = true;
		gma = true;

	};
	audio = {
		wav = true;
		mp3 = true;

	};
	image = {
		jpg = true;
		jpeg = true;
		png = true;
		gif = true;

	};
	lua = {
		lua = true;

	};
	text = {
		txt = true;

	};
	texture = {
		vtf = true;

	};
	texture_misc = {
		vmt = true;

	};
	map = {
		bsp = true;

	};
	model = {
		mdl = true;

	};
	model_misc = {
		vtx = true;
		phy = true;
		vvd = true;

	};
	restricted_files = {
		["thumbs.db"] = true;

	};
	restricted = {
		db = true;

	};
};


//
//
//
fileio = fileio || {
	__filecache = { };
	__filetimecache = { };
};
fileio.__index = fileio;


//
// Creates a new instance of the fileio class. Can be run direct, or a new copy can be created!
//
function fileio:New( )
	local __class = { };
	setmetatable( __class, self );
	self.__index = self;

	return __class;
end


//
// Determines whether or not a file is restricted; separate functions have separate uses for outcome of this
//
function fileio:IsRestrictedFile( _file )
	local _ext = string.GetExtensionFromFilename( string.lower( _file ) );

	if ( FILEIO_FILETYPES.restricted_files[ string.lower( _file ) ] || FILEIO_FILETYPES.restricted[ _ext ] ) then
		return true;
	end

	return false;
end


//
// Reads a file into memory based on the folder location, file you want to read, and if it's internal ( DATA Folder ) or external ( Garry's Mod Folder )
//
function fileio:Read( _folder, _file, _external, _external_path )
	-- Being rewritten to avoid lengthy args.. fileio:Read( _file ) -- default path "DATA"
	-- Instead of requiring folder to be split from file, it'll be allowed together...
end


//
// Reads a file into memory based on the folder location, file you want to read, and if it's internal ( DATA Folder ) or external ( Garry's Mod Folder )
//
function fileio:ReadExternal( _folder, _file, _external, _external_path )
	-- Being written to avoid lengthy args.. fileio:ReadExternal( _file ) -- default path "GAME"
	-- Instead of requiring folder to be split from file, it'll be allowed together...
end


//
// Reads a function into data ( can't read client files as the paths won't exist )
//
function fileio:ReadFunction( _func )
	if( isfunction( _func ) ) then
		local _data = debug.getinfo( _func );
		local _file = _data.short_src;

		// Make sure we can read the function
		local _bLua = ( _data.what == "Lua" );
		local _bC = ( _data.what == "C" );
		if ( !_bLua ) then return false; end

		// Does the function contain ...?
		local _tripledot = _data.isvararg;

		// How many arguments does the function have?
		local _args = _data.nparams;

		// ??
		local _namewhat = _data.namewhat;

		// ??
		local _wtf = _data.nups;

		// currentline gives the current line where a given function is executing; if the function was pre-compiled with debug info 4.8 http://www.lua.org/manual/2.4/node12.html#pragma
		local _current_line = _data.currentline;

		// Line where the function starts if Lua
		local _start_line = _data.linedefined || 0;

		// Line where the function ends if Lua
		local _last_line = _data.lastlinedefined || 0;

		// Total lines if we can read them
		local _lines = _last_line - _start_line;

		// Helper var; holds the current cache if any...
		local _cache = self.__filecache[ _file ];

		local _filetime = file.Time( _file, "GAME" );
		if ( !self.__filecache[ _file ] || !self.__filetimecache[ _file ] || _filetime > self.__filetimecache[ _file ] ) then
			// Read file
			self.__filecache[ _file ] = file.Read( _file, "GAME" );

			// Update filetime
			self.__filetimecache[ _file ] = _filetime;

			// Strip Windows new-lines and replace with standard \n
			self.__filecache[ _file ] = string.gsub( self.__filecache[ _file ] || "", "\r\n", "\n" );

			// Explode the file so it can be read line by line...
			self.__filecache[ _file ] = string.Explode( "\n", self.__filecache[ _file ] );

			// Update our "cache" helper-var
			_cache = self.__filecache[ _file ];
		end

		// Read the file if we can
		local _output = "";
		if ( _last_line - _start_line > 0 ) then
			for i = _start_line, _last_line do
				_output = _output .. ( _cache[ i ] || "" ) .. "\n";
			end
		end

		// Read any comments if we can
		local _comments = "";
		if ( _start_line - 1 > 1 ) then
			local i = _start_line - 1;
			while true do
				// If we reach the beginning of the document, quit...
				if ( i < 1 ) then break; end

				local _line = ( _cache[ i ] ) && _cache[ i ] .. "\n" || "";
				local _clean_line = string.Trim( _line );

				// If we reach the "beginning" of another function...
				if ( string.StartWith( _clean_line, "end" ) ) then break; end

				// We're only looking for comments, so if the line isn't a comment then break out..
				if ( !string.StartWith( _clean_line, "--" ) && !string.StartWith( _clean_line, "//" ) ) then break; end

				// Otherwise we have a comment...
				_comments = _line .. _comments;

				// Decrement the line...
				i = i -1;
			end
		end

		// Typically we want the data, not the comments so they're reversed here.
		return _output, _comments;
	end
end


//
// Reads a large list of files into memory.
//
function fileio:ReadFiles( _files, _overwrite_folder, _prep )
	-- Being rewritten
end


//
// Generates a list of files in an addon... ( MAY BE REWRITTEN BECAUSE engine.GetAddons( ) IS BROKEN ON SERVER )
// local _list = fileio:GenerateAddonFileList( v ); -- v from k, v in pairs( engine.GetAddons( ) );
//
function fileio:GenerateAddonFileList( _addon, _directory, _files )
	if ( !_addon || !_addon.downloaded || !_addon.mounted ) then return; end
	if ( !_directory ) then _directory = ""; end
	if ( !_files ) then _files = { }; end

	local _title =	_addon.title;

	local files, folders = file.Find( _directory .. "*", _title );
	for k, v in pairs( folders or { } ) do
		self:GenerateAddonFileList( _addon, _directory .. v .. "/", _files );
	end

	for k, v in pairs( files or { } ) do
		if ( !_files[ _directory ] ) then _files[ _directory ] = { }; end

		table.insert( _files[ _directory ], v );
	end

	return _files;
end


//
// Categorizes files from an addon file list. ( MAY BE REWRITTEN BECAUSE engine.GetAddons( ) IS BROKEN ON SERVER )
// This lets helps in determining whether or not the addon should be loaded... example, if the addon contains a map, is it this map... etc...
//
// May be rewritten for optimizations if possible...
function fileio:GenerateAddonInfoList( _folders )
	if ( !_folders || !istable( _folders ) ) then return _folders; end

	local _total_count, _total_count_all, _maps, _models, _model_related, _images, _textures, _texture_related, _lua, _info, _archives, _sounds, _other = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
	local _map_names = { };

	for __, folder in pairs( _folders ) do
		_total_count_all = _total_count_all + table.Count( folder );
		for k, v in pairs( folder ) do
			local _counted = false;
			local _ext = string.GetExtensionFromFilename( v ); // string.Right( v, 3 );

			if ( FILEIO_FILETYPES.map[ _ext ] ) then _maps = _maps + 1; _counted = true; table.insert( _map_names, v ); end
			if ( FILEIO_FILETYPES.model[ _ext ] ) then _models = _models + 1; _counted = true; end
			if ( FILEIO_FILETYPES.lua[ _ext ] ) then _lua = _lua + 1; _counted = true; end
			if ( FILEIO_FILETYPES.text[ _ext ] ) then _info = _info + 1; _counted = true; end
			if ( FILEIO_FILETYPES.texture[ _ext ] ) then _textures = _textures + 1; _counted = true; end
			if ( FILEIO_FILETYPES.image[ _ext ] ) then _images = _images + 1; _counted = true; end
			if ( FILEIO_FILETYPES.audio[ _ext ] ) then _sounds = _sounds + 1; _counted = true; end
			if ( FILEIO_FILETYPES.archive[ _ext ] ) then _archives = _archives + 1; _counted = true; end
			if ( FILEIO_FILETYPES.model_misc[ _ext ] ) then _model_related = _model_related + 1; _counted = true; end
			if ( FILEIO_FILETYPES.texture_misc[ _ext ] ) then _texture_related = _texture_related + 1; _counted = true; end

			if ( _counted ) then
				_total_count = _total_count + 1;
			else
				_other = _other + 1;
			end
		end
	end

	return {
		maps 					= _maps;
		map_names 				= _map_names;
		models 					= _models;
		model_related 			= _model_related;
		images 					= _images;
		sounds 					= _sounds;
		textures 				= _textures;
		texture_related 		= _texture_related;
		lua 					= _lua;
		info 					= _info;
		other 					= _other;
		archives 				= _archives;
		TOTAL_COUNT 			= _total_count;
		VERIFIED_TOTAL_COUNT 	= _total_count_all;
	};
end


//
// Generates an array where the key is the directory, and values are files within the key directory.
// 		This type of file-list is what the ReadFiles ( into memory ) fileio function requires.
//
function fileio:GenerateFileList( _folder, _filter, _maxdepth, _files, _depth )
	-- Being rewritten to use the cache system so files can be loaded into memory / purged, etc...
end


//
// Writes to an internal file.
//
function fileio:Write( _folder, _file, _data )
	-- Rewriting to shorten args to 2
end


//
// Converts data to JSON if table, otherwise writes as though data is already converted...
//
function fileio:WriteJSON( _folder, _file, _data )
	-- Rewriting to shorten args to 2
end


//
// Incrementally sequence files without overwriting issues ondelete
//
function fileio:WriteIncrementally( _filename, _data )
	-- Being rewritten to create a whole incremental saving / loading system with
	-- ReadLatestIncremental( _file ), OverwriteLatestIncremental( _file, _data ), etc...
end


//
// Removes an internal file.
//
function fileio:Remove( _folder, _file, _external, _external_path )
	-- Rewriting to simplify because it IS LOCKED INTO "DATA"
end


//
// Checks if an internal or external file exists.
//
function fileio:Exists( _folder, _file, _external, _external_path )
	-- Rewriting to simplify
end


//
// Appends data to an internal file.
//
function fileio:Append( _folder, _file, _data )
	-- Rewriting to simplify
end


//
// Prepends data to an internal file.
//
function fileio:Prepend( _folder, _file, _data )
	-- Rewriting to simplify
end


//
// Creates a directory.
//
function fileio:CreateDir( _folder, _external, _external_path )
		-- Rewriting to simplify because it is locked in "DATA"
end


//
// Builds a semi-safe destination folder name.
//
function fileio:GetFolder( _folder )
	-- Rewriting to work with new fileio
end


//
// Returns the file extension
//
function fileio:GetExtension( )
	// Because we're locked to writing in TXT, this'll play a part with writing to files
	return ".txt";
end


//
// Returns a safe gamemode name
//
function fileio:GetBaseFolder( )
	return string.safe( string.lower( ( GM or GAMEMODE ).Name ) );
end


//
// Returns a safe gamemode folder name
//
function fileio:GetGMFolder( )
	return string.safe( string.lower( ( GM or GAMEMODE ).FolderName ) );
end


//
// Returns a safe file-name
//
function fileio:GetFile( _file )
	return string.safe( string.lower( _file ) );
end