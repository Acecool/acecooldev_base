//
// Simple time system for easy calls to time - Josh 'Acecool' Moser
//
time = { };
time.__index = time;
time.__type = "Time";

// DD.MM.YYYY HH:MM:SS
time.__DateOutput = "%d.%b.%Y %X";
-- os.date( "%d.%b.%Y %X", os.time( ) );


//
// Create a new instance of this class ( may be useful in the future, right now not needed unless you want different date outputs )
//
function time:New( object )
	local __class = object or { };
	setmetatable( __class, self );

	return __class;
end


//
// Change the date output...
//
function time:SetDateOutput( _output )
	self.__DateOutput = _output;
end


//
// Output the date of a timestamp
//
function time:When( _when )
	return os.date( self.__DateOutput, _when );
end


//
// Current Timestamp with date output
//
function time:Now( )
	return self:When( self:Stamp( ) );
end


//
// The beginning, some servers it is Jan 1 1970, others it is Dec 31 1969
//
function time:Beginning( )
	return self:When( 0 );
end


//
// Grab the current time stamp.
//
function time:Stamp( )
	return os.time( );
end


//
// Simple Helper-Function which provides time-elapsed from a set-start time; use _func to change the time function from os.time to CurTime, etc...
// TIME-ELAPSED: ( NOW - START )
//
function time:Elapsed( _start, _func )
	return ( ( isfunction( _func ) && _func( ) || self:Stamp( ) ) - ( _start || 0 ) );
end


//
// Simple Helper-Function which provides time-remaining from a set-start time; use _func to change the time function from os.time to CurTime, etc...
// TIME-REMAINING: ( DURATION - ( NOW - START ) )
//
function time:Remaining( _start, _duration, _func )
	return _duration - self:Elapsed( _start, _func );
end


//
// Simple Helper-Function which provides a fraction from 0 to 1
// 0 - 1 Time Fraction: ELAPSED / DURATION
//
function time:Fraction( _start, _duration, _func )
	if ( _duration != 0 ) then
		return math.Clamp( self:Elapsed( _start, _func ) / _duration, 0, 1 );
	end

	return 0;
end


//
// Example..
//
function time:GenerateExample( )
	print( "string.safe( time:Now( ) );", string.safe( time:Now( ) ) );
	print( "time:Stamp( );\t\t", time:Stamp( ) );
	print( "time:Beginning( );\t", time:Beginning( ) );
	
	print( "" );
	print( "Now, lets look at some basic time formulas..." );

	print( "" );
	print( "Time-Elapsed = ( CurrentTime - StartTime )" );
	print( "Time Elapsed [ 5 seconds ]:", time:Elapsed( time:Stamp( ) - 5 ) );
	print( "Time Elapsed [ 5 seconds ]:", time:Elapsed( CurTime( ) - 5, CurTime ) );

	print( "" );
	print( "Time-Remaining = ( Duration / TimeElapsed )\n\t... OR ...\n\t\t( Duration / ( CurrentTime - StartTime ) )" );
	print( "Time Remaining [ 15 seconds ]:", time:Remaining( os.time( ) - 5, 20 ) );
	print( "Time Remaining [ 15 seconds ]:", time:Remaining( CurTime( ) - 5, 20, CurTime ) );

	print( "" );
	print( "Time-Fraction = ( TimeElapsed / Duration )\n\t... OR ...\n\t\t( ( CurrentTime - StartTime ) / Duration )" );
	print( "Time Fraction [ 1/9 seconds ]:", time:Fraction( os.time( ) - 1, 9 ) );
	print( "Time Fraction [ 2/9 seconds ]:", time:Fraction( os.time( ) - 2, 9 ) );
	print( "Time Fraction [ 3/9 seconds ]:", time:Fraction( os.time( ) - 3, 9 ) );
	print( "Time Fraction [ 4/9 seconds ]:", time:Fraction( os.time( ) - 4, 9 ) );
	print( "Time Fraction [ 5/9 seconds ]:", time:Fraction( os.time( ) - 5, 9 ) );
	print( "Time Fraction [ 6/9 seconds ]:", time:Fraction( os.time( ) - 6, 9 ) );
	print( "Time Fraction [ 7/9 seconds ]:", time:Fraction( os.time( ) - 7, 9 ) );
	print( "Time Fraction [ 8/9 seconds ]:", time:Fraction( os.time( ) - 8, 9 ) );
	print( "Time Fraction [ 9/9 seconds ]:", time:Fraction( os.time( ) - 9, 9 ) );
	
	print( );
	print( "Time-Elapsed is useful for understanding how much time has passed since you started recording time ( Basic Stop-Watch )" );
	print( "Time-Remaining is useful for knowing when something will happen ( Simple Egg-Timer )" );
	print( "Time-Fraction ( ie 0-1 ) is incredibly useful for creating progress-bars and other systems that can be scaled based on time-left..." )
	
end
-- time:GenerateExample( );