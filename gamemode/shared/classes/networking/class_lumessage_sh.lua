//
// LUMessage Meta-Table / Class - Josh 'Acecool' Moser
// This is what UMSG will send and receive, it contains all of the functions
// and a way to input / output...
//


//
// This is what I would've done in order to support Reset; I may do this if people need it
// but for now, I'm leaving this dead in the water. It will be included with the AcecoolDev_Base
// just in case anyone wants to play with it.
//
// But the code won't load
//
do return; end


// Set up a new table, which will become a metatable
LUMessage = { };

// Set up a default table for recipients so it'll be copied over
LUMessage.__data = {
	name = "";
	recipients = { };
};

// What gets requested when LUMessage is accessed
LUMessage.__index = LUMessage;

// For advanced Typing
LUMessage.__type = "LUMessage";

LUMessage.__tostring = function( )
	return "LUMessage";
end


//
//
//
if ( SERVER ) then
	// We'll send all LUMessages through one message name ( adding more names has no benefit, but this
	// is all in the back-end; the front-end will appear as though there are multiple message names )
	util.AddNetworkString( LUMessage.__type );


	//
	//
	//
	function LUMessage:Start( _name, _recipients )
		PENDING_UMSG:SetName( _name );
		PENDING_UMSG:SetRecipients( _recipients );
	end


	//
	// This can either be done like this, where all net functions are called at once, or all functions
	// can be called on the fly. Either will work, this method requires more planning but in order to
	// support Reset on the other end, a little bit of extra planning isn't a bad idea...
	//
	function LUMessage:End( )
		// Start the message...
		net.Start( LUMessage.__type );
			// Set the name so the client knows what function to access
			net.WriteString( _name );

			// Process other data bits...
		// Depending on type of recipients... Send / Broadcast...
	end


	//
	//
	//
	function LUMessage:SetName( _name )
		self.__data.name = _name;
	end


	//
	//
	//
	function LUMessage:SetRecipients( _recipients )
		self.__data.recipients = _recipients;
	end

	function umsg.Start( _name, _recipients )
		if ( !PENDING_UMSG ) then
			PENDING_UMSG = UserMessage( );
			PENDING_UMSG:Start( );
		else
			error( "#umsg_already_pending_finish_sending_first" );
		end
	end

	function umsg.End( )
		if ( PENDING_UMSG ) then
			PENDING_UMSG:End( );
		else
			error( "#umsg_not_pending_create_one_first" );
		end
	end

	//
	// Returns the list of players in the filter in table format ( what net-messages require )
	//
	function LUMessage:Angle( )

	end

	function umsg.Angle( )

	end
	-- umsg/Angle
	-- umsg/Bool
	-- umsg/Char
	-- umsg/End
	-- umsg/Entity
	-- umsg/Float
	-- umsg/Long
	-- umsg/PoolString
	-- umsg/Short
	-- umsg/Start
	-- umsg/String
	-- umsg/Vector
	-- umsg/VectorNormal

	-- umsg.Start( string name, Player filter )
	-- umsg.End( )
	-- umsg.Angle( Angle angle )
	-- umsg.Bool( boolean angle )
	-- umsg.Char( number char )
	-- umsg.Entity( Entity entity )
	-- umsg.Float( number float )
	-- umsg.Long( number int )
	-- umsg.Short( number short )
	-- umsg.String( string string )
	-- umsg.Vector( Vector vector )
	-- umsg.VectorNormal( Vector normal )

	-- umsg.PoolString( string string )
else
	-- ReadAngle -- angle
	-- ReadBool -- bool
	-- ReadChar -- signed char and returns a number from -127 to 127 representing the ascii value of that char.
	-- ReadEntity -- Reads a short representing an entity index and returns the matching entity handle.
	-- ReadFloat -- Reads a 4 byte float from the bitstream and returns it.
	-- ReadLong -- Reads a 4 byte long from the bitstream and returns it.
	-- ReadShort -- Reads a 2 byte short from the bitstream and returns it.
	-- ReadString -- string
	-- ReadVector -- can't read normalized
	-- ReadVectorNormal -- can't read normal vectors
	-- Reset -- Rewinds the bitstream so it can be read again.
end



//
// Create a new LUMessage
//
function LUMessage:New( _data )
	local __class = _data || { };
	setmetatable( __class, self );
	return __class;
end


//
// Create a new LUMessage
//
function UserMessage( _data )
	return LUMessage:New( _data );
end