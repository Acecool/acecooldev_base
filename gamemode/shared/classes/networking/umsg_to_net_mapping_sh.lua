//
// Converts UMsg, NWVars, etc... to use my networking system - Josh 'Acecool' Moser
//


//
//
//
if ( SERVER ) then
	//
	// We do need to store some data from an active user-message, namely the recipients...
	// We'll only use one net-message to transfer all these umsgs through...
	//
	PENDING_USERMESSAGE = PENDING_USERMESSAGE || { };
	util.AddNetworkString( "LUMessage" );
	util.AddNetworkString( "SendPlayerLua" );
	util.AddNetworkString( "SendUserMessage" );


	//
	// Broadcasts up to 64KB of Lua to all users...
	//
	function BroadcastLua( _data )
		net.Start( "SendPlayerLua" );
			net.WriteString( _data );
		net.Broadcast( );
	end


	//
	// Sends a user-message...
	//
	function SendUserMessage( _name, _p, ... )
		local _tab = { ... };
		local _count = table.Count( _tab );
		local _net = net.WriteVars;

		net.Start( "SendUserMessage" );
			net.WriteString( _name );
			if ( _count > 0 ) then
				for k, v in pairs( _tab ) do
					local _typeid = TypeID( v );
					local _write_as = networking.Senders[ _typeid ];

					if ( _write_as ) then
						_write_as( v );
					else
						ErrorNoHalt( "Error sending user message data: " .. _name );
					end
				end
			end
		net.Send( _p );
	end


	//
	// Replacement for usermessage SendLua - no more 255 byte restriction. Now 64kb.
	// This could be "unlimited" with my networking system; if there are enough requests, I'll do it...
	//
	function META_PLAYER:SendLua( _data )
		net.Start( "SendPlayerLua" );
			net.WriteString( _data );
		net.Send( self );
	end


	//
	// Start just uses net.Start, plus a net.WriteString for the name of the message...
	//
	function umsg.Start( _name, _players )
		//
		-- AssertArgs( TYPE_STRING, _name );

		// Make sure we're not overlapping...
		if ( PENDING_USERMESSAGE.active ) then
			if ( game.DeveloperMode( ) ) then
				ErrorNoHalt( "You must dispatch the current UMSG using umsg.End( ) before Starting another!" );
				print( "PENDING_USERMESSAGE DATA: " .. PENDING_USERMESSAGE.name );
			end

			PENDING_USERMESSAGE = { };
			return;
			-- umsg.End( );
		end

		// Set the pending as active so we don't overlap
		PENDING_USERMESSAGE.name = _name;
		PENDING_USERMESSAGE.active = true;
		PENDING_USERMESSAGE.recipients = _players;

		// Write the starting data...
		net.Start( "LUMessage" );
			net.WriteString( _name ); // || "UnknownLUMessage" );
	end


	//
	// Work-horse functions
	//


	//
	//
	//
	function umsg.Angle( _ang )
		-- AssertArgs( TYPE_ANGLE, _ang );
		net.WriteAngle( _ang );
	end


	//
	//
	//
	function umsg.Bool( _bool )
		-- AssertArgs( TYPE_BOOL, _bool );
		net.WriteBool( _bool );
	end


	//
	//
	//
	function umsg.Char( _char )
		-- AssertArgs( TYPE_NUMBER, _char );
		local _char = ( isstring( _char ) && string.char( _char ) || _char );
		net.WriteInt( _char, 8 );
	end


	//
	//
	//
	function umsg.Entity( _ent )
		local _valid = IsValid( _ent );
		local _id = -1;
		if ( _valid ) then
			_id = _ent:EntIndex( );
		end
		net.WriteInt( _id, 16 );
	end


	//
	//
	//
	function umsg.Float( _float )
		-- AssertArgs( TYPE_NUMBER, _float );
		net.WriteFloat( _float );
	end


	//
	//
	//
	function umsg.Long( _long )
		-- AssertArgs( TYPE_NUMBER, _long );
		net.WriteInt( _long, 32 );
	end


	//
	//
	//
	function umsg.Short( _short )
		-- AssertArgs( TYPE_NUMBER, _short );
		net.WriteInt( _short, 16 );
	end


	//
	//
	//
	function umsg.String( _string )
		-- AssertArgs( TYPE_STRING, _string );
		net.WriteString( _string );
	end


	//
	//
	//
	function umsg.Vector( _vector )
		-- AssertArgs( TYPE_VECTOR, _vector );
		net.WriteVector( _vector );
	end


	//
	//
	//
	function umsg.VectorNormal( _vector )
		-- AssertArgs( TYPE_VECTOR, _vector );
		net.WriteVector( _vector );
	end


	//
	// End is what sends it. Somewhat annoying levels of code to sort out recipient-filter from table from user from nothing.
	//
	function umsg.End( )
		// Convert _players from RecipientFilter to table...
		local _players = PENDING_USERMESSAGE.recipients;
		if ( istable( _players ) || type( _players ) == "LRecipientFilter" ) then
			if ( _players.GetRecipients ) then
				PENDING_USERMESSAGE.recipients = _players:GetRecipients( );
			else
				PENDING_USERMESSAGE.recipients = _players;
			end
		else
			if ( IsValid( _players ) ) then
				PENDING_USERMESSAGE.recipients = _players;
			end
		end

		// Broadcast or send...
		if ( !PENDING_USERMESSAGE.recipients ) then
			net.Broadcast( );
			if ( game.DeveloperMode( ) ) then
				print( "UMSG Broadcasting: " .. PENDING_USERMESSAGE.name );
			end
		else
			net.Send( PENDING_USERMESSAGE.recipients );
			if ( game.DeveloperMode( ) ) then
				print( "UMSG Sending: " .. PENDING_USERMESSAGE.name );
			end
		end

		// Reset the pending message
		PENDING_USERMESSAGE = { };
	end
else
	//
	// The work-horse reader functions and conversions...
	//


	//
	//
	//
	function usermessage:ReadAngle( )
		return net.ReadAngle( );
	end


	//
	//
	//
	function usermessage:ReadBool( )
		return net.ReadBool( );
	end


	//
	//
	//
	function usermessage:ReadChar( )
		return net.ReadInt( 8 );
	end


	//
	//
	//
	function usermessage:ReadEntity( )
		local _id = net.ReadInt( 16 );
		if ( _id == -1 ) then
			return NULL;
		else
			return Entity( _id ) || NULL;
		end
	end


	//
	//
	//
	function usermessage:ReadFloat( )
		return net.ReadFloat( );
	end


	//
	//
	//
	function usermessage:ReadLong( )
		return net.ReadInt( 32 );
	end


	//
	//
	//
	function usermessage:ReadShort( )
		return net.ReadInt( 16 );
	end


	//
	//
	//
	function usermessage:ReadString( )
		return net.ReadString( );
	end


	//
	//
	//
	function usermessage:ReadVector( )
		return net.ReadVector( );
	end


	//
	//
	//
	function usermessage:ReadVectorNormal( )
		return net.ReadVector( );
	end


	//
	//
	//
	function usermessage:Reset( )
		ErrorNoHalt( "usermessage.Reset( ) is the one function I didn't convert because it would require too much overhead.. Store the reads into vars and go back to them that way!" );
	end


	//
	// One Net Message to receive them all
	//
	net.Receive( "LUmessage", function( _len )
		// We need the name
		local _name = net.ReadString( );

		// To get the callback
		local _callback = usermessage.__hooks[ _name ];
		local _callback_other = usermessage.GetTable( )[ _name ];

		// To call the callback
		if ( _callback ) then
			// And we'll pass the usermessage function library through, because normally it is the "msg/data"
			// var which references the actual data. I didn't set up an entire table and just did a quick and
			// dirty conversion so it just maps the functions essentially. Reset won't work, but with that being
			// the only downside, I can live with it.
			_callback( usermessage );
		elseif( _callback_other ) then
			_callback_other( usermessage );
		else
			// Error if something wrong happened...
			error( "Undefined User Message. Client Code / Addon doesn't contain usermessage.Hook( " .. _name .. ", function( _um ) ... end );" );
		end
	end );


	//
	// Likely to fail - working on something...
	//
	net.Receive( "SendUserMessage", function( _len )
		local _name = net.ReadString( );
		local _callback = usermessage.__hooks[ _name ];
		local _callback_other = usermessage.GetTable( )[ _name ];

		if ( _callback ) then
			_callback( usermessage );
		elseif( _callback_other ) then
			_callback_other( usermessage );
		end
	end );


	//
	//
	//
	net.Receive( "SendPlayerLua", function( _len )
		local _data = net.ReadString( );
		if ( _data && _data != "" ) then
			if ( game.DeveloperMode( ) ) then
				RunStringEx( _data, "Acecool -> SendPlayerLua ->" );
			else
				RunString( _data );
			end
		end
	end );


	//
	// This registers the user-message hook into the function library
	//
	usermessage.__hooks = usermessage.__hooks || { };
	function usermessage.Hook( _name, _callback )
		if ( game.DeveloperMode( ) ) then
			print( "Adding UMSG Hook: ", _name );
		end
		usermessage.__hooks[ _name ] = _callback;
	end


	//
	// Since GetTable isn't redirected, we can scan it so if umsg.Hook wasn't included quickly enough, it'll still
	// process all pre-existing functions, then remove them from the table so that it will be auto-refresh compatible.
	//
	for _name, v in pairs( usermessage:GetTable( ) ) do
		if ( game.DeveloperMode( ) ) then
			print( "Adding UMSG Hook: ", _name );
		end
		usermessage.Hook( _name, v.Function );

		// TODO: For the time being, we'll leave the old messages active...
		-- usermessage:GetTable( )[ _name ] = nil;
	end


	//
	// Essentially the same thing as net.Incoming ( can be used to track down and replace other umsgs )..
	// TODO: Reroute messages from here, or not....
	//
	-- function usermessage.IncomingMessage( _name, _msg )
		-- if ( game.DeveloperMode( ) ) then
			-- print( "UMSG: ", _name, ", ", _msg );
		-- end
	-- end
end