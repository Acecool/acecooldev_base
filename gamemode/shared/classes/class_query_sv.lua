//
// Query Class - Josh 'Acecool' Moser
// http://dev.mysql.com/doc/refman/5.0/en/func-op-summary-ref.html
//


//
// Note: As of right now this is a small taste of what this builder will become. It is in its infancy
// and it just joins strings together based on a set of rules. It works for simple to complex queries
// including the pivot function which can turn many rows into 1, with columns populated by row data.
// In the future, to allow maximum compatibility with SQLite, and other SQL service types, I will be
// redesigning this to build the strings differently. It'll simply collect the mandatory data as it
// does now, but it'll allow the strings to be output in many different ways.
//
// Right now, this uses the SET method; I'll add in the VALUE method in the future when I move things around.
// Example: https://dl.dropboxusercontent.com/u/26074909/tutoring/database/basic_queries_and_query_formats_used_to_check_for_existing_row.lua.html
//
// Additionally, I'll add contingency queries such as ON DUPLICATE UPDATE, etc..
// I plan on keeping the syntax the same as I feel that it is simple and straightforward, however,
// I will be adding a reset option so that the same query object can be re-used in different queries,
// and I also plan on adding query:Insert, query:Select, query:Update, etc.. as simplified helper commands
// which some users prefer, and which can make for 1 line queries being built.
//
// I'd like your input. Let me know what you'd like to see, what you think of the syntax for building these
// prepared statements, and where you'd like to see this go. I'd be happy to respond and potentially
// tailor this prepared SQL statement builder towards your needs.
//
// For examples, please refer to the documentation directory.
//
// As of right now, this query builder is not suited for redistribution with addons. When it is done, the
// option to allow it to be included in mods may be up for discussion. In its current, unfinished state,
// I believe the best option would be to wait until it is finished before making the decision. If you want
// to generate a query using this tool, and use that generated query in an addon, you are free to do so with
// a link to my repo, and my name, included in the file that uses the query so others know where they can
// have prepared statements created for them. Note: For simple queries it won't matter; for complex queries,
// such as Pivots or Joins, please include the link back.
//
// Josh 'Acecool' Moser - AcecoolDev_Base : https://bitbucket.org/Acecool/acecooldev_base
//


//
// ENUMerations
//

// Query Types
QUERY_SELECT 								= 0;
QUERY_UPDATE 								= 1;
QUERY_INSERT 								= 2;
QUERY_REPLACE_INTO 							= 3;

// Join Types
QUERY_JOIN_INNER 							= 0;
QUERY_JOIN_OUTER 							= 1;
QUERY_JOIN_FULL_OUTER 						= 2;
QUERY_JOIN_FULL 							= 3;
QUERY_JOIN_RIGHT 							= 4;
QUERY_JOIN_LEFT 							= 5;
QUERY_JOIN_LEFT_EXCLUDING_INNER 			= 6;
QUERY_JOIN_RIGHT_EXCLUDING_INNER 			= 7;
QUERY_JOIN_OUTER_EXCLUDING_INNER 			= 8;


//
// String Building
//
local QUERY_STRINGS = {
	[ QUERY_SELECT ] 						= { start = "\nSELECT\t", after = "\n\nFROM " };
	[ QUERY_UPDATE ]						= { start = "\nUPDATE\t", after = "\n\nSET " };
	[ QUERY_INSERT ] 						= { start = "\nINSERT INTO\t", after = "\n\nSET " };
	[ QUERY_REPLACE_INTO ] 					= { start = "\nREPLACE INTO\t", after = "\n\nSET " };
};

local QUERY_JOIN_STRINGS = {
	[ QUERY_JOIN_INNER ] 					= "INNER JOIN";
	[ QUERY_JOIN_OUTER ] 					= "OUTER JOIN";
	[ QUERY_JOIN_FULL_OUTER ] 				= "FULL OUTER JOIN";
	[ QUERY_JOIN_FULL ] 					= "FULL JOIN";
	[ QUERY_JOIN_RIGHT ]					= "RIGHT JOIN";
	[ QUERY_JOIN_LEFT ] 					= "LEFT JOIN";
	[ QUERY_JOIN_LEFT_EXCLUDING_INNER ] 	= "LEFT JOIN EXCLUDING INNER JOIN";
	[ QUERY_JOIN_RIGHT_EXCLUDING_INNER ] 	= "RIGHT JOIN EXCLUDING INNER JOIN";
	[ QUERY_JOIN_OUTER_EXCLUDING_INNER ] 	= "OUTER JOIN EXCLUDING INNER JOIN";
};


//
// Our "object"
//
query = query || { };
query.__index = query;


//
// Creates a new instance of the class.
//
function query:New( )
	return setmetatable( {
		__index = query;
		__type = "Query";
		__query = {
			table_count = 0;
			querytype = QUERY_SELECT;
			tables = { };
			columns = { };
			column_pivots = "";
			selectall = false;
			joins = "";
			where_count = 0;
			where = "";
			limit = nil;
		}
	}, query );
end


//
// Defines the query type ( SELECT is default )
//
function query:SetQueryType( _type )
	self.__query.querytype = _type
end


//
// Defines the query type ( SELECT is default )
//
function query:SetType( _type )
	self:SetQueryType( _type );
end


//
//
//
function query:AddTable( _table, _columns, _values )
	// Make sure values are set if this is an update query, or insert.
	local _bUpdate = ( self.__query.querytype == QUERY_UPDATE || self.__query.querytype == QUERY_INSERT );

	local _bTypesMatch = _columns && _values && ( ( istable( _columns ) && istable( _values ) ) || ( !istable( _columns ) && !istable( _values ) ) ) // || ( isstring( _columns ) && isstring( _values ) )
	if ( _bUpdate && ( !_values || !_bTypesMatch ) ) then
		MsgC( COLOR_CYAN, language.GetPhrase( "#query_builder_expects_values" ) .. "\n" );
		return;
	end

	if ( istable( _columns ) ) then
		for k, v in pairs( _columns ) do
			if ( v != "*" ) then
				if ( _bUpdate ) then
					table.insert( self.__query.columns, _table .. "." .. v .. " = " .. self:Escape( _values[ k ] ) );
				else
					table.insert( self.__query.columns, _table .. "." .. v );
				end
			end
		end
	elseif ( !_columns || isstring( _columns ) ) then
		_columns = ( !_columns ) && "*" || _columns;
		if ( _columns == "*" ) then
			self.__query.selectall = true;
			_columns = "*";
		end

		if ( _bUpdate ) then
			table.insert( self.__query.columns, _table .. "." .. _columns .. " = " .. self:Escape( _values ) );
		else
			table.insert( self.__query.columns, _table .. "." .. _columns );
		end
	end

	table.insert( self.__query.tables, _table );
	self.__query.table_count = self.__query.table_count + 1;
end


//
//
//
function query:AddPivot( _enum_table, _data_table, _enum_key_column, _data_value_column, _columns )
	assert( isstring( _enum_table ) == true, "AddPivot( String ENUM_TABLE, String DATA_TABLE, String ENUM_KEY_COLUMN, String DATA_VALUE_COLUMN, String/Table FauxColumns ) requires ENUM_TABLE as String" );
	assert( isstring( _data_table ) == true, "AddPivot( String ENUM_TABLE, String DATA_TABLE, String ENUM_KEY_COLUMN, String DATA_VALUE_COLUMN, String/Table FauxColumns ) requires DATA_TABLE as String" );
	assert( !isstring( _enum_key ) == true, "AddPivot( String ENUM_TABLE, String DATA_TABLE, String ENUM_KEY_COLUMN, String DATA_VALUE_COLUMN, String/Table FauxColumns ) requires ENUM_KEY_COLUMN as String" );
	assert( !isstring( _data_value ) == true, "AddPivot( String ENUM_TABLE, String DATA_TABLE, String ENUM_KEY_COLUMN, String DATA_VALUE_COLUMN, String/Table FauxColumns ) requires enumeration DATA_VALUE_COLUMN as String" );
	assert( ( istable( _columns ) || isstring( _columns ) ) == true, "AddPivot( String ENUM_TABLE, String DATA_TABLE, String ENUM_KEY_COLUMN, String DATA_VALUE_COLUMN, String/Table FauxColumns ) requires FauxColumns as either String, or Table" );

	// Will be inserted after columns; uses comma because last select won't have comma, and last one of this won't use comma... Cheap way...
	local _pivot = ",\n\tGROUP_CONCAT( IF ( " .. _enum_table .. "." .. _enum_key_column .. " = %s, " .. _data_table .. "." .. _data_value_column .. ", NULL ) ) AS %s"
	if ( istable( _columns ) ) then
		for k, v in pairs( _columns ) do
			self.__query.column_pivots = self.__query.column_pivots .. string.format( _pivot, SQLStr( v ), SQLStr( v, true ) );
		end
	elseif( isstring( _columns ) ) then
		self.__query.column_pivots = self.__query.column_pivots .. string.format( _pivot, SQLStr( _columns ), SQLStr( _columns, true ) );
	end
end


//
//
//
function query:AddJoin( _type, _table1, _table2, _field1, _field2 )
	local _type = ( !_type || _type == "" ) && QUERY_JOIN_INNER || _type;
	local _field2 = ( !_field2 || _field2 == "" ) && _field1 || _field2;
	if ( !_table1 || _table1 == "" ) then error( "You must define _table1" ); end
	if ( !_table2 || _table2 == "" ) then error( "You must define _table2" ); end
	if ( !_field1 || _field1 == "" ) then error( "You must define _field1" ); end

	local _joinstring = QUERY_JOIN_STRINGS[ _type ];
	self.__query.joins = self.__query.joins .. "\n\n" .. _joinstring .. " " .. _table2 .. "\n\tAS " .. _table2 .. "\n\tON " .. _table1 .. "." .. _field1 .. " = " .. _table2 .. "." .. _field2;
end


//
// Generate "[, ]column='value'" string or "[AND || OR ]column='value'" string.
//
function query:ColumnValueString( _comma, _key, _value, _noquotes )
	return ( ( isbool( _comma ) && _comma ) && ", " || ( isstring( _comma ) && _comma || "" ) ) .. _key .. " = " .. self:Escape( _value, _noquotes );
end


//
//
//
function query:WhereString( _where )
	return ( ( _where ) && ( " WHERE " .. _where ) || "" );
end


//
//
//
function query:AddWhere( _table, _column, _value, _logic )
	self.__query.where_count = self.__query.where_count + 1;
	-- _logic = ( !_logic ) && nil || string.upper( _logic || "" );
	-- print( _logic )
	if ( self.__query.where_count > 1 ) then
		if ( !_logic || ( _logic != "AND" && _logic != "OR" ) ) then
			MsgC( COLOR_CYAN, language.GetPhrase( "#query_builder_where_expects_and_or_logic" ) .. "\n" );
			return;
		end
		self.__query.where = self.__query.where .. _logic .. _table .. "." .. _column .. " = " .. self:Escape( _value );
	else
		self.__query.where = self.__query.where .. "\n\nWHERE " .. _table .. "." .. _column .. " = " .. self:Escape( _value );
	end
end


//
//
//
function query:LikeString( _like )
	return ( ( _like ) && ( " LIKE " .. _like ) || "" );
end


//
//
//
function query:AddLike( _table, _column, _value, _logic )

end


//
//
//
function query:LimitString( _limit )
	local _output = ( ( self.__query.limit ) && ( "\nLIMIT " .. self.__query.limit ) || "" );
	_output = _output .. ( ( self.__query.limit && self.__query.limit_end ) && ( ", " .. self.__query.limit_end ) || "" )
	return _output;
end


//
//
//
function query:AddLimit( _limit, _limit_end )
	self.__query.limit = _limit;
	self.__query.limit_end = _limit_end;
end


//
//
//
function query:Escape( _string, _noquotes )
	return SQLStr( _string, _noquotes );
end


//
//
//
function query:FieldsString( _fields, _prefix )
	local _string = "";
	local _comma = "";

	if ( istable( _fields ) ) then
		local _count = 1;
		for k, v in pairs( _fields ) do
			if ( _count > 1 ) then
				_comma = ",\n\t";
			end

			_string = _string .. _comma .. ( _prefix || "" ) .. v;
			_count = _count + 1;
		end
	else
		_string = _fields || "";
	end

	return _string;
end


//
//
//
function query:Build( )
	local _query = self.__query
	local _tablecount = _query.table_count;

	if ( _tablecount == 0 ) then MsgC( COLOR_CYAN, language.GetPhrase( "#query_builder_expects_tables" ) .. "\n" ); return; end

	local _type = _query.querytype;
	local _tables = _query.tables;

	local _table = _tables[ 1 ];
	local _typedata = QUERY_STRINGS[ _type ];

	local _fieldsstring = ( _query.selectall && _type == DB_SELECT_QUERY ) && "*" || ( self:FieldsString( self.__query.columns ) || "" );
	local _wherestring = self.__query.where; //( _type != DB_INSERT_QUERY ) && self:WhereString( _where ) || "";
	local _limitstring = ( _type != DB_INSERT_QUERY ) && self:LimitString( _limit ) || "";
	local _joinstring = _query.joins;


	local _q = _typedata.start;
	if ( _type == QUERY_INSERT ) then
		_q = _q .. _table .. _typedata.after .. _fieldsstring .. _wherestring .. _limitstring;
	elseif ( _type == QUERY_UPDATE ) then
		_q = _q .. _table .. " AS " .. _table .. _typedata.after .. _fieldsstring .. _wherestring .. _limitstring;
	elseif ( _type == QUERY_SELECT ) then
		_q = _q .. _fieldsstring .. self.__query.column_pivots .. _typedata.after .. _table .. " AS " .. _table .. _joinstring .. _wherestring .. _limitstring;
	end

	return _q;
end
query.__tostring = query.Build;