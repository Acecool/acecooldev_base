//
// Maps Class - Josh 'Acecool' Moser
//


//
-- if ( !map ) then
	map = { };
	map.__index		= map;
	map.__type		= "Maps";

	// Controls a few basic systems
	map.__config = {
		// Map Scale ( Used if you want everything in the game-mode to resize based on scale of map )
		scale		= 1;

		// Map origin
		origin		= Vector( 0, 0, 0 );

		// Light ENV entity ( for environment system )
		lightenv	= 0;
	};

	// Controls clientside map alterations
	map.__client = {
		// Sets up which textures the client should use on the map
		texture_replacements = { };
	};

	map.__scale		= 1;
	map.__origin	= Vector( 0, 0, 0 );
	map.light_env	= 0;
-- end


//
// Returns a safe Map Name string
//
function map:GetName( )
	return string.lower( game.GetMap( ) );
end


//
// Alias of GetName ( Works using map.Name( ), map.GetName( ), map:Name( ), map:GetName( ) )
//
function map:Name( )
	return map:GetName( );
end


//
// Will be added soon to allow spawn points to be added based on a TEAM ( a hook will be called so a specific
// id can be assigned if you're using a setup like darkrp where each job is a team.. This hook will allow you
// to assign whatever identifier you want for each team so TEAM_CITIZEN can be 1, as can be TEAM_TOW_TRUCK_DRIVER
// so spawns can be shared )
//
function map:AddSpawnPoint( _team, _pos, _ang )
	// Returns the LinkID
	local _team = hook.Call( "GetTeamLinks", GAMEMODE, _team );

	//

end


//
// Will return a position and angle spawn point that are free of obstructions and can be used ( at random )
//
function map:GetSpawnPoint( _team )
	// Returns the TeamID ( so multiple teams setup can share spawn-points )
	local _team = hook.Call( "GetTeamLinks", GAMEMODE, _team );

	//
end


//
// Will return all spawn-points associated with the team ( useful for spawning models/player.mdl grey-freeman
// for debugging. Will also use the same team id hook system...
//
function map:GetSpawnPoints( _team )

end


//
// Adds Entity; pos and ang support tables, but both must be a table with the same number of entries.
// Options support: invisible, frozen / freeze / lock / weld, droptofloor.
// Callback is a function that should be called on the spawned entity right before Spawn/Activate.
//
// I'm not very happy with how this is set up; I may change this in the future...
//
function map:AddEntity( _class, _pos, _model, _skin, _color, _options, _callback )
	//
	table.insert( map.__additions, {
		class = _class;
		positions = _pos;
		model = _model;
		skin = _skin;
		color = _color;
		options = _options;
		callback = _callback;
	} );
end


//
// Sets whether or not _ent:IsNPC( ) entities are removed from the map on startup
//
function map:RemoveEntityNPCs( _bool )
	map.__removals.npcs = _bool;
end


//
// Removes all instances of this entity class from the map on startup
//
function map:RemoveEntityByID( _id, ... )
	// Recursion
	if ( #{ ... } > 0 ) then self:RemoveEntityByID( ... ); end

	//
	map.__removals.by_id[ _id ] = true;
end


//
// Removes all instances of this entity class from the map on startup
//
function map:RemoveEntityByClass( _class, ... )
	// Recursion
	if ( #{ ... } > 0 ) then self:RemoveEntityByClass( ... ); end

	//
	map.__removals.by_class[ _class ] = true;
end


//
// Removes all instances of this entity model from the map on startup
//
function map:RemoveEntityByModel( _model, ... )
	// Recursion
	if ( #{ ... } >= 1 ) then self:RemoveEntityByModel( ... ); end

	//
	map.__removals.by_model[ _model ] = true;
end


//
// Removes all instances of this entity class which is using this specific model from the map on startup
//
function map:RemoveEntityByClassAndModel( _class, _model, ... )
	// Recursion
	if ( #{ ... } >= 2 ) then self:RemoveEntityByClassAndModel( ... ); end

	if ( !map.__removals.by_classmodel[ _class ] ) then map.__removals.by_classmodel[ _class ] = { }; end

	//
	map.__removals.by_classmodel[ _class ][ _model ] = true;
end


//
//
//
if ( CLIENT ) then
	//
	//
	//
	function map:AddTextureReplacement( _original, _replacement, ... )
		// Recursion
		if ( #{ ... } >= 2 ) then self:AddTextureReplacement( ... ); end

		local _replacements = map.__client.texture_replacements;

		//
		local _bOriginalString = isstring( _original );
		local _bReplacementString = isstring( _replacement );
		local _bReplacementTable = istable( _replacement );

		//
		if ( _bOriginalString && _bReplacementString ) then
			table.insert( _replacements, { original = _original; replacement = _replacement; } );
		elseif ( _bOriginalString && _bReplacementTable ) then
			-- table.insert( _replacements, { original = _original; replacement = _replacement; } );
			// ToDo
			error( "Error using AddTextureReplacement as AddTextureReplacementEx( " .. _original .. ", " .. _replacement .. " );" );
		else
			error( "Error using AddTextureReplacement( " .. _original .. ", " .. _replacement .. " );" );
		end
	end


	//
	//
	//
	function map:ProcessTextureReplacements( )
		local _replacements = map.__client.texture_replacements;
		if ( !istable( _replacements ) ) then error( "You must pass in a table for the texture replacer!" ); end

		// Process the textures.
		for k, v in pairs( _replacements ) do
			// Grab the original material and the replacement material
			local _originalMaterial = draw.GetMaterial( v.original );
			local _replacementMaterial = draw.GetMaterial( v.replacement );
			local _newTexture = _replacementMaterial:GetTexture( "$basetexture" );

			// If the replacement exists, process
			if ( _replacementMaterial && _newTexture ) then
				_originalMaterial:SetTexture( "$basetexture", _newTexture );
			else
				// Let the user know they're missing out on HD textures...
				chat.AddText( Color( 255, 0, 0, 255 ), "[" .. GAMEMODE.Name .. "] You're missing downloadable content *HD Map Replacement Textures*.\n" );
				chat.PlaySound( );
			end
		end
	end
end


//
// Map Removals Logic
//
if ( SERVER ) then
	//
	//
	//
	function map:ProcessMapRemovals( )
		// Our data source and our sub-tables
		local _removals		= map.__removals;
		local _npcs			= _removals.npcs;
		local _classes		= _removals.by_class;
		local _models		= _removals.by_model;
		local _classmodels	= _removals.by_classmodel;
		local _ids			= _removals.by_id;
		local _creation_ids	= _removals.by_creationid;

		// Process once using direct access
		for k, _ent in pairs( ents.GetAll( ) ) do
			// Worldspawn is NULL, skip it..
			if ( !IsValid( _ent ) ) then continue; end

			// base data from entity
			local _id			= _ent:GetID( );
			local _creationid 	= _ent:MapCreationID( );
			local _class		= _ent:GetClass( );
			local _model		= _ent:GetModel( );

			// Base Boolean Statements ( allows us to combine them below )
			local _bID			= _ids[ _id ];
			local _bCID			= _creation_ids[ _creationid ];
			local _bClass		= _classes[ _class ];
			local _bModel		= _models[ _model ];
			local _bClassModel	= _classmodels[ _class ] && _classmodels[ _class ][ _model ];

			// If ID or CreationID was entered, then we don't need to worry about class or model as the class/id shouldn't change
			if ( _bID || _bCID || _bClass || _bModel || _bClassModel || ( _npcs && _ent:IsNPC( ) ) ) then
				// Remove all constraints from the entity as they can count as entities
				if ( constraint.HasConstraints( _ent ) ) then
					for _, _constraint in pairs( CONSTRAINTS ) do
						constraint.RemoveConstraints( _ent, _constraint );
					end
				end

				// Use SafeRemoveEntity because its there ( avoid needing to check IsValid, etc.. even though we already did, but things can change quickly )
				SafeRemoveEntity( _ent );
			end
		end
	end


	//
	// This may be changed to ents.CreateEx aimed at including more functionality / 1 line creation
	//
	function map:CreateEnt( _class, _pos, _ang, _model, _skin, _color, _options, _callback )
		local _ent = ents.Create( _class );
		if ( !IsValid( _ent ) ) then ErrorNoHalt( "Error Creating Entity: ", v ); return; end

		if ( _model ) then
			if ( istable( _model ) ) then
				_ent:SetModel( table.Random( _model ) );
			else
				_ent:SetModel( _model );
			end
		end

		if ( _skin ) then _ent:SetSkin( _skin ); end
		if ( _color ) then _ent:SetColor( _color ); end
		if ( _options.droptofloor ) then _ent:DropToFloor( ); end
		if ( _options.scale ) then _ent:Scale( ); end
		if ( _options.invisible ) then
			_ent:SetRenderMode( 1 );
			_ent:SetNoDraw( true );
			_ent:DrawShadow( false );
			_ent:SetColor( Color( 0, 0, 0, 0 ) );
		end

		if ( _callback ) then
			_callback( _ent );
		end

		_ent:SetPos( _pos );
		_ent:SetAngles( _ang );

		_ent:Spawn( );
		_ent:Activate( );

		// Prevent object from moving, and weld object to world - prevents invisible props from moving.
		if ( _options.weld || _options.freeze || _options.lock || _options.frozen ) then
			local _phys = _ent:GetPhysicsObject( );
			if( IsValid( _phys ) ) then
				_phys:EnableMotion( false );
			end

			constraint.Weld( _ent, game.GetWorld( ) );
		end
	end


	//
	//
	//
	function map:ProcessMapAdditions( )
		for k, v in pairs( self.__additions ) do
			local _class = v.class;
			if ( !_class ) then continue; end

			if ( istable( v.positions ) ) then
				for _k, _v in pairs( v.positions ) do
					map:CreateEnt( _class, _v.pos, _v.ang, v.model, v.skin, v.color, v.options, v.callback )
				end
			end
		end
	end


	//
	// This is where the maps/ folders gets loaded. It should have no trouble loading before this one.
	//
	function GM:SetupMap( )
		// Simple output...
		MsgC( COLOR_GREEN, "[" .. GAMEMODE.Name .. "] MapRemovals: " );
		MsgC( COLOR_AMBER, string.lower(game.GetMap( ) ) );
		MsgC( COLOR_CYAN, "!\n" );

		// Controls which entities, etc gets removed from the map
		map.__removals = {
			npcs			= false;
			by_id			= { };
			by_creationid	= { };
			by_class		= { };
			by_model		= { };
			by_classmodel	= { };
		};

		// Ensure config files loaded and removals are made clear
		hook.Call( "SetupMapRemovals", GAMEMODE );
		map:ProcessMapRemovals( );
		hook.Call( "PostMapEntityRemovals", GAMEMODE );


		// Simple output...
		MsgC( COLOR_GREEN, "[" .. GAMEMODE.Name .. "] MapAdditions: " );
		MsgC( COLOR_AMBER, string.lower(game.GetMap( ) ) );
		MsgC( COLOR_CYAN, "!\n" );

		// Controls entity additions
		map.__additions = {

		};

		// Ensure config files loaded and removals are made clear
		hook.Call( "SetupMapAdditions", GAMEMODE );
		map:ProcessMapAdditions( );
		hook.Call( "PostMapAdditions", GAMEMODE );
	end

	concommand.Add( "loadmap", function( _p, _cmd, _args )
		// Make sure Player is NULL or IsSuperAdmin, otherwise STOP. NULL player is RCON user...
		if ( _p != NULL && !_p:IsSuperAdmin( ) ) then Log( _p, language.GetPhrase( "loadgm_log", language.__default ) ); return; end

		hook.Call( "SetupMap", GAMEMODE );
	end );

	//
	// Additions to the map...
	//
	hook.Add( "PostMapEntityRemovals", "PostMapEntityRemovals:MapAdditions", function( )

	end );
else
	//
	//
	//
	function GM:SetupClientMap( _p )
		print( "Setting up Clientside Map Experience!" );

		// Requires some time so MAC users don't crash...
		timer.Simple( 3, function( )
			hook.Call( "SetupMapTextures", GAMEMODE, _p );
			map:ProcessTextureReplacements( );
		end );

		hook.Call( "PostSetupClientMap", GAMEMODE, _p );
	end;
end


//
// Map Removals Logic
//
if ( SERVER ) then
	do return; end
	hook.Add( "InitPostEntity", "InitPostEntity:MapEntityRemovals", function( )
		//
		MsgC( COLOR_CYAN, "Setting up Map Entity Removals!\n" );

		// Ensure config files loaded and removals are made clear
		hook.Call( "SetupMapRemovals", GAMEMODE );

		//
		MsgC( COLOR_CYAN, "Done setting up Map Entity Removals, acting!\n" );

		// Our
		local _mapdata		= map.__data;
		local _removals		= _mapdata.entity_removals;

		// Our sub-tables
		local _classes		= _removals.by_class;
		local _models		= _removals.by_model;
		local _ids			= _removals.by_id;
		local _creation_ids	= _removals.by_creationid;

		//
		local _output = "";

		// Process once using direct access
		for k, _ent in pairs( ents.GetAll( ) ) do
			//
			if ( !IsValid( _ent ) ) then MsgC( COLOR_RED, "*" ); continue; end

			// base data from entity
			local _id			= _ent:GetID( );
			local _creationid 	= _ent:MapCreationID( );
			local _class		= _ent:GetClass( );
			local _model		= _ent:GetModel( );

			// Link the data
			local _mid			= _ids[ _id ] || { };
			local _mcid			= _creation_ids[ _creationid ] || { };
			local _mclass		= _classes[ _class ] || { };
			local _mmodel		= _models[ _model ] || { };

			// Because there can only be one entity but many models, we need to cross tables under certain circumstances
			if ( _mid || _mcid || _mclass || _mmodel ) then
				// Base Boolean Statements ( allows us to combine them below )
				local _bID		= ( _mid.id && _id == _mid.id );
				local _bCID		= ( _mcid.creationid && _mcid.creationid == _creationid );
				local _bClass	= ( _mclass.class && _mclass.class == _class );
				local _bModel	= ( _mmodel.model && _mmodel.model == _model );

				// Class Match can only be true if there was a class match and no Model entered, or class match AND model match
				_bClass = _bClass && ( !_mmodel.class || _mmodel.class == _class );

				// Model Match can only be true if there was a model match and no class entered, or model match AND class match
				_bModel	= _bModel && ( !_mclass.model || _mclass.model == model );

				// If ID or CreationID was entered, then we don't need to worry about class or model as the class/id shouldn't change
				if ( _bID || _bCID || _bClass || _bModel ) then
					_output = _output .. "ID: " .. toprint( _id );
					_output = _output .. "\tCID: " .. toprint( _creationid );
					_output = _output .. "\tClass: " .. toprint( _class );
					_output = _output .. "\tModel: " .. toprint( _model );
					_output = _output .. "\t_bID: " .. toprint( _bID );
					_output = _output .. "\t_bCID: " .. toprint( _bCID );
					_output = _output .. "\t_bClass: " .. toprint( _bClass );
					_output = _output .. "\t_bModel: " .. toprint( _bModel );
					_output = _output .. "\n";
					SafeRemoveEntity( _ent );
				end
			end
		end

		fileio:Write( "debugging", "map_entity_removals_debugging", toprint( _output ) );

		// Another hook for other cleanup purposes, or whatever
		hook.Call( "PostMapEntityRemovals", GAMEMODE );

		// Cleanup
		map.__data.entity_removals.by_model = nil;
		map.__data.entity_removals.by_class = nil;
		map.__data.entity_removals.by_id = nil;
		map.__data.entity_removals.by_creationid = nil;
		map.__data.entity_removals = nil;
		-- map.__data = nil;
	end );
end

	//
	-- local _tab = {
		-- id			= ( _id ) && _id || nil;
		-- class		= ( _class ) && _class || nil;
		-- model		= ( _model ) && _model || nil;
		-- creationid	= ( _creationid ) && _creationid || nil;
	-- };



-- // Cleanup
-- map.__removals.npcs = false;
-- map.__removals.by_class = nil;
-- map.__removals.by_model = nil;
-- map.__removals.by_classmodel = nil;
-- map.__removals.by_id = nil;
-- map.__removals.by_creationid = nil;
-- map.__removals = nil;

-- by_id			= { };
-- by_creationid	= { };
-- by_class		= { };
-- by_model		= { };
-- };

//
-- fileio:Write( "debugging", "map_entity_removals_debugging", toprint( _output ) );

-- local _output = "";
-- _output = _output .. "ID: " .. toprint( _id );
-- _output = _output .. "\tCID: " .. toprint( _creationid );
-- _output = _output .. "\tEnt: " .. toprint( _ent );
-- _output = _output .. "\tModel: " .. toprint( _model );
-- _output = _output .. "\t_bID: " .. toprint( _bID );
-- _output = _output .. "\t_bCID: " .. toprint( _bCID );
-- _output = _output .. "\t_bClass: " .. toprint( _bClass );
-- _output = _output .. "\t_bModel: " .. toprint( _bModel );
-- _output = _output .. "\t_bClassModel: " .. toprint( _bClassModel );
-- _output = _output .. "\n";
//
//
//
-- function map:EntityRemovalByID( _id )
	-- if ( _id ) then
		-- map.__data.entity_removals.by_id[ _id ] = _tab;
	-- end
-- end


//
//
//
-- function map:EntityRemovalByCreationID( _creationid )
	-- if ( _creationid ) then
		-- map.__data.entity_removals.by_creationid[ _creationid ] = true;
	-- end
-- end


//
//
//
-- function map:EntityRemoval( _class, _model )
	-- //
	-- MsgC( COLOR_YELLOW, "Adding map:EntityRemoval( " .. toprint( _class ) .. ", " .. toprint( _model ) .. ", " .. toprint( _id ) .. ", " .. toprint( _creationid ) .. " )\n" );

	-- //
	-- local _tab = {
		-- class		= ( _class ) && _class || nil;
		-- model		= ( _model ) && _model || nil;
	-- };

	-- if ( _class ) then
		-- map.__data.entity_removals.by_class[ _class ] = _tab;
	-- end

	-- if ( _model ) then
		-- map.__data.entity_removals.by_model[ _model ] = _tab;
	-- end
-- end


//
//
//
-- function map:AddEntityRemoval( _tab, ... )
	-- //
	-- if ( #{ ... } > 0 ) then
		-- self:AddEntityRemoval( ... );
	-- end

	-- //

-- end


//
//
//
-- function map:AddEntityRemovalByClass( _class, ... )
	-- //
	-- if ( #{ ... } > 0 ) then
		-- self:AddEntityRemovalByClass( ... );
	-- end

	-- //

-- end


//
//
//
-- function map:AddEntityRemovalByModel( _model, ... )
	-- //
	-- if ( #{ ... } > 0 ) then
		-- self:AddEntityRemovalByModel( ... );
	-- end

	-- //

-- end


-- //
-- //
-- //
-- function map:( )

-- end