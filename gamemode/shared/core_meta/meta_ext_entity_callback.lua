//
// meta_entity_callback_ext - Josh 'Acecool' Moser
// Purpose is to modify the Callback system to make it more developer friendly. Allows the function to be changed __INSTEAD__
// of enabling something that can only be changed by respawning the entity after changing the code... Additionally, each
// callback can be toggled on / off so specific elements can be tried without all of them being necessary ( console spam? )
// and, because I forward the functions through hook.Call, any of the behavior can be modified by hooks ( although you must
// be sure to target the correct entity because each ent will call the same named hook )...
//


//
// Config vars...
//

// If you wish to revert to the traditional system of Entity Callbacks...
local DISABLE_CALLBACK_SYSTEM = false;


//
// If you want to de-activate this to test out your callbacks as they'd work in a traditional server, set the config to true.
// If you only use 1 callback maximum per category per entity, then this can be left on without any issues... Only disable if
// you truly need to add multiple callbacks to the same category ( ie 2 or more to "OnAngleChange", "BuildBonePositions", or 
// "PhysicsCollide"; any one of these categories.
//
if ( DISABLE_CALLBACK_SYSTEM ) then return; end


//
// Out table which has all of the callback names in here...
//
META_ENTITY_CALLBACK_FUNCTIONS = META_ENTITY_CALLBACK_FUNCTIONS || {
	// Args: self, _ang as _T, _data for data.func...
	OnAngleChange = { };

	// Args: self, _bone_count as _T, _data for data.func...
	BuildBonePositions = { };

	// Args: self, _tab as _T, _data for data.func...
	PhysicsCollide = { };
};


// If you're running my dev base, this isn't needed.. This is for those that don't ( I'll be more vigilant in the future regarding things of this natue )!
META_ENTITY = META_ENTITY || FindMetaTable( "Entity" );


//
// Redefine the Callback system to call a Game-Mode hook which makes things easy to enable / disable because these can't be removed...
//
__META_ENTITY_ADDCALLBACK = __META_ENTITY_ADDCALLBACK || META_ENTITY.AddCallback;
function META_ENTITY:AddCallback( _name, _func )
	if ( self:HasCallback( _name ) ) then
		self:ModifyCallback( _name, _func );
		return false;
	end

	// Enable the callback...
	META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ] = {
		// Since we can not "remove" callbacks, I'm limiting them to 1 that can be toggled and altered but not removed..
		enabled			= true;

		// Just in case the name of the callback is needed
		name			= _name;

		// The name of the GM function
		gm_name			= "EntityCallback" .. _name;

		// Entity CallOnRemove Name
		callonremove	= "EntityCallbackRemoval_" .. _name .. "_" .. self:EntIndex( );

		// Our callback function ( Technically all we need, but... )
		func			= _func;
	};

	// To simplify some of the calls, alias of data...
	local _alias = META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ];

	// Define our callback with 2 args, the constant: self / Entity, and the Generic / Unknown...
	local function _callback( self, _T )
		if ( _alias && _alias.enabled ) then
			hook.Call( _alias.gm_name, GAMEMODE, self, _T, _alias );
		end
	end;

	// Remove this callback if the entity is removed...
	self:CallOnRemove( _alias.callonremove, function( )
		self:__RemoveCallback( _name );
	end );

	// Call the original function...
	__META_ENTITY_ADDCALLBACK( self, _name, _callback );
end


//
// Check to see if a callback is active...
//
function META_ENTITY:HasCallback( _name )
	// If we have data at this location then a callback is active...
	if ( META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ] ) then
		return true;
	end

	return false;
end


//
// Check to see if a callback is active...
//
function META_ENTITY:ModifyCallback( _name, _func )
	// Allow modifying a function to update it; useful for developing code for use in callbacks...
	if ( self:HasCallback( _name ) ) then
		if ( isfunction( _func ) ) then
			META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ].func = _func;
		end
	else
		self:AddCallback( _name, _func );
	end
end


//
// Toggle a callback to allow it to "run", or prevent it from "running"...
//
function META_ENTITY:CallbackEnabled( _name )
	if ( self:HasCallback( _name ) ) then
		return META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ].enabled;
	end
end


//
// Toggle a callback to allow it to "run", or prevent it from "running"...
//
function META_ENTITY:ToggleCallback( _name, _bool )
	if ( self:HasCallback( _name ) ) then
		META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ].enabled = ( ( isbool( _bool ) ) && _bool || !self:CallbackEnabled( _name ) );
	end
end


//
// Internal; using this will allow multiple callbacks.. be sure you want this because you won't be able to alter the original one any longer..
//
function META_ENTITY:__RemoveCallback( _name )
	if ( self:HasCallback( _name ) ) then
		META_ENTITY_CALLBACK_FUNCTIONS[ _name ][ self:EntIndex( ) ] = nil;
	end
end


//
// Only allow "Toggling" unless the entity is removed...
//
function META_ENTITY:RemoveCallback( _name )
	if ( self:HasCallback( _name ) ) then
		self:ToggleCallback( _name, false );
	end
end


//
// Remove all callbacks associated with this ent ( well... )...
//
function META_ENTITY:RemoveCallbacks( _name )
	for _name, _tab in pairs( META_ENTITY_CALLBACK_FUNCTIONS ) do
		self:RemoveCallback( _name );
	end
end


//
// Entity Added Callback: BuildBonePositions
//
function GM:EntityCallbackBuildBonePositions( _ent, _bones, _data )
	-- print( "EntityCallbackBuildBonePositions", _ent, _bones );

	if ( _data && _data.func ) then
		_data.func( _ent, _bones );
	end
end


//
// Entity Added Callback: OnAngleChange
//
function GM:EntityCallbackOnAngleChange( _ent, _ang, _data )
	-- print( "EntityCallbackOnAngleChange", type( _ent ), type( _ang ) );

	if ( _data && _data.func ) then
		_data.func( _ent, _ang );
	end
end


//
// Entity Added Callback: PhysicsCollide
//
function GM:EntityCallbackPhysicsCollide( _ent, _tab, _data )
	-- print( "EntityCallbackPhysicsCollide", _ent, _tab );

	if ( _data && _data.func ) then
		_data.func( _ent, _tab );
	end
end


//
// Used for developers to see how it works....
//
function META_ENTITY:GenerateCallbackExample( )
	// If you're the first player in your dev-server, this is you:
	local _p = Entity( 1 );

	// Pick one, pick them all, but one at a time or copy the other data...
	local _name = "OnAngleChange";
	-- local _name = "BuildBonePositions";
	-- local _name = "PhysicsCollide";

	// To ensure the callback doesn't exist yet...
	print( _name, _p:HasCallback( _name ) );

	// To create the callback...
	_p:AddCallback( _name, function( self, _ang )
		if ( math.Round( CurTime( ) ) % 3 == 0 ) then
			print( "11111" );
		end
	end );

	// Forces the callback to skip the _p entity...
	_p:RemoveCallback( _name );

	// Re-enables it... The second argument can be set to force enable / disable, or nil which will then cause it to act as a true toggle...
	_p:ToggleCallback( _name, true );

	// Now, is the callback in existence?
	print( _name, _p:HasCallback( _name ) );

	// Output the table to show the data ( You'll need my dev_addon or dev_base to use print with tables; change to PrintTable otherwise )...
	print( META_ENTITY_CALLBACK_FUNCTIONS );

	// Because the callback exists, this will modify it from printing 1s to 2s...
	timer.Simple( 5, function( )
		_p:AddCallback( _name, function( self, _ang )
			if ( math.Round( CurTime( ) ) % 3 == 0 ) then
				print( "22222" );
			end
		end );
	end );

	// And this will modify it to print 3s
	timer.Simple( 10, function( )
		_p:ModifyCallback( _name, function( self, _ang )
			if ( math.Round( CurTime( ) ) % 3 == 0 ) then
				print( "33333" );
			end
		end );
	end );

	//
	// Hopefully this shows how versatile the Callback system is, albeit limited to 1 function per category per entity,
	// it'll make developing and debugging so much simpler because I've elimited several steps.. Y = eliminated, X = still needed..
	//
	// ( Y ) Remove target entity ( X ) Save File / Auto-Refresh.. ( Y ) Respawn entity.. ( Y ) Trigger code via console command
	// while looking at the entity in order to know which entity will need the code.. ( X ) Debug, make changes, etc...
	//
	// Not bad, 3 steps elimited which are tedious.. Now, simply update the code for the same entity.. Auto-refresh, make
	// changes / debug.. rinse and repeat...
	//
end