//
// Angle Meta-Table Extension - Josh 'Acecool' Moser
//


//
// Turns an Angle into a normalized vector for use in tracing towards that direction via * _dist - Josh 'Acecool' Moser
//
function META_ANGLE:ToVector( )
	local x = math.cos( math.rad( self.y ) );
	local y = math.sin( math.rad( self.y ) );
	local z = -math.sin( math.rad( self.p ) );

	return Vector( x, y, z );
end


//
// START NORMALIZED VECTOR FUNCTIONS
//


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_ANGLE_GET_FORWARD = __META_ANGLE_GET_FORWARD || META_ANGLE.Forward
function META_ANGLE:Forward( _length )
	return __META_ANGLE_GET_FORWARD( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_ANGLE_GET_RIGHT = __META_ANGLE_GET_RIGHT || META_ANGLE.Right
function META_ANGLE:Right( _length )
	return __META_ANGLE_GET_RIGHT( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_ANGLE_GET_UP = __META_ANGLE_GET_UP || META_ANGLE.Up
function META_ANGLE:Up( _length )
	return __META_ANGLE_GET_UP( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_ANGLE:Backward( _length )
	return self:Forward( _length ) * -1; //( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_ANGLE:Left( _length )
	return self:Right( _length ) * -1; //( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_ANGLE:Down( _length )
	return self:Up( _length ) * -1; //( _length || 1 );
end


//
// END NORMALIZED VECTOR FUNCTIONS
//


//
// Any code below has already been implemented into GMod or is obsolete but remains until I 
// move it to it's own file in the _development / snippets NOLOAD folder
//
do return; end


//
// Snaps Angle to nearest... Josh 'Acecool' Moser
// Implemented into GMod; no longer needed but will be kept to show how easy it is to add things
//
function META_ANGLE:SnapTo( _component, _snap_degrees )
	if ( _snap_degrees == 0 ) then ErrorNoHalt( "The snap degrees must be non-zero.\n" ); return self; end
	if ( !self[ _component ] ) then ErrorNoHalt( "You must choose a valid component of Angle( p || pitch, y || yaw, r || roll ) to snap such as Angle( 80, 40, 30 ):SnapTo( \"p\", 90 ):SnapTo( \"y\", 45 ):SnapTo( \"r\", 40 ); and yes, you can keep adding snaps.\n" ); return self; end

	self[ _component ] = math.Round( self[ _component ] / _snap_degrees ) * _snap_degrees;
	self[ _component ] = math.NormalizeAngle( self[ _component ] );

	return self;
end

-- print( Angle( 0, 0, 0 ):ToVector( ) )

-- print( Angle( 45, 45, 45 ):SnapTo( "r", 0 ) ); //:SnapTo( "y", 8 ):SnapTo( "r", 8 ) )
-- local _err = debug.getinfo( self.SnapTo ); //	.. "Function Line: " .. _err.linedefined .. " - " .. _err.short_src .. "\n"
-- local _err = debug.traceback( ) .. "\n"

-- print( Angle( 12, 98, 167 ):SnapTo( "p", 30 ):SnapTo( "y", 45 ):SnapTo( "r", 45 ) )