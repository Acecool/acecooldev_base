//
// Player Meta-Table Extensions - Josh 'Acecool' Moser
//


//
// INIT / Vars
//


//
// NOTE: These replace existing functions in favor of the flag system. Additionally, these values are
//	PRIVATE meaning cheats will not be able to read these values like the original methods allowed.
//	This also means if there is a HUD / Mod which needs access to these values, change the private
//	value to FALSE instead of TRUE! Additionally, some may update frequently ( like stamina ) so
//	some vars are meant to be private ( known between server and client )...
//
local IsHealthPrivate	= false;
local IsArmorPrivate	= false;
local IsStaminaPrivate	= true;


//
// FUNCTIONS
//


//
// Health Meta-Functions - Josh 'Acecool' Moser
//
// Internal Function: _p:CanSetHealth( x ); -- Determines whether or not x is valid data; if not then flag won't update!
//
// Usable Functions:
//	_p:Health( );		_p:SetHealth( x );		_p:AddHealth( x );	_p:HasHealth( [ x ] );
//	_p:MinHealth( );	_p:SetMinHealth( x );	_p:MaxHealth( );	_p:SetMaxHealth( x );
//
local _tab = data:BuildAccessors( "Health", 0, IsHealthPrivate, FLAG_CHARACTER, META_PLAYER, TYPE_NUMBER, nil, {
	adder		= true;	-- Adds Add*, and allows other functions to be added
	get_prefix	= "";	-- Max* instead of GetMax, etc...
	adder_clamp	= true;	-- Clamps values between the Min* / Max* functions
	min			= 0;	-- Default Minimum, Adds SetMin* / Min* functions
	max			= 100;	-- Default Maximum, Adds SetMax*, Max* functions
} );

// Aliases...
META_PLAYER.GetHealth		= _tab.Get;
META_PLAYER.GetMinHealth	= _tab.GetMin;
META_PLAYER.GetMaxHealth	= _tab.GetMax;


//
// Armor Meta-Functions - Josh 'Acecool' Moser
//
// Internal Function: _p:CanSetArmor( x ); -- Determines whether or not x is valid data; if not then flag won't update!
//
// Usable Functions:
//
//	_p:Armor( );	_p:SetArmor( x );		_p:AddArmor( x );	_p:HasArmor( [ x ] );
//	_p:MinArmor( );	_p:SetMinArmor( x );	_p:MaxArmor( );		_p:SetMaxArmor( x );
//
local _tab = data:BuildAccessors( "Armor", 0, IsArmorPrivate, FLAG_CHARACTER, META_PLAYER, TYPE_NUMBER, nil, {
	adder		= true;	-- Adds Add*, and allows other functions to be added
	get_prefix	= "";	-- Max* instead of GetMax, etc...
	adder_clamp	= true;	-- Clamps values between the Min* / Max* functions
	min			= 0;	-- Default Minimum, Adds SetMin* / Min* functions
	max			= 100;	-- Default Maximum, Adds SetMax*, Max* functions
} );

// Aliases...
META_PLAYER.GetArmor		= _tab.Get;
META_PLAYER.GetMinArmor		= _tab.GetMin;
META_PLAYER.GetMaxArmor		= _tab.GetMax;


//
// Stamina Meta-Functions
//
// Internal Function: _p:CanSetStamina( x ); -- Determines whether or not x is valid data; if not then flag won't update!
//
// Usable Functions:
//
//	_p:Stamina( );		_p:SetStamina( x );		_p:AddStamina( x );		_p:HasStamina( [ x ] );
//	_p:MinStamina( );	_p:SetMinStamina( x );	_p:MaxStamina( );		_p:SetMaxStamina( x );
//
local _tab = data:BuildAccessors( "Stamina", 100, IsStaminaPrivate, FLAG_CHARACTER, META_PLAYER, TYPE_NUMBER, nil, {
	adder		= true;	-- Adds Add*, and allows other functions to be added
	get_prefix	= "";	-- Max* instead of GetMax, etc...
	adder_clamp	= true;	-- Clamps values between the Min* / Max* functions
	min			= 0;	-- Default Minimum, Adds SetMin* / Min* functions
	max			= 100;	-- Default Maximum, Adds SetMax*, Max* functions
} );

// Aliases...
META_PLAYER.GetStamina		= _tab.Get;
META_PLAYER.GetMinStamina	= _tab.GetMin;
META_PLAYER.GetMaxStamina	= _tab.GetMax;