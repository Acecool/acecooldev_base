//
//  - Josh 'Acecool' Moser
//

// I'll need an alternate method for some of these as GetDriver is SERVERside.
do return; end

//
//
//
function META_VEHICLE:SetVehicleClass( _value )
	self:SetFlag( "classname", _value, false, FLAG_VEHICLE );
end
 

//
//
//
function META_VEHICLE:GetVehicleClass( )
	return self:GetFlag( "classname", "prop_vehicle_jeep", false, FLAG_VEHICLE );
end 


//
//
//
function META_VEHICLE:SetThirdPersonMode( _value )
	local _p = self:GetDriver( );
	if ( IsValidPlayer( _p ) ) then
		_p:SetFlag( "thirdperson", _value, true, FLAG_VEHICLE );
	end
end 


//
//
//
function META_VEHICLE:GetThirdPersonMode( )
	local _p = self:GetDriver( );
	if ( IsValidPlayer( _p ) ) then
		return _p:GetFlag( "thirdperson", false, true, FLAG_VEHICLE );
	end
end 


//
//
//
function META_VEHICLE:SetCameraDistance( _value )
	local _p = self:GetDriver( );
	if ( IsValidPlayer( _p ) ) then
		_p:SetFlag( "camera_distance", _value, true, FLAG_VEHICLE );
	end
end 


//
//
//
function META_VEHICLE:GetCameraDistance( )
	local _p = self:GetDriver( );
	if ( IsValidPlayer( _p ) ) then
		return _p:SetFlag( "camera_distance", 3, true, FLAG_VEHICLE );
	end
end
