//
// Vector Meta-Table Extension - Josh 'Acecool' Moser
//


//
// Converts Vector To Color - Josh 'Acecool' Moser
//
function META_VECTOR:ToColor( )
	return Color( math.Round( self.x * 255, 0 ), math.Round( self.y * 255, 0 ), math.Round( self.z * 255, 0 ), 255 );
end


//
// START NORMALIZED VECTOR FUNCTIONS
//


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_VECTOR_GET_FORWARD = __META_VECTOR_GET_FORWARD || META_VECTOR.GetForward
function META_VECTOR:GetForward( _length )
	return __META_VECTOR_GET_FORWARD( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_VECTOR_GET_RIGHT = __META_VECTOR_GET_RIGHT || META_VECTOR.GetRight
function META_VECTOR:GetRight( _length )
	return __META_VECTOR_GET_RIGHT( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * 1 calculation and allows for input
//
__META_VECTOR_GET_UP = __META_VECTOR_GET_UP || META_VECTOR.GetUp
function META_VECTOR:GetUp( _length )
	return __META_VECTOR_GET_UP( self ) * ( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_VECTOR:GetBackward( _length )
	return self:GetForward( _length ) * -1; //( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_VECTOR:GetLeft( _length )
	return self:GetRight( _length ) * -1; //( _length || 1 );
end


//
// Helper Function to avoid needing to do the * -1 calculation and allows for input
//
function META_VECTOR:GetDown( _length )
	return self:GetUp( _length ) * -1; //( _length || 1 );
end


//
// END NORMALIZED VECTOR FUNCTIONS
//


//
// Any code below has already been implemented into GMod or is obsolete but remains until I
// move it to it's own file in the _development / snippets NOLOAD folder
//
do return; end