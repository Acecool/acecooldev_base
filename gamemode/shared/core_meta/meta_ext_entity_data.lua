//
//  - Josh 'Acecool' Moser
// 


//
//
//
function META_ENTITY:GetVar( _flag, _default )
	return self:GetFlag( _flag, _default, false, FLAG_DATA );
end


//
//
//
function META_ENTITY:SetVar( _flag, _value )
	self:SetFlag( _flag, _value, false, FLAG_DATA );
end


//
//
//
function META_ENTITY:SetCreator( _p )
	self:SetFlag( "creator", _p, false, FLAG_DATA );
end


//
//
//
function META_ENTITY:GetCreator( )
	return self:GetFlag( "creator", game.GetWorld( ), false, FLAG_DATA );
end