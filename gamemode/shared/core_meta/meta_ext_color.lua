//
// Color Meta-Table Extension - Josh 'Acecool' Moser
// Thanks to http://www.rapidtables.com/convert/color/rgb-to-cmyk.htm for formulas
//


//
// Converts CMY to Color
//
function META_COLOR:FromCMYK( _c, _m, _y, _k )
	if ( !IsColor( self ) ) then return self; end

	// Ensure CMYK are number, and values are more than or equal to 0, less than or equal to 1
	local _c = ( isnumber( _c ) ) && math.Clamp( _c, 0, 1 ) || 0
	local _m = ( isnumber( _m ) ) && math.Clamp( _m, 0, 1 ) || 0
	local _y = ( isnumber( _y ) ) && math.Clamp( _y, 0, 1 ) || 0
	local _k = ( isnumber( _k ) ) && math.Clamp( _k, 0, 1 ) || 0

	// Calculate RGB from CMYK
	self.r = 255 * ( 1 - _c ) * ( 1 - _k );
	self.g = 255 * ( 1 - _m ) * ( 1 - _k );
	self.b = 255 * ( 1 - _y ) * ( 1 - _k );

	// Use what the user entered as the new a...
	self.a = ( self.a ) && self.a || 255;

	return self;
end


//
// Converts Color to CMY
//
function META_COLOR:ToCMYK( )
	if ( !IsColor( self ) ) then return self; end

	// Converts RGB to 0-1 range stemming from 0-255; uses 0 if
	local _r = ( self.r > 0 ) && ( math.min( self.r, 255 ) / 255 ) || 0
	local _g = ( self.g > 0 ) && ( math.min( self.g, 255 ) / 255 ) || 0
	local _b = ( self.b > 0 ) && ( math.min( self.b, 255 ) / 255 ) || 0

	// Calculate CMYK from RGB ( and K )
	local _k = 1 - math.max( _r, _g, _b );
	local _c = ( 1 - _r - _k ) / ( 1 - _k );
	local _m = ( 1 - _g - _k ) / ( 1 - _k );
	local _y = ( 1 - _b - _k ) / ( 1 - _k );

	// Prevents NaN
	_c = ( _c >= 0 ) && _c || 0;
	_m = ( _m >= 0 ) && _m || 0;
	_y = ( _y >= 0 ) && _y || 0;

	return _c, _m, _y, _k;
end


//
// Converts HEX to Color using HEX value
//
function META_COLOR:FromHex( _hex )
	// Remove # from the string if added
	local _hex = string.gsub( _hex, "#", "" );

	// Len for errorchecking / shorthand checking
	local _hexlen = string.len( _hex )

	// Is shorthand used, if it is set the valueAdjuster to -1
	local _bShorthand = ( _hexlen == 3 )
	local _bStandard = ( _hexlen == 6 )
	local _x = ( _bShorthand ) && -1 || 0

	// Error checking; if we're not using shorthand, and not longhand, we won't know what to do...
	if ( !_bShorthand && !_bStandard ) then
		// Using ErrorNoHalt so "Lua" error isn't generated from missing color because we're throwing "nice" error.
		ErrorNoHalt( "You must enter valid hex-code to convert hex-to-color.\nIf you're using shorthand, valid input is #FFF or FFF, alternatively, if using standard, valid input is #FFFFFF or FFFFFF.\nNOTE: The color is unchanged from default!" )

		// Color will just be black/original.
		return self;
	end

	// Convert RGB... If shorthand is used, ensure string.sub uses 11,22,33 in place of 12,34,56 using value adjuster x.
	// Additionally, if shorthand is used, repeat the output character 2 times ( ABC becomes AABBCC ), otherwise output 1 character.
	local _r = tonumber( string.rep( string.sub( _hex, 1, 2 + _x ), ( _bShorthand && 2 || 1 ) ), 16 )
	local _g = tonumber( string.rep( string.sub( _hex, 3 + _x, 4 + _x * 2 ), ( _bShorthand && 2 || 1 ) ), 16 )
	local _b = tonumber( string.rep( string.sub( _hex, 5 + _x * 2, 6 + _x * 2 ), ( _bShorthand && 2 || 1 ) ), 16 )

	// Update THIS color object; We do this here and not ToHex because ToHex just outputs the value...
	// This we want a color object from hex...
	self.r = _r;
	self.g = _g;
	self.b = _b;
	self.a = self.a || 255;

	return self;
end


//
// Converts Color to HEX
//
function META_COLOR:ToHex( _alpha, _first, _trailing_a )
	if ( !IsColor( self ) ) then return self; end

	local _hex = string.upper( string.format( "%02x%02x%02x", self.r, self.g, self.b ) );

	// Alpha is ticked on to the front with a in rear...
	if ( _alpha ) then
		// Some machines alpha comes FIRST, others it comes second; some require an a at the end too...
		local _a = string.upper( string.format( "%02x", self.a ) );

		if ( _first ) then
			_hex = _a .. _hex .. ( _trailing_a && "a" || "" );
		else
			_hex = _hex .. _a .. ( _trailing_a && "a" || "" );
		end
	end

	return _hex;
end


//
// Converts CMYK Color code to Color Object - loss of precision / alpha lost
//
function CMYKToColor( c, m, y, k )
	return Color( 0, 0, 0, 255 ):FromCMYK(	c, m, y, k );
end


//
// Converts Hex Color code to Color - loss of precision / alpha lost
//
function HexToColor( hex )
	return Color( 0, 0, 0, 255 ):FromHex( hex );
end