//
// String Extension - Josh 'Acecool' Moser
// http://www.lua.org/pil/20.2.html
//


//
// Concatenates two strings without .. ( no rawconcat for meta-tables )
//
function string.concat( self, _text )
	return string.format( "%s%s", self:__tostring( ), _text );
end


//
// Formats a digit with leading 0s
//
function string.formatdigit( _number, _leading_zeros )
	return string.format( "%0" .. ( _leading_zeros || 2 ) .. "d", _number );
end


//
// Alternative to string.Comma
//
function string.formatmoney( _amount )
	return string.Comma( _amount );
end


//
// Returns only the numbers in a string by stripping non-numbers
//
function string.numbers( _text )
	return tonumber( string.gsub( ( _text || "" ), "%D", "" ), 10 ) || -1;
end


//
// All non alpha-numeric characters become _
//
function string.safe( text )
	text = string.gsub( text, "%W", "_" );
	return text;
end