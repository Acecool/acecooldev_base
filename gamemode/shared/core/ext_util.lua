//
// util Extension - Josh 'Acecool' Moser
//


//
// Benchmark - Josh 'Acecool' Moser
//
function util.Benchmark( _times, func, ... )
	local function benchmark_process( func, ... )
		local x = SysTime( ); //os.clock( );
		local ret = func( ... );
		local _time = SysTime( ) - x; //( os.clock( ) - x );
		-- MsgC( COLOR_CYAN, "Benchmark has finished with a total elapsed time of: " .. _time .. "\n" );
		return _time;
	end

	local _processtime = SysTime( ); //os.clock( );
	local _time = 0;
	for i = 1, _times do
		_time = _time + benchmark_process( func, ... );
	end
	_processtime = SysTime( ) - _processtime; //( os.clock( ) - _processtime );

	MsgC( Color( 0, 255, 255, 255 ), "Total Process Time is how long the benchmark took to complete.\nTotal Operation Time is the SUM of how long the individual function calls took.\nAverage Operation Time is the AVERAGE time to execute the function.\n\n" );
	MsgC( Color( 0, 255, 0, 255 ), "Total Benchmark Process Time: \t" .. _processtime .. " seconds to run " .. _times .. " times\n" );
	MsgC( Color( 0, 255, 0, 255 ), "Total Operation Time: \t" .. _time .. "\tseconds\n" );
	MsgC( Color( 0, 255, 0, 255 ), "Avg. Operation Time: \t" .. _time / _times .. "\tseconds\n" );
end


//
// Converts a comma delimited text to a table - Josh 'Acecool' Moser
//
function util.StringToTable( _text, _commaTrailing )
	local _explode = string.Explode( ",", _text );

	if ( _commaTrailing ) then
		table.remove( _explode, table.GetLastKey( _explode ) );
	end

	return _explode;
end


//
// Translate input SteamID, SteamID64 or SteamU into SteamID, SteamID64 AND SteamU. Return in same order 32, 64, U - Josh 'Acecool' Moser
//
function util.TranslateSteamID( _steam )
	local _steamid = _steam;
	local _steamid64 = -1;
	local _steamuid = "";

	// If UID was input, ensure it is moved to correct var and calculate Steam32.
	if ( string.sub( string.upper( _steam ), 2, 2 ) == "U" ) then
		_steamuid = _steam;

		// Remove all non-digits so we can tonumber the result below, and divide by 2
		_steam = string.gsub( _steam, "%D", "" );

		// Remove first number from 1x after strip, before: [U:1:x] and convert to number.
		_steam = tonumber( string.sub( _steam, 2 ) );

		// Even or odd?
		local _bEven = ( _steam % 2 == 0 );
		_steam = math.floor( _steam / 2 );

		// Set _steamid since we calculated it, and because we have the steamU we only need 64. Do it, and return.
		_steamid = "STEAM_0:" .. ( _bEven && "0" || "1" ) .. ":" .. _steam;
		_steamid64 = util.SteamIDTo64( _steamid );
	else
		// Check for BOT SteamID of bots...
		if ( _steam == "BOT" || _steam == 0 ) then
			return "BOT", 0, "BOT";
		end

		// Check to see if the length changes if we remove non-digits.
		// Easy way to tell Steam64 from 32 ( could also get first char, and other methods )
		if ( string.len( _steam ) == string.len( string.gsub( _steam, "%D", "" ) ) ) then
			_steamid64 = _steam;
			_steamid = util.SteamIDFrom64( _steamid64 );
		else
			_steamid64 = util.SteamIDTo64( _steamid );
		end

		// Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
		if ( _steamuid == "" ) then
			local _uidX = tonumber( string.sub( _steamid, 9, 9 ) );
			local _uidY = tonumber( string.sub( _steamid, 11 ) );
			if ( _uidY && _uidX ) then
				_uidY = ( _uidY * 2 ) + _uidX;
				_steamuid = "[U:1:" .. _uidY .. "]";
			end
		end
	end

	// We're done..
	return _steamid, _steamid64, _steamuid;
end