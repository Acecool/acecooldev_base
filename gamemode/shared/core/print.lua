//
// Extended Print Josh 'Acecool' Moser
//

// BUFFER: If this is too high, there will be missing chunks of data.
local PRINT_BUFFER			= 250;

// Arg separator: Default print function inserts a tab \t for each new argument. Here this can be redefined. Suggestions: tab \t, new line \n, space	, etc..
local PRINT_ARG_SEPARATOR		= "\t";

// Call empty print at end of function for a new-line ( only called if true, and PRINT_ARG_SEPARATOR is not \n )
local PRINT_NEWLINE			= true;


//
// print( ) default colors are COLOR_CYAN on SERVER, and COLOR_YELLOW/ORANGE on CLIENT
//
PRINT_COLORS = {
	Angle 				= COLOR_WHITE; --
	boolean 			= COLOR_WHITE; --
	Color 				= nil; -- This actually uses the COLOR of the element, instead of a set color.
	DamageInfo 			= COLOR_WHITE; --
	CTakeDamageInfo 	= COLOR_WHITE; --
	EffectData 			= COLOR_WHITE;
	CEffectData 		= COLOR_WHITE;	--
	Entity 				= COLOR_WHITE; --
	["function"] 		= COLOR_WHITE; --
	LuaEmitter 			= COLOR_WHITE; --
	LuaParticle 		= COLOR_WHITE; --
	Material 			= COLOR_WHITE; --
	MoveData 			= COLOR_WHITE; --
	number 				= COLOR_WHITE; --
	Panel 				= COLOR_WHITE; --
	Player 				= COLOR_WHITE; --
	RecipientFilter 	= COLOR_WHITE; --
	CRecipientFilter 	= COLOR_WHITE; --
	SoundPatch 			= COLOR_WHITE; --
	String 				= COLOR_WHITE; --
	StringLanguage 		= COLOR_CYAN;
	StringLanguageF 	= COLOR_RED;
	Table 				= COLOR_WHITE; --
	ToString 			= COLOR_GRAY; --
	Texture 			= COLOR_WHITE; --
	UserCmd 			= COLOR_WHITE; --
	Vector 				= COLOR_WHITE; --
	VMatrix 			= COLOR_WHITE; --
};


//
// Print Redirect
//
if ( !__PRINT ) then __PRINT = print; end


//
// Extended Print ( uses MsgC )
//
function print( ... )
	local _args = { ... };

	//
	// Each arg...
	//
	for k, v in pairs( _args ) do
		// Convert data to friendly output...
		local _p, _c = toprint( v );

		// Color system, needs to be an arg for this to work.
		local _color = _c || COLOR_RED; //COLOR_SNOW / COLOR_SILVER

		// Buffer / Chunking system.
		local _len = string.len( _p );
		local _count = math.ceil( _len / PRINT_BUFFER );
		for i = 1, _count do
			local _sub = string.sub( _p, 1 + ( PRINT_BUFFER * i ) - PRINT_BUFFER, PRINT_BUFFER * i );

			// This is where we pass printing off to, so we can do it in color!
			MsgC( _color, _sub );
		end

		// End of the arg, add the separator.
		MsgC( COLOR_WHITE, PRINT_ARG_SEPARATOR );
	end

	// If the separator is not a new line, we need a new line to end it all or the next print or MsgC will be on the same line!
	if ( PRINT_NEWLINE && PRINT_ARG_SEPARATOR != "\n" ) then
		__PRINT( );
	end
end