//
// Team Extension - Josh 'Acecool' Moser
//


//
// Set / Change color of a team
//
function team.SetColor( _id, _color )
	local _teams = team.GetAllTeams( );

	if ( _teams[ _id ] ) then
		_teams[ _id ].Color = _color;
		return true;
	end

	return false;
end