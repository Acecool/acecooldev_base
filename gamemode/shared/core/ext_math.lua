//
//	- Josh 'Acecool' Moser
//


//
// Calculates the 2 unknown angles of a right-triangle so the sum adds up to -180 or 180
//
function math.OmegaAngleDifference( _pos, _pos2 )
	local _rad = math.atan( ( _pos.y - _pos2.y ) / ( _pos2.x - _pos.x ) );
	local _deg = math.deg( _rad );

	local _deg2 = 180 - ( 90 + math.abs( _deg ) );
	_deg2 = ( _deg < 0 ) && _deg2 * -1 || _deg2;

	return _deg, _deg2, _rad, math.rad( _deg2 );
end


//
// Handles repetitive smooth rotational calculations into 1 line without the confusion and normalizes the angle ( -180 )
//
function math.rotate( _speed, _offset )
	return ( ( RealTime( ) * ( _speed || 180 ) + ( _offset || 0 ) ) % 360 ) - 180;
end


//
// Handles repetitive sin wave code into 1 line
//
function math.sinwave( _speed, _size, _abs )
	local _sin = math.sin( RealTime( ) * ( _speed || 1 ) ) * ( _size || 1 );
	if ( _abs ) then _sin = math.abs( _sin ); end
	return _sin;
end


//
// Converts a size to readable format. Josh 'Acecool' Moser
// Input: math.ConvertSize( 1024, SIZE_KB, true ) = 1KiloBytes
// Input: math.ConvertSize( 1024, SIZE_KB ) = 1KB
//
function math.ConvertSize( _val, _unit, _extended )
	// Value or 0 / Unit or bytes, text.
	return ( ( _val ) && _val || 0 ) / ( ( _unit ) && _unit || SIZE_B ), ( _extended ) && SIZES[ _unit + 1 ] || SIZES[ _unit ];
end


//
// Calculates points on a circles edge; the more points-count the smoother the circle
//	using 3 points = triangle, 4 = diamond / square, 5 = pentagon, 6 = hexagon, etc...
//
//	This may be deprecated later on when I release poly and shapes classes ( will have caching, etc... built in )
//
//									 Center,	2r = w, smoothness, rotate points	, spread distance between first and last
function math.CalculateCirclePoints( _origin, _radius, _pointsCount, _rotationOffset, _splitSpread )
	//
	_pointsCount = _pointsCount || 4;

	// SplitSpread is the points of distance between the first, and the last point ( Useful for speedometers )
	_splitSpread = _splitSpread || 0;

	// RotationOffset is how far around the first point should swing ( Useful for speedometers as by default, the first unit is at 3 o'clock )
	_rotationOffset = _rotationOffset || 0;

	// pi = 1 half, we need to go full circle, then compensate for the number of points we are doing.
	local _slices = ( 2 * math.pi ) / _pointsCount;
	local _points = { };
	for i = 0, ( _pointsCount + _splitSpread ) - 1 do
		if ( _splitSpread > 0 && i > _pointsCount -1 -_splitSpread ) then continue; end

		local _angle = ( _slices * ( i + _rotationOffset ) ) % _pointsCount;
		local _x = _origin.x + _radius * math.cos( _angle );
		local _y = _origin.y + _radius * math.sin( _angle );
		_points[ i ] = Vector( _x, _y, 0 );
	end

	return _points;
end