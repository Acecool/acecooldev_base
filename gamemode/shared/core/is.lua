//
// Is*/is* - all Is functions "standardized" / defined in Lua - Josh 'Acecool' Moser
//


//
// Type components
//
local TYPES = {
	//
	// Standard Types
	//
	Angle 			= "Angle";
	Boolean 		= "boolean";
	DamageInfo 		= "CTakeDamageInfo";
	EffectData 		= "CEffectData";
	Entity 			= "Entity";
	Function 		= "function";
	LuaEmitter 		= "CLuaEmitter";
	LuaParticle 	= "CLuaParticle";
	Material 		= "IMaterial";
	MoveData 		= "CMoveData";
	Nil 			= "nil";
	Null 			= "";
	Number 			= "number";
	-- Panel 			= "";
	Player 			= "Player";
	RecipientFilter = "CRecipientFilter";
	SoundPatch 		= "CSoundPatch";
	String 			= "string";
	Table 			= "table";
	Texture 		= "ITexture";
	UserCmd 		= "CUserCmd";
	Vector 			= "Vector";
	VMatrix 		= "VMatrix";

	//
	// Extended Types
	//
	Color 			= { "table", "Color" };
	DeckOfCards 	= { "table", "DeckOfCards" };
	MarkupObject 	= { "table", "MarkupObject" };
	PlayingCard 	= { "table", "PlayingCard" };
};


function isvalid( ... )
	return IsValid( ... );
end

-- [IsColor] 						= function: 0x10224690
-- [IsEnemyEntityName] 			= function: 0x102254b0
-- [IsEntity] 						= function: 0x101b4e48
-- [IsFirstTimePredicted] 			= function: 0x101b4af0
-- [IsFriendEntityName] 			= function: 0x102256a0
-- [IsMounted] 					= function: 0x102256e0
-- [IsTableOfEntitiesValid]		= function: 0x101d7270
-- [IsValid] 						= function: 0x10225128

-- [isangle] 						= function: 0x101b4818
-- [isbool] 						= function: 0x101b4858
-- [isentity] 						= function: 0x101b48d8
-- [isfunction]					= function: 0x101b4898
-- [isnumber] 						= function: 0x101b4798
-- [ispanel] 						= function: 0x101b4918
-- [isstring] 						= function: 0x101b4758
-- [istable] 						= function: 0x101b4718
-- [isvector] 						= function: 0x101b47d8


-- util
	-- [IsModelLoaded] 			= function: 0x101ba2a0
	-- [IsSkyboxVisibleFromPoint] 	= function: 0x101ba148
	-- [IsValidModel] 				= function: 0x101b8d50
	-- [IsValidPhysicsObject] 		= function: 0x10235978
	-- [IsValidProp] 				= function: 0x101b8d98
	-- [IsValidRagdoll] 			= function: 0x101b8dd8