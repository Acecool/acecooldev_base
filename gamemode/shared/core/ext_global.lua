//
// Global Extension - Josh 'Acecool' Moser
//


//
// Simple recursive callback to handle scaling IsValid functions to reduce code needed
//
local function IsValidCallback( _data, _func_is, _func_are, ... )
	AssertArgs( { TYPE_FUNCTION, TYPE_FUNCTION }, _func_is, _func_are );
	if ( #{ ... } > 0 ) then
		return _func_is( _data ) && _func_are( ... );
	end

	return _func_is( _data );
end


//
// Helper - it's quite common to check multiple objects at once
//
function AreValid( _object, ... )
	return IsValidCallback( _p, IsValid, AreValid, ... );
end


//
// Helper - It's quite common to run these together
//
function IsPlayerValid( _p )
	return ( IsValid( _p ) && _p.IsPlayer && _p:IsPlayer( ) );
end


//
// Helper - it's quite common to check multiple players at once
//
function ArePlayersValid( _p, ... )
	return IsValidCallback( _p, IsPlayerValid, ArePlayersValid, ... );
end


//
// Helper - It's quite common to run these together
//
function IsWeaponValid( _w )
	return IsValid( _w ) && _w.IsWeapon && _w:IsWeapon( );
end


//
// Helper - It's quite common to verify multiple weapons at once
//
function AreWeaponsValid( _w, ... )
	return IsValidCallback( _w, IsWeaponsValid, AreWeaponsValid, ... );
end


//
// Helper - It's quite common to run these together
//
function IsSWEPValid( _w )
	return IsValidWeapon( _w ) && _w.Clip1;
end


//
// Helper - It's quite common to verify multiple weapons at once
//
function AreSWEPsValid( _w, ... )
	return IsValidCallback( _w, IsSWEPValid, AreSWEPsValid, ... );
end


//
// Helper - It's quite common to run these together
//
function IsNPCValid( _npc )
	return IsValid( _npc ) && _npc.IsNPC && _npc:IsNPC( );
end


//
// Helper - It's quite common to verify multiple npcs at once
//
function AreNPCsValid( _npc, ... )
	return IsValidCallback( _npc, IsNPCValid, AreNPCValid, ... );
end


//
// Helper Aliases - Is*Valid reads better than IsValid* but both allowed...
//
IsValidPlayer	= IsPlayerValid;
IsValidWeapon	= IsWeaponValid;
IsValidSWEP		= IsSWEPValid;
IsValidNPC		= IsNPCValid;

AreValidPlayers	= ArePlayersValid;
AreValidWeapons	= AreWeaponsValid;
AreValidSWEPs	= AreSWEPsValid;
AreValidNPCs	= AreNPCsValid;


//
// Used like assert( bool, text ) except args are ( map, ... as args )
// Example: AssertArgs( { TYPE_STRING, TYPE_BOOL }, _text, _private );
//
function AssertArgs( _map, ... )
	local _example_unknown = language.GetPhrase( "datatype_example_unknown" );
	local _args = { ... };

	if ( !istable( _map ) ) then
		_map = { _map };
	end

	if ( #_args > 0 && #_args == #_map ) then
		for k, v in pairs( _args ) do
			if ( _map[ k ] && TypeID( v ) != _map[ k ] ) then
				-- local _error = language.GetPhrase( "assert_error", v, TypeName( TypeID( v ) ), TypeID( v ), TypeName( _map[ k ] ), _map[ k ] );
				local _example_entered = language.GetPhrase( "datatype_example_" .. TypeName( TypeID( v ) ) ) || _example_unknown;
				local _example_expected = language.GetPhrase( "datatype_example_" .. TypeName( TypeID( k ) ) ) || _example_unknown;
				local _error = language.GetPhrase( "assert_error", TypeName( TypeID( v ) ), _example_entered, v, TypeName( _map[ k ] ), _example_expected );

				// Instead of reporting the error occuring in this page, set the reporter to what file / line called this function...
				error( _error, 2 );
			end
		end
	else
		error( "Can't assert non-like data... Map: " .. tostring( #_map ) .. ", Args: " .. tostring( #_args ) .. "! HINT: Make sure the argument value is not nil..." );
	end
end


//
//
//
function TypeName( _id )
	return TYPEID_TOSTRING[ _id ] || TYPEID_TOSTRING.default;
end


//
//
//
function GenerateMessageID( _sender )
	local _time = os.time( );
	_time = math.random( _time );

	local _clock = os.clock( );
	_clock = math.random( _clock );

	local _sender = _sender or "SERVER";
	if ( math.random( 1, 100 ) % 2 == 0 ) then
		_sender = string.upper( _sender )
	else
		_sender = string.lower( _sender )
	end

	local _messageID = "+T:" .. _time .. "; C:" .. _clock .. "; S:" .. _sender .. ";-";
	-- _messageID = util.Base64Encode( _messageID );

	return _messageID;
end