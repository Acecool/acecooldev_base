//
// hook Extension - Josh 'Acecool' Moser
//


//
// Useful tool for debugging bits of code, or to have something display / run for a limited time
//
// hook.AddTemp( "HUDPaint", 3, function( ) end, true );
//
// It will create up to 1 hook per hook-name with a list of events to process so if 2 hook.AddTemps were created for the
// same hook, it will only create 1 hook and there will be a table with 2 entries. The added hook processes the entries
// in this list and calls the function/callback with the hook arguments via ...; maximum of 1 hook per hook-name is created
// and unlimited number of events can be added to process. When the element expires it removes the element from the list. 
// Upon last element being removed from the list, the hook is removed along with the table for that particular hook is nulled.
//
function hook.AddTemp( _hook, _time, _callback, _debug )
	if ( !hook.__TempHooks ) then hook.__TempHooks = { }; end

	// Ensure a hook is selected...
	if ( !_hook || _hook == "" ) then error( "You must supply a valid hook name to create a temporary hook!" ); end

	//
	local _id = "hook.AddTemp:" .. _hook;

	// If we haven't initialized it yet, do so
	if ( !hook.__TempHooks[ _hook ] ) then
		if ( _debug ) then
			MsgC( COLOR_GREEN, "Activating draw.This hook: " .. _hook .. "\n" );
		end

		// Initialize it
		hook.__TempHooks[ _hook ] = { };

		// Insert our render job so it can be processed
		table.insert( hook.__TempHooks[ _hook ], { expiry = CurTime( ) + _time, callback = _callback } );

		// Add the hook
		hook.Add( _hook, _id, function( ... )
			// Process content
			for k, v in pairs( hook.__TempHooks[ _hook ] ) do
				// If the job has expired
				if ( CurTime( ) > v.expiry ) then
					// Remove it
					table.remove( hook.__TempHooks[ _hook ], k );

					// If this was the last job
					if ( #hook.__TempHooks[ _hook ] < 1 ) then
						// Remove the hook, and reset the var
						if ( _debug ) then
							MsgC( COLOR_RED, "Deactivating hook.AddTemp hook: " .. _hook .. "\n" );
						end

						hook.Remove( _hook, _id );
						hook.__TempHooks[ _hook ] = nil;
					end
				else
					// If the job hasn't expired, process it
					v.callback( ... );
				end
			end
		end );
	else
		// The hook exists, we'll just add a new job...
		table.insert( hook.__TempHooks[ _hook ], { expiry = CurTime( ) + _time, callback = _callback } );
	end

	return hook.GetTable( )[ _hook ];
end