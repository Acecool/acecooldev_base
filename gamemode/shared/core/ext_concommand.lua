//
//  - Josh 'Acecool' Moser
//


//
// This will be changed into a full fledged autofill system; this particular partial will be in charge of 
// replacing <entity> or whatever in the template text.
//
function concommand.GetAutofillEntityList( _cmd, _input )
	// Helper function to avoid needing to repeat this code over and over... Pass by reference, we're modifying _list directly.
	local function ProcessEntityList( _cmd, _input, _data, _list )
		for k, v in pairs( _data ) do
			local _bContains = !_input || _input == "" || string.StartWith( string.lower( k ), _input );
			local _blacklisted = ( BLACKLISTED_ENTITIES[ string.lower( k ) ] || BLACKLISTED_WEAPONS[ string.lower( k ) ] );
			if ( _bContains && !_blacklisted ) then
				table.insert( _list, _cmd .. " " .. k );
			end
		end

		return _list;
	end

	// All of the _list = aren't needed because of pass by ref, the helper-function is modifying the actual list
	// so it isn't needed; but I plan on recoding it to make it much more robust so I'm leaving it...
	local _list = { };
	_list = ProcessEntityList( _cmd, _input, list.Get( "SpawnableEntities" ), _list );
	_list = ProcessEntityList( _cmd, _input, list.Get( "NPC" ), _list );
	_list = ProcessEntityList( _cmd, _input, list.Get( "Weapon" ), _list );
	_list = ProcessEntityList( _cmd, _input, list.Get( "Vehicles" ), _list );

	return _list;
end