//
// torpint - converts all data-types into strings - Josh 'Acecool' Moser
// Internal function for extended print functionality	can be called like it is to beautify text before saving to a document or so..
//
-- CLuaParticle, CLuaEmitter, CMoveData, CRecipientFilter, CSEnt, CSoundPatch, CTakeDamageInfo, CUserCmd
local function tostringdepth( _depth )
	return string.rep( "\t", _depth );
end


//
// converts input _data to string... Additionally allows tables to be converted to Lua, or other data structures that allow it.
//
function toprint( _data, _lua, _depth, _processed )
	if ( !_depth ) then _depth = 0; end
	if ( !_processed ) then _processed = { }; end

	//
	if ( IsColor( _data ) ) then
		return "Color( " .. _data.r .. ", " .. _data.g .. ", " .. _data.b .. ", " .. _data.a .. " )", PRINT_COLORS.Color || _data;
	elseif ( isstring( _data ) ) then
		// Language system...
		if ( string.StartWith( _data, "#" ) ) then
			local _phrase = string.sub( _data, 2, string.len( _data ) );
			local _lang = language.GetPhrase( _phrase, ( LocalPlayer && LocalPlayer( ):GetFlag( "language", language.__default, true ) || language.__default ) );
			local _color = ( _lang ) && PRINT_COLORS.StringLanguage || PRINT_COLORS.StringLanguageF;
			return tostring( _lang or _phrase ), _color;
		else
			return tostring( _data ), PRINT_COLORS.String;
		end
	elseif ( istable( _data ) ) then
		return table.ToString( _data, _depth, _processed, _lua ), PRINT_COLORS.Table;
	else
		return tostring( _data ), PRINT_COLORS[ type( _data ) ];
	end
end