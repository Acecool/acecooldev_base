//
// Notify - done in order to ensure they are SHARED
// http://wiki.garrysmod.com/page/Enums/NOTIFY
//


//
// Set the values to the names
//
NOTIFY_GENERIC 					= 0;
NOTIFY_ERROR 					= 1;
NOTIFY_UNDO 					= 2;
NOTIFY_HINT 					= 3;
NOTIFY_CLEANUP 					= 4;


//
// Ids to legible output
//
NOTIFICATIONS = { };
NOTIFICATIONS[ NOTIFY_GENERIC ] = "Generic";
NOTIFICATIONS[ NOTIFY_ERROR ] 	= "Error";
NOTIFICATIONS[ NOTIFY_UNDO ] 	= "Undo";
NOTIFICATIONS[ NOTIFY_HINT ] 	= "Hint";
NOTIFICATIONS[ NOTIFY_CLEANUP ] = "Cleanup";