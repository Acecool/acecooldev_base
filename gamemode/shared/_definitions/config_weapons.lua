//
// Weapon Config - Josh 'Acecool' Moser
//


//
// Define the weapons the player may not drop
//
UNDROPPABLE_WEAPONS = {
	weapon_toolgun 		= true;
	weapon_physcannon 	= true;
	weapon_physgun 		= true;
	admin_baton 		= true;
};

//
// Define the weapons the player may not receive
//
BLACKLISTED_WEAPONS = {
	"acecool_weapon_base";
	"weapon_base";
	"admin_baton";
	"flechette_gun";
	"manhack_welder";
}