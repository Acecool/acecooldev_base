//
// Data Types - Josh 'Acecool' Moser
//


TYPEID_TOSTRING = {
	[ TYPE_ANGLE ] = "Angle";
	[ TYPE_BOOL ] = "Boolean";
	[ TYPE_CONVAR ] = "ConVar";
	[ TYPE_COUNT ] = "Count";
	[ TYPE_DAMAGEINFO ] = "DamageInfo";
	[ TYPE_DLIGHT ] = "DynamicLight";
	[ TYPE_EFFECTDATA ] = "EffectData";
	[ TYPE_ENTITY ] = "Entity";
	[ TYPE_FILE ] = "File";
	[ TYPE_FUNCTION ] = "Function";
	[ TYPE_IMESH ] = "IMesh";
	[ TYPE_INVALID ] = "Invalid";
	[ TYPE_LIGHTUSERDATA ] = "LightUserData";
	[ TYPE_MATERIAL ] = "Material";
	[ TYPE_MATRIX ] = "Matrix";
	[ TYPE_MOVEDATA ] = "MoveData";
	[ TYPE_NIL ] = "nil";
	[ TYPE_NUMBER ] = "Number";
	[ TYPE_PANEL ] = "Panel";
	[ TYPE_PARTICLE ] = "Particle";
	[ TYPE_PARTICLEEMITTER ] = "ParticleEmitter";
	[ TYPE_PHYSOBJ ] = "PhysicsObject";
	[ TYPE_PIXELVISHANDLE ] = "PixelVisibleHandler";
	[ TYPE_RECIPIENTFILTER ] = "RecipientFilter";
	[ TYPE_RESTORE ] = "Restore";
	[ TYPE_SAVE ] = "Save";
	[ TYPE_SCRIPTEDVEHICLE ] = "ScriptedVehicle";
	[ TYPE_SOUND ] = "Sound";
	[ TYPE_STRING ] = "String";
	[ TYPE_TABLE ] = "Table";
	[ TYPE_TEXTURE ] = "Texture";
	[ TYPE_THREAD ] = "Thread";
	[ TYPE_USERCMD ] = "UserCMD";
	[ TYPE_USERMSG ] = "UserMSG";
	[ TYPE_VECTOR ] = "Vector";
	[ TYPE_VIDEO ] = "Video";
	default = "Unknown";
};

-- TYPE_NIL	
-- TYPE_STRING
-- TYPE_NUMBER
-- TYPE_TABLE
-- TYPE_BOOL	
-- TYPE_ENTITY
-- TYPE_VECTOR
-- TYPE_ANGLE
-- TYPE_COLOR

-- TYPE_ANGLE 				11		Angle
-- TYPE_BOOL 				1 		boolean
-- TYPE_CONVAR 				27		ConVar
-- TYPE_COUNT 				39		Amount of TYPE_* enums
-- TYPE_DAMAGEINFO 			15		CTakeDamageInfo
-- TYPE_DLIGHT 				30		
-- TYPE_EFFECTDATA 			16		CEffectData
-- TYPE_ENTITY 				9 		Entity
-- TYPE_FILE 				34		File
-- TYPE_FUNCTION 			6 		function
-- TYPE_IMESH 				28		IMesh
-- TYPE_INVALID 			-1		Invalid type
-- TYPE_LIGHTUSERDATA 		30		
-- TYPE_MATERIAL 			21		IMaterial
-- TYPE_MATRIX 				29		VMatrix
-- TYPE_MOVEDATA 			17		CMoveData
-- TYPE_NIL 				0 	
-- TYPE_NUMBER 				3 		number
-- TYPE_PANEL 				22		Panel
-- TYPE_PARTICLE 			23		CLuaParticle
-- TYPE_PARTICLEEMITTER 	24		CLuaEmitter
-- TYPE_PHYSOBJ 			12		PhysObj
-- TYPE_PIXELVISHANDLE 		30		pixelvis_handle_t
-- TYPE_RECIPIENTFILTER 	18		CRecipientFilter
-- TYPE_RESTORE 			14		IRestore
-- TYPE_SAVE 				13		ISave
-- TYPE_SCRIPTEDVEHICLE 	20		Vehicle
-- TYPE_SOUND 				30		
-- TYPE_STRING 				4 		string
-- TYPE_TABLE 				5 		table
-- TYPE_TEXTURE 			24		ITexture
-- TYPE_THREAD 				8 		thread
-- TYPE_USERCMD 			19		CUserCmd
-- TYPE_USERMSG 			26		
-- TYPE_VECTOR 				10		Vector
-- TYPE_VIDEO 				33		IVideoWriter


local i 							= 0;
DATA_TYPE_NIL 						= i; i = i + 1;
DATA_TYPE_ANGLE 					= i; i = i + 1;
DATA_TYPE_BOOLEAN 					= i; i = i + 1;
DATA_TYPE_COLOR 					= i; i = i + 1;
DATA_TYPE_DAMAGEINFO 				= i; i = i + 1;
DATA_TYPE_EFFECTDATA 				= i; i = i + 1;
DATA_TYPE_EMITTER 					= i; i = i + 1;
DATA_TYPE_ENTITY 					= i; i = i + 1;
DATA_TYPE_FUNCTION 					= i; i = i + 1;
DATA_TYPE_MATERIAL 					= i; i = i + 1;
DATA_TYPE_MOVEDATA 					= i; i = i + 1;
DATA_TYPE_NUMBER 					= i; i = i + 1;
DATA_TYPE_PANEL 					= i; i = i + 1;
DATA_TYPE_PARTICLE 					= i; i = i + 1;
DATA_TYPE_PLAYER 					= i; i = i + 1;
DATA_TYPE_RECIPIENTFILTER 			= i; i = i + 1;
DATA_TYPE_SOUNDPATCH 				= i; i = i + 1;
DATA_TYPE_STRING 					= i; i = i + 1;
DATA_TYPE_TABLE 					= i; i = i + 1;
DATA_TYPE_TEXTURE 					= i; i = i + 1;
DATA_TYPE_USERCMD 					= i; i = i + 1;
DATA_TYPE_VECTOR 					= i; i = i + 1;
DATA_TYPE_VMATRIX 					= i; i = i + 1;

DATA_TYPE_MAP = { };
DATA_TYPE_MAP[ "Angle" ] 			= DATA_TYPE_ANGLE;
DATA_TYPE_MAP[ "boolean" ] 			= DATA_TYPE_BOOLEAN;
DATA_TYPE_MAP[ "CTakeDamageInfo" ] 	= DATA_TYPE_DAMAGEINFO;
DATA_TYPE_MAP[ "CEffectData" ] 		= DATA_TYPE_EFFECTDATA;
DATA_TYPE_MAP[ "CLuaEmitter" ] 		= DATA_TYPE_EMITTER;
DATA_TYPE_MAP[ "function" ]  		= DATA_TYPE_FUNCTION;
DATA_TYPE_MAP[ "CLuaParticle" ] 	= DATA_TYPE_PARTICLE;
DATA_TYPE_MAP[ "IMaterial" ] 		= DATA_TYPE_MATERIAL;
DATA_TYPE_MAP[ "CMoveData" ] 		= DATA_TYPE_MOVEDATA;
DATA_TYPE_MAP[ "nil" ] 				= DATA_TYPE_NIL;
DATA_TYPE_MAP[ "number" ] 			= DATA_TYPE_NUMBER;
DATA_TYPE_MAP[ "Entity" ] 			= DATA_TYPE_ENTITY;
DATA_TYPE_MAP[ "Player" ] 			= DATA_TYPE_ENTITY;
DATA_TYPE_MAP[ "NPC" ] 				= DATA_TYPE_ENTITY;
DATA_TYPE_MAP[ "Vehicle" ] 			= DATA_TYPE_ENTITY;
DATA_TYPE_MAP[ "Weapon" ] 			= DATA_TYPE_ENTITY;
DATA_TYPE_MAP[ "CRecipientFilter" ] = DATA_TYPE_RECIPIENTFILTER;
DATA_TYPE_MAP[ "CSoundPatch" ] 		= DATA_TYPE_SOUNDPATCH;
DATA_TYPE_MAP[ "string" ] 			= DATA_TYPE_STRING;
DATA_TYPE_MAP[ "table" ] 			= DATA_TYPE_TABLE;
DATA_TYPE_MAP[ "ITexture" ] 		= DATA_TYPE_TEXTURE;
DATA_TYPE_MAP[ "CUserCmd" ] 		= DATA_TYPE_USERCMD;
DATA_TYPE_MAP[ "Vector" ] 			= DATA_TYPE_VECTOR;
DATA_TYPE_MAP[ "VMatrix" ] 			= DATA_TYPE_VMATRIX;