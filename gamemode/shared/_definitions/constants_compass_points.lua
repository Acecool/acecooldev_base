//
// Directions - Josh 'Acecool' Moser
//
DIR_NORTH 	= 90
DIR_WEST 	= math.NormalizeAngle( DIR_NORTH + 90 )
DIR_SOUTH 	= math.NormalizeAngle( DIR_NORTH - 180 )
DIR_EAST 	= math.NormalizeAngle( DIR_NORTH - 90 )