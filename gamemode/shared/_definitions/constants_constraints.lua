//
//	- Josh 'Acecool' Moser
//
CONSTRAINTS = {
	"Weld";
	"Rope";
	"NoCollide";
	"AdvBallsocket";
	"Axis";
	"Ballsocket";
	"Elastic";
	"Hydraulic";
	"KeepUpright";
	"Muscle";
	"Pulley";
	"Winch";
	"Slider";
};