//
// Conversions - Josh 'Acecool' Moser
//


//
// Unit to Unit conversions... Simply tells us how many source-units per 1 real-life-length-unit
//

// 1 unit 			= 0.75 inches.
// 4/3 units 		= 1 inch - from this we base our calculations.
UNITS_PER_INCH 		= 4 / 3; // 4/3 units per 1 inch, 16 units per foot

// Feet conversion ( 16 Source Units Per FEET )
UNITS_PER_FEET 		= UNITS_PER_INCH * 12; // == 16 in game units

// Source units in 1 meter
UNITS_PER_METER		= UNITS_PER_FEET / 0.3048; -- 52.493438320209973753280839895013
-- UNITS_PER_METER		= UNITS_PER_FEET * 3.28084; -- 52.49344

// Source units in 1 mile ( either calculation works )
UNITS_PER_MILE		= UNITS_PER_FEET * 5279.9983104005406718269850153648; -- 84479.972966408650749231760245837
-- UNITS_PER_MILE		= UNITS_PER_FEET / 0.000189394; -- 84479.972966408650749231760245837

// Source units in 1 Kilometer
UNITS_PER_KM		= UNITS_PER_FEET / 0.0003048; -- 52493.438320209973753280839895013
-- UNITS_PER_KM		= UNITS_PER_FEET * 3280.84; -- 52493.44


//
// Velocity calculations - Calculates SPEED using: ( Velocity * dTime * VEL_TO_* )
//
// Simply _ent:GetVelocity( ):Length( ) / VELOCITY_TO_MPH;
// Simply _ent:GetVelocity( ):Length( ) / VELOCITY_TO_KPH;
//
// 1 mile per hour 		= 0.44704 m/s
// 1 kilometer per hour = 0.277777778 m/s
//
VELOCITY_TO_MPH		= ( UNITS_PER_METER * 0.44704 ) / 60 / 60;
VELOCITY_TO_KPH		= ( UNITS_PER_METER * 0.277777778 ) / 60 / 60;
VELOCITY_TO_KMH		= VELOCITY_TO_KPH;