//
// - Josh 'Acecool' Moser
//
ACCESSLEVEL_GUEST 				= 0;
ACCESSLEVEL_MEMBER 				= 1;
ACCESSLEVEL_VIP 				= 100;
-- ACCESSLEVEL_VIP2 			= 101;
-- ACCESSLEVEL_VIP3 			= 102;
-- ACCESSLEVEL_VIP4 			= 103;
-- ACCESSLEVEL_VIP5 			= 104;
ACCESSLEVEL_TRIAL_ADMIN 		= 1000;
ACCESSLEVEL_GAME_MOD 			= 1001;
ACCESSLEVEL_ADMIN 				= 1002;
ACCESSLEVEL_SENIOR_ADMIN 		= 1003;
ACCESSLEVEL_SUPER_ADMIN 		= 2000;
ACCESSLEVEL_SENIOR_SUPER_ADMIN 	= 2001;
ACCESSLEVEL_DEVELOPER 			= 3000;
ACCESSLEVEL_OWNER 				= 9000;

local i = 0;
PERMISSIONS_CAN_FLY 		= i; i = i + 1;
PERMISSIONS_CAN_NOCLIP 		= i; i = i + 1;
-- PERMISSIONS_CAN_NOCLIP 	= i; i = i + 1;
-- PERMISSIONS_CAN_NOCLIP 	= i; i = i + 1;

//
// 
//
ACCESSLEVELS_DB = { };
ACCESSLEVELS_DB[ ACCESSLEVEL_GUEST ] = {
	name = "Guest";
	permissions = { };
	noclip = false;
	icon = "icon16/new.png";
};
ACCESSLEVELS_DB[ ACCESSLEVEL_MEMBER ] = {
	name = "Member";
	permissions = { };
	noclip = false;
	gender_icon = { [ GENDER_MALE ] = "icon16/user_male.png", [ GENDER_FEMALE ] = "icon16/user_female.png" };
};
ACCESSLEVELS_DB[ ACCESSLEVEL_VIP ] = {
	name = "VIP";
	permissions = { };
	noclip = false;
	color = COLOR_ARMY;
	icon = "icon16/ruby.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_TRIAL_ADMIN ] = {
	name = "Trial Admin";
	permissions = {  };
	noclip = PERMISSIONS_CAN_FLY;
	color = COLOR_PINK;
	icon = "icon16/tag_pink.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_GAME_MOD ] = { 
	name = "Game Moderator";
	permissions = { };
	noclip = PERMISSIONS_CAN_FLY;
	color = COLOR_CYAN;
	icon = "icon16/tag_blue.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_ADMIN ] = { 
	name = "Administrator";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_YELLOW;
	icon = "icon16/tag_yellow.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_SENIOR_ADMIN ] = { 
	name = "Senior Administrator";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_ORANGE;
	icon = "icon16/tag_orange.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_SUPER_ADMIN ] = { 
	name = "Super Administrator";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_GREEN;
	icon = "icon16/award_gold_star_2.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_SENIOR_SUPER_ADMIN ] = { 
	name = "Senior Super Administrator";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_DARK_GREEN;
	icon = "icon16/award_gold_star_2.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_DEVELOPER ] = { 
	name = "Developer";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_RED;
	icon = "icon16/award_gold_star_1.png"
};
ACCESSLEVELS_DB[ ACCESSLEVEL_OWNER ] = { 
	name = "Owner / Founder";
	permissions = {  };
	noclip = PERMISSIONS_CAN_NOCLIP;
	color = COLOR_BLUE;
	icon = "icon16/award_gold_star_3.png"
};