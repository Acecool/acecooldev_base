//
// Return if a player is fully connected - Josh 'Acecool' Moser
//
function META_PLAYER:IsFullyConnected( )
	return self:GetFlag( "isfullyconnected", false, false, FLAG_PLAYER );
end