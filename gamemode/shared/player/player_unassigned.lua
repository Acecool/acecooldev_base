//
// Simple helper function to determine whether or not the player isn't assigned to a team - Josh 'Acecool' Moser
//
function META_PLAYER:Unassigned( )
	if ( self:Team( ) == TEAM_UNASSIGNED || self:Team( ) == TEAM_SPECTATOR ) then
		return true;
	end

	return false;
end