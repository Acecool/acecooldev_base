//
// Returns the players access level to the server
//
function META_PLAYER:GetAccessLevel( )
	local _level = self:GetFlag( "accesslevel", ACCESSLEVEL_GUEST );
	local _db = ACCESSLEVELS_DB[ _level ];
	return _level, _db;
end