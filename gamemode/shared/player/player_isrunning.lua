//
// Simple helper function to determine if the player is sprinting - Josh 'Acecool' Moser
//
function META_PLAYER:IsRunning( )
	// self:KeyDown( IN_SPEED ) &&
	return self:OnGround( ) && self:GetVelocity( ):Length( ) > self:GetWalkSpeed( );
end