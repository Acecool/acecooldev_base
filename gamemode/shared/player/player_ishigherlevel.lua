//
// Checks to see if self > target in terms of admin power. - Josh 'Acecool' Moser
//
function META_PLAYER:IsHigherLevel( _t )
	if ( !IsValid( _t ) && !isnumber( _t ) ) then return false; end

	// Disable IsHigherLevel if the player is frozen ( So if a bad admin is frozen, they can't kickall, etc... )
	if ( self.IsFrozen && self:IsFrozen( ) ) then return false; end

	// Allow numerical input; compares the current player access level against the numerical value
	if ( isnumber( _t ) ) then
		if ( self.GetAccessLevel && self:GetAccessLevel( ) > _t ) then
			return true;
		end

		return false;
	end

	// Allow player entity input; compares the current player access level vs input player. IsHigherLevel returns true for self.
	if ( _t:IsPlayer( ) ) then
		if ( ( self.GetAccessLevel && self:GetAccessLevel( ) > _t:GetAccessLevel( ) ) || ( self:IsAdmin( ) && !_t:IsAdmin( ) ) || ( self == _t ) ) then
			return true;
		end

		return false;
	end
end