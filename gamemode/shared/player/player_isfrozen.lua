//
// Returns whether or not the player is frozen - Josh 'Acecool' Moser
//
function META_PLAYER:IsFrozen( )
	return self:GetFlag( "isfrozen", false, false, FLAG_PLAYER );
end