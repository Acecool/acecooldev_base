//
// Returns the selected player language or the default language for the server - Josh 'Acecool' Moser
//
function META_PLAYER:GetLanguage( )
	return self:GetFlag( "language", language.__default, true );
end