//
//	- Josh 'Acecool' Moser
//
function GM:InitPostEntity( )
	// Redirect used so that on map cleanup, only the map setup stuff can be called...
	hook.Call( "SetupMap", GAMEMODE );
	hook.Call( "PostSetupMap", GAMEMODE );

end