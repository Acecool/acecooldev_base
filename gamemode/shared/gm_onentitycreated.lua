//
//	- Josh 'Acecool' Moser
//
function GM:OnEntityCreated( _ent )
	// SERVER Handles the details to prevent loss of precision from this being called on CLIENT by PVS
	if ( SERVER ) then
		// No point in continuing if the entity isn't valid...
		if ( !IsValid( _ent ) ) then return; end

		// Entity ID / EntIndex
		local _id = _ent:GetID( );

		// A Lot of useful hooks which execute on client and server!
		if ( _ent:IsWeapon( ) ) then
			// Weapon Removal Hook
			hook.Call( "OnWeaponCreated", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnWeaponCreated", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsVehicle( ) ) then
			// Vehicle Removal Hook
			hook.Call( "OnVehicleCreated", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnVehicleCreated", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsPlayer( ) ) then
			// Player Removal Hook
			hook.Call( "OnPlayerCreated", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnPlayerCreated", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsNPC( ) ) then
			// NPC Removal Hook
			hook.Call( "OnNPCCreated", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnNPCCreated", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsRagdoll( ) ) then
			// Ragdoll Removal Hook
			hook.Call( "OnRagdollCreated", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnRagdollCreated", "GAMEMODE", _ent, _id );
		end
	end
end