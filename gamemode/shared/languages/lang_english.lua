//
//  - Josh 'Acecool' Moser
//
local _lang = "en";


//
// Language Data Examples
//
language.Add( "hello", "Hello", _lang );
language.Add( "test_format", "string.Format is called on language strings %s when the GetPhrase is called with additional arguments.", _lang );

// Asser Language
language.Add( "datatype_example_unknown", "Examples Unavailable", _lang );
language.Add( "datatype_example_number", "Examples of <number>: -999999, -1234, -100, -10, 0, 10, 100, 1234, 999999, etc...", _lang );
language.Add( "datatype_example_boolean", "Examples of 'Boolean': true or false", _lang );
language.Add( "datatype_example_vector", "Examples of 'Vector': vector_origin, Vector( <number' x, <number> y, <number> z ) where <number> represents the data-type of each argument value", _lang );
language.Add( "datatype_example_angle", "Examples of 'Angle': angle_zero, Angle( <number> pitch, <number> yaw, <number> roll ) where <number> represents the data-type of each argument value", _lang );
language.Add( "assert_error", "failed", _lang );
language.Add( "assert_error", "Reported an AssertArgs( _map, ... ) Improper Argument-Map > Argument-Data Error! Argument data-type entered is '%s' ( %s ) containing the value '%s' when the data-type '%s' ( %s ) was expected instead; Verify you're passing the correct data-type and value to the function.", _lang );




// Errors / Debugging
language.Add( "meta_color_unavailable", "You're not running the DEV version of Garry's Mod meaning META_COLOR isn't initialized therefore portions of the script have been halted to avoid messy errors!\n", _lang );

// Example of the above.. nil is where the dictionary should be used, if empty, false or nil it will 
// use the DEFAULT language of the server defined in the class_language.lua
-- print( language.GetPhrase( "test_format", nil, "like this one" ) );


language.Add( "assert_error", "Assert failed. Expected ID %s, Type: %s, received ID %s, Type: %s, on data: %s", _lang );
language.Add( "you_must_write_something_to_the_admins", "You must include some form of question to the admins ( up to 125 characters )", _lang );
language.Add( "admin_chat_from_x", "%sat %s > Admins: ", _lang );
language.Add( "admin_chat_from_x_error", "%sat %s > Admins [ NO ADMINS ONLINE ]: ", _lang );

language.Add( "physgun_pickup_by", "You've been Picked Up by: %s [ %s ]!", _lang );
language.Add( "physgun_dropped_by", "You've been Dropped by: %s [ %s ]!", _lang );
language.Add( "attacking_frozen_player_warning", "You're attacking a frozen player; they don't take damage!", _lang );
language.Add( "unfrozen_by", "You've been Un-Frozen by: %s [ %s ]!", _lang );
language.Add( "frozen_by", "You've been Frozen by: %s [ %s ]!", _lang );

language.Add( "target_ishigherlevel", "You may not perform that action on users with the same or higher level than yourself.", _lang );


//
//
//
language.Add( "query_builder_expects_tables", "QueryBuilder: You must add at least 1 table to build a query!", _lang );
language.Add( "query_builder_expects_values", "QueryBuilder: You must add values to go along with the columns.", _lang );
language.Add( "query_builder_where_expects_and_or_logic", "QueryBuilder: AddWhere expects AND or OR for additional WHERE clauses!", _lang );