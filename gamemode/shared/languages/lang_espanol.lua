//
//  - Josh 'Acecool' Moser
//
local _lang = "es";


//
// Language Data
//
language.Add( "hello", "Ola", _lang );
language.Add( "test_format", "string.Format is called on language strings %s when the GetPhrase is called with additional arguments.", _lang );

// Example of the above.. nil is where the dictionary should be used, if empty, false or nil it will 
// use the DEFAULT language of the server defined in the class_language.lua
-- print( language.GetPhrase( "test_format", nil, "like this one" ) );