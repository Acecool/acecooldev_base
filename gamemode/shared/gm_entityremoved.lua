//
// EntityRemoved; Called on SERVER when an ENTITY is REMOVED; Called on CLIENT when ENTITY leaves PVS for that client.
//
function GM:EntityRemoved( _ent )
	// SERVER Handles the details to prevent loss of precision from this being called on CLIENT by PVS
	if ( SERVER ) then
		// No point in continuing if the entity isn't valid...
		if ( !IsValid( _ent ) ) then return; end

		// Entity ID / EntIndex
		local _id = _ent:GetID( );

		// A Lot of useful hooks which execute on client and server!
		if ( _ent:IsWeapon( ) ) then
			// Weapon Removal Hook
			hook.Call( "OnWeaponRemoved", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnWeaponRemoved", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsVehicle( ) ) then
			// Vehicle Removal Hook
			hook.Call( "OnVehicleRemoved", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnVehicleRemoved", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsPlayer( ) ) then
			// Player Removal Hook
			hook.Call( "OnPlayerRemoved", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnPlayerRemoved", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsNPC( ) ) then
			// NPC Removal Hook
			hook.Call( "OnNPCRemoved", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnNPCRemoved", "GAMEMODE", _ent, _id );
		elseif ( _ent:IsRagdoll( ) ) then
			// Ragdoll Removal Hook
			hook.Call( "OnRagdollRemoved", GAMEMODE, _ent, _id );
			networking:BroadcastHook( "OnRagdollRemoved", "GAMEMODE", _ent, _id );
		end
	end
end