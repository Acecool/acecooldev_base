//
//	- Josh 'Acecool' Moser
//
__META_ENTITY_SPAWN		= __META_ENTITY_SPAWN || META_ENTITY.Spawn;


//
// Logic for OnEntitySpawned
//
function META_ENTITY:Spawn( )
	// Call the original function
	__META_ENTITY_SPAWN( self );

	// Call our hook
	hook.Call( "OnEntitySpawned", GAMEMODE, self );
end