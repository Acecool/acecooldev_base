//
// A simple system so the server can use IsSpeaking - Josh 'Acecool' Moser
//
function META_PLAYER:IsSpeaking( )
	return self:GetFlag( "isspeaking", false, false, FLAG_PLAYER );
end