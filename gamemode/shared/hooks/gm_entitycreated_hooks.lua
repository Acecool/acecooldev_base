//
// Simple Callbacks in OnEntityCreated for specific purposes - Josh 'Acecool' Moser
//


//
// This is called when a Weapon is created. Overwrite here or hook.Add
//
function GM:OnWeaponCreated( _w, _id )

end


//
// This is called when a Vehicle is created. Overwrite here or hook.Add
//
function GM:OnVehicleCreated( _v, _id )

end


//
// This is called when an NPC is created. Overwrite here or hook.Add
//
function GM:OnNPCCreated( _npc, _id )

end


//
// This is called when a Player is created. Overwrite here or hook.Add
//
function GM:OnPlayerCreated( _p, _id )

end


//
// This is called when a Ragdoll is created. Overwrite here or hook.Add
//
function GM:OnRagdollCreated( _ragdoll, _id )

end