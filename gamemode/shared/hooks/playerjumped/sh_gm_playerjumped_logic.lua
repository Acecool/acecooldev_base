//
// Player Jumped Shared Hook - Josh 'Acecool' Moser
//
// This is a shared hook which fires when a player jumps, it fires once and only once
//  until the player is on the ground when they can jump again.
hook.Add( "SetupMove", "SetupMove:PlayerJumped", function( _p, _move, _cmd )
	local _bInWater = _p:IsFlagSet( bit.bor( FL_INWATER ) );
	if ( _move:KeyDown( IN_JUMP ) && ( _p:OnGround( ) || _bInWater ) && !_p.__PlayerJumped ) then
		_p.__PlayerJumped = true;
		hook.Call( "PlayerJumped", GAMEMODE, _p, _bInWater );
	elseif ( !_move:KeyDown( IN_JUMP ) ) then
		_p.__PlayerJumped = false;
	end
end );