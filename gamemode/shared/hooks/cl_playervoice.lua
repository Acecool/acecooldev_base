//
// A simple system so the server can use IsSpeaking - Josh 'Acecool' Moser
//


//
// Have the client update the server regarding their voice...
//
hook.Add( "PlayerStartVoice", "PlayerStartVoice:Networking", function( _p )
	if ( _p == LocalPlayer( ) ) then
		networking:SyncFlag( "isspeaking", _p, _p, true, false, FLAG_PLAYER );
	end
end );


//
// Have the client update the server regarding their voice...
//
hook.Add( "PlayerEndVoice", "PlayerEndVoice:Networking", function( _p )
	if ( _p == LocalPlayer( ) ) then
		networking:SyncFlag( "isspeaking", _p, _p, false, false, FLAG_PLAYER );
	end
end );