//
// Simple Callbacks in EntityRemoved for specific purposes - Josh 'Acecool' Moser
//


//
// This is called when a Weapon is removed. Overwrite here or hook.Add
//
function GM:OnWeaponRemoved( _w, _id )

end


//
// This is called when a Vehicle is removed. Overwrite here or hook.Add
//
function GM:OnVehicleRemoved( _v, _id )

end


//
// This is called when an NPC is removed. Overwrite here or hook.Add
//
function GM:OnNPCRemoved( _npc, _id )

end


//
// This is called when a Player is removed. Overwrite here or hook.Add
//
function GM:OnPlayerRemoved( _p, _id )

end


//
// This is called when a Ragdoll is removed. Overwrite here or hook.Add
//
function GM:OnRagdollRemoved( _ragdoll, _id )

end