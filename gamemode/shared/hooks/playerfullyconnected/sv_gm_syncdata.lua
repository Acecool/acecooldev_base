//
// SyncData - Override for syncing data to the client after they're verified fully-connected - Josh 'Acecool' Moser
//
function GM:SyncData( _p )
	// Send Syncable Entity Flags to Clients
	networking:SendToClient( "SyncData", _p, data:GetFlags( ) );
end