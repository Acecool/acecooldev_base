//
// PlayerFullyConnected Hook - Josh 'Acecool' Moser
//


//
// Helper-Function to ensure users don't bypass client-code...
//
local function DetectPlayerFullyConnected( _p, _hook, ... )
	if ( _p:IsFullyConnected( ) ) then return; end

	// Set the name, manages name changes.
	_p:SetFlag( "name", _p:Nick( ), true, FLAG_PLAYER );
	_p:SetFlag( "timeconnected", CurTime( ), true, FLAG_PLAYER );
	_p:SetFlag( "isfullyconnected", true, false, FLAG_PLAYER );

	// Notify the client that they're here. This'll be incorporated with the net system integration update.
	networking:CallHook( "PlayerFullyConnected", _p, "GAMEMODE", _p );

	// Broadcast the hook...
	hook.Call( "PlayerFullyConnected", GAMEMODE, _p );

	// Sync Data ( Perfect place to call it )...
	hook.Call( "SyncData", GAMEMODE, _p );
end


//
// Security feature to ensure client doesn't bypass code.
//
hook.Add( "SetupMove", "PlayerFullyConnected:SetupMove", function( _p, _move, _cmd )
	DetectPlayerFullyConnected( _p, "SetupMove", _p, _move, _cmd );
end );