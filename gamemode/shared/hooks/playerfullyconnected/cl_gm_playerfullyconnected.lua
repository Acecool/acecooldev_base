//
// PlayerFullyConnected Hook - Josh 'Acecool' Moser
//
// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
// and keep server code from being downloaded by the client.
//
function GM:PlayerFullyConnected( _p )
	hook.Call( "SetupClientMap", GAMEMODE, _p );
end