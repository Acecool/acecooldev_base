//
// PlayerFullyConnected Hook - Josh 'Acecool' Moser
//
// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
// and keep server code from being downloaded by the client.
//
function GM:PlayerFullyConnected( _p )
	// Notify in console when the player has fully connected
	MsgC( COLOR_WHITE, tostring( _p ) .. " has fully connected. ( " .. CurTime( ) .. " )\n" );
end