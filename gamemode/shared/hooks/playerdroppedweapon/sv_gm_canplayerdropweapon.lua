//
// Pre-Check to detect whether or not a weapon is allowed to be dropped - Josh 'Acecool' Moser
//
function GM:CanPlayerDropWeapon( _p, _w )
	// Can't drop a weapon if it isn't valid
	if ( !IsValid( _w ) ) then return false; end

	// Can't drop a weapon we're not allowed to holster
	if ( _w.Holster && !_w:Holster( ) ) then return false; end

	// If the weapon is in the list of undroppable weapons, don't allow it to be dropped, and tell the client...
	if ( UNDROPPABLE_WEAPONS[ _w:GetClass( ) ] ) then
		_p:Notify( language.GetPhrase( "can_not_drop_weapon", _p:GetLanguage( ) ), NOTIFY_ERROR );
		return false;
	end

	return true;
end