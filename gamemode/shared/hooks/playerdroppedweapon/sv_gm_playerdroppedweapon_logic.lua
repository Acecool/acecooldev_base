//
// Add CanPlayerDropWeapon check, and PlayerDroppedWeapon hook - Josh 'Acecool' Moser
//

//
// Redirect DropWeapon ( Now keeping it internal to the player meta-table instead of creating new global )
//
META_PLAYER.__DropWeapon = META_PLAYER.__DropWeapon || META_PLAYER.DropWeapon;

//
// Logic for PlayerDroppedWeapon
//
function META_PLAYER:DropWeapon( _w )
	// Ensure the player is allowed to drop the weapon
	local _canDrop = hook.Call( "CanPlayerDropWeapon", GAMEMODE, self, _w ) || true;
	if ( _canDrop ) then
		// Call a pre-drop hook for the sake of allowing code to be executed prior to weapon-drop... and network it to the client
		hook.Call( "PrePlayerDroppedWeapon", GAMEMODE, self, _w );
		networking:CallHook( "PrePlayerDroppedWeapon", self, "GAMEMODE", self, _w );

		// Call the original function ( which calls META_WEAPON:OnDrop )
		self:__DropWeapon( _w );

		// Call our hook and network it to the client
		hook.Call( "PlayerDroppedWeapon", GAMEMODE, self, _w );
		networking:CallHook( "PlayerDroppedWeapon", self, "GAMEMODE", self, _w );
	end
end