//
// Callback for when a player successfully drops a weapon - Josh 'Acecool' Moser
//
// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate, 
// and keep server code from being downloaded by the client.
//
function GM:PlayerDroppedWeapon( _p, _w )
	debug.print( "Weapon:Dropped", _p, _w );
	data:UnlinkData( _p, _w );
end