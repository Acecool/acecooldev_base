//
// PlayerStartedRecording - Josh 'Acecool' Moser
//
// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
// and keep server code from being downloaded by the client.
//
function GM:PlayerRecordingDemo( _p, _recording )
	// When the client starts recording, they need to resync as the demo only knows about data it receives after it starts.
	if ( SERVER && _recording ) then
		hook.Call( "SyncData", GAMEMODE, _p );
	end
end


if ( SERVER ) then
	//
	// Needs a receiver because allowing client to call hook serverside itsn't good.
	//
	networking:AddReceiver( "PlayerRecordingDemo", function( _p, _recording )
		hook.Call( "PlayerRecordingDemo", GAMEMODE, _p, _recording );
	end );
end