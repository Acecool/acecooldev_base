//
// PlayerRecordingDemo Logic - Josh 'Acecool' Moser
//
hook.Add( "Think", "PlayerStartedRecordingLogic", function( )
	local _p = LocalPlayer( );
	if ( !IsValid( _p ) ) then return; end

	local _recording = _p:GetFlag( "isrecording", false, false, FLAG_PLAYER );
	if ( engine.IsRecordingDemo( ) && !_recording ) then
		networking:SyncFlag( "isrecording", _p, _p, true, true, FLAG_PLAYER );
		hook.Call( "PlayerRecordingDemo", GAMEMODE, _p, true );
		networking:SendToServer( "PlayerRecordingDemo", true );
	elseif ( !engine.IsRecordingDemo( ) && _recording ) then
		networking:SyncFlag( "isrecording", _p, _p, false, true, FLAG_PLAYER );
		hook.Call( "PlayerRecordingDemo", GAMEMODE, _p, false );
		networking:SendToServer( "PlayerRecordingDemo", false );
	end
end );