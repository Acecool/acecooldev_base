//
//	- Josh 'Acecool' Moser
//
__META_ENTITY_ACTIVATE	= __META_ENTITY_ACTIVATE || META_ENTITY.Activate;


//
// Logic for OnEntityActivated
//
function META_ENTITY:Activate( )
	// Call the original function
	__META_ENTITY_ACTIVATE( self );

	// Call our hook
	hook.Call( "OnEntityActivated", GAMEMODE, self );
end