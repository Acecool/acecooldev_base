//
//	- Josh 'Acecool' Moser
//
__META_ENTITY_SETMODEL = __META_ENTITY_SETMODEL || META_ENTITY.SetModel;


//
// Logic for OnEntitySpawned
//
function META_ENTITY:SetModel( ... )
	// Call the original function
	__META_ENTITY_SETMODEL( self, ... );

	// Call our hook
	hook.Call( "OnEntitySetModel", GAMEMODE, self, ... );
end