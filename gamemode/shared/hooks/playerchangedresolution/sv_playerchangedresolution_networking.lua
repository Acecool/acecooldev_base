//
// Called when the client changes resolution
//
networking:AddReceiver( "PlayerChangedResolution", function( _p, _w, _h, _oldw, _oldh )
	hook.Call( "PlayerChangedResolution", GAMEMODE, _p, _w, _h, _oldw, _oldh );
end );