//
// PlayerChangedResolution Hook - Josh 'Acecool' Moser
//
// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
// and keep server code from being downloaded by the client.
//
function GM:PlayerChangedResolution( _p, _w, _h, _oldw, _oldh )
	// Output resolution information in console when it happens...
	MsgC( COLOR_WHITE, tostring( _p ) .. " changed resolution to ( w, h ): ( " .. _w .. ", " .. _h .. " )\n" );
end