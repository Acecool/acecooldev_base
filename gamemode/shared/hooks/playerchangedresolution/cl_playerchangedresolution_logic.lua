//
// Logic for detecting resolution changes - Josh 'Acecool' Moser
// This allows you to create complex poly or other hud elements ONCE, and re-create or modify them ONCE iff resolution changes.
// Example usage: https://dl.dropboxusercontent.com/u/26074909/tutoring/_hooks/acecool_hook_playerchangedresolution/example_usage.lua
//
hook.Add( "HUDPaint", "PlayerChangedResolutionLogic", function( )
	local _p = LocalPlayer( );

	// Check if we should draw this hud - Most likely you'd want to keep this working...
	local _shoulddraw = hook.Call( "HUDShouldDraw", GAMEMODE, "PlayerChangedResolutionLogic" );

	// If the player isn't valid, is spectator, or we didn't want this to draw, then return
	if ( !IsValid( _p ) || !_shoulddraw ) then return; end

	// Init Screen Resolution...
	if ( !_p.__ScreenResolution ) then
		_p.__ScreenResolution = {
			w = ScrW( );
			h = ScrH( );
		};
	end

	// Detect Resolution Change...
	if ( ScrW( ) != _p.__ScreenResolution.w || ScrH( ) != _p.__ScreenResolution.h ) then
		// Call the PlayerChangedResolution hook with player object, NEW width, NEW height, OLD width, and OLD height
		hook.Call( "PlayerChangedResolution", GAMEMODE, _p, _w, _h, _p.__ScreenResolution.w, _p.__ScreenResolution.h );

		// Network it to the server; this'll be incorporated when the networking system is included.
		networking:SendToServer( "PlayerChangedResolution", _p, _p.__ScreenResolution.w, _p.__ScreenResolution.h );

		// Update the vars so it only calls once
		_p.__ScreenResolution.w = ScrW( );
		_p.__ScreenResolution.h = ScrH( );
	end
end );