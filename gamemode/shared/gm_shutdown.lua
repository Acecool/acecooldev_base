//
// Shutdown Example - Josh 'Acecool' Moser
//
function GM:ShutDown( )
	if ( SERVER ) then
		MsgC( COLOR_RED, "[" .. GAMEMODE.Name .. "]-GameMode ShutDown\n" );
	else
		MsgC( COLOR_AMBER, "[" .. GAMEMODE.Name .. "] Thank you, we hope you had fun playing on our server, feel free to add us to your favorites list.\n" );
		MsgC( COLOR_AMBER, "[" .. GAMEMODE.Name .. "] If you experienced any bugs or issues with other players and wish to make a report,\n" );
		MsgC( COLOR_AMBER, "[" .. GAMEMODE.Name .. "] please email " .. GAMEMODE.Author .. " at " .. GAMEMODE.Email  .. " or visit us on the web at " .. GAMEMODE.Website .. "\n" );
	end

	//
	// Gracefully remove console commands, hooks, etc using this hook...
	//
end